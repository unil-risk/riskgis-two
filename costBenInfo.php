<?php
	require_once 'dbConnect.php'; // Connect to the database
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$userID = $_POST['userID'];	
	$userRole = $_POST['userRole'];		
	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'load') {
		// allow all reports to be visible if logged with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT cost_benefit.*, user_name AS nom_utilisateur 
			FROM ".$workspace.".cost_benefit, ".$workspace.".users
			WHERE id_utilisateur = users.id;";
		}
		else {
			// allow only own reports to be visible if logged with other roles
			$query = "SELECT cost_benefit.*, user_name AS nom_utilisateur 
			FROM ".$workspace.".cost_benefit, ".$workspace.".users 
			WHERE id_utilisateur = $userID
			AND id_utilisateur = users.id;";
		}
		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'loadGrid') {
		// allow all layers to be visible if logged with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT DISTINCT risk_scenarios.*, alternatives.nom AS alt_nom, COALESCE(sum(cout_annuel) OVER (PARTITION BY risk_scenarios.id), 0) AS cout_total
			FROM ".$workspace.".risk_scenarios
			LEFT OUTER JOIN ".$workspace.".alternatives ON (risk_scenarios.alt_id = alternatives.id)
			LEFT OUTER JOIN ".$workspace.".cost_values ON (alternatives.id = cost_values.alt_id);";
		}
		else {
			// allow only own layers to be visible if logged with other roles
			$query = "SELECT DISTINCT risk_scenarios.*, alternatives.nom AS alt_nom, COALESCE(sum(cout_annuel) OVER (PARTITION BY risk_scenarios.id), 0) AS cout_total
			FROM ".$workspace.".risk_scenarios
			LEFT OUTER JOIN ".$workspace.".alternatives ON (risk_scenarios.alt_id = alternatives.id)
			LEFT OUTER JOIN ".$workspace.".cost_values ON (alternatives.id = cost_values.alt_id)
			WHERE risk_scenarios.id_utilisateur = $userID;";
		}
			
/* 		$query = "SELECT t1.id, t1.nom, t1.alt_nom, t1.processus_type, t1.objet_type, t1.risque_total, t1.indice, COALESCE(sum(cout_annuel),0) AS cout_total
		FROM (SELECT risk_scenarios.*, alternatives.nom AS alt_nom
		FROM ".$workspace.".risk_scenarios, ".$workspace.".alternatives
		WHERE risk_scenarios.alt_id = alternatives.id) AS t1
		LEFT OUTER JOIN ".$workspace.".cost_values ON (cost_values.alt_id = t1.alt_id)
		GROUP BY t1.id, t1.nom, t1.alt_nom, t1.processus_type, t1.objet_type, t1.risque_total, t1.indice;"; */

		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'loadChartStoreGrid') {
		$id = $_POST['id'];
		
		$query = "WITH t2 AS 
			(WITH t1 AS (SELECT cost_benefit_values.alt_id, alternatives.nom as alt_nom, sum(risque_total) as risque_total 
			FROM ".$workspace.".cost_benefit_values, ".$workspace.".alternatives, ".$workspace.".risk_scenarios
			WHERE cb_id = $id
			AND cost_benefit_values.risque_sce_id = risk_scenarios.id
			AND cost_benefit_values.alt_id = alternatives.id
			GROUP BY cost_benefit_values.alt_id, alt_nom)
			SELECT t1.*, (SELECT t1.risque_total as tp FROM t1 WHERE alt_id = 1), COALESCE(sum(cout_annuel),0) as cout_annuel, 0 as rapport FROM t1 LEFT JOIN ".$workspace.".cost_values
			ON t1.alt_id = cost_values.alt_id
			GROUP BY t1.alt_id, t1.alt_nom, t1.risque_total)
			SELECT t2.*, (tp-risque_total) as benefice FROM t2;"; 			
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	
	}
	
	if ($task == 'loadCostBenComValues') {
		$id = $_POST['cbID'];
		
		$query = "SELECT cost_benefit_values.alt_id, cost_benefit_values.risque_sce_id, risk_scenarios.nom as risque_sce_nom, alternatives.nom as alt_nom, processus_type, objet_type, objet_total, humain_total, risque_individuel, risque_total, risk_scenarios.indice
				FROM ".$workspace.".cost_benefit_values, ".$workspace.".alternatives, ".$workspace.".risk_scenarios
				WHERE cb_id = $id AND cost_benefit_values.risque_sce_id = risk_scenarios.id AND cost_benefit_values.alt_id = alternatives.id;";
				
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {		
		// retrieve POST data submitted by form
		$name = $_POST['name_report'];
		$description = $_POST['description_report'];
		$remarks = $_POST['remarks_report'];
		
		$postdata = $_POST['records'];	
		$phpArray = json_decode($postdata, true);
		$length = count($phpArray);
		
		### STEP 1: add a record to the cost_benefit table
		$query = "INSERT INTO ".$workspace.".cost_benefit VALUES (DEFAULT, '$name', '$description','$remarks', $userID);";
		
		###	STEP 2: insert the new records to cost_benefit_values table			
		
		for ($i = 0; $i < $length; $i++) {
			$temp = $phpArray[$i]; 				
			$altID = $temp['alt_id'];	
			$riskSceID = $temp['id'];
			$query .= "INSERT INTO ".$workspace.".cost_benefit_values VALUES (
				(SELECT last_value FROM ".$workspace.".cost_benefit_id_seq), $altID, $riskSceID);"; 		
		}
								
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else Echo '{success:true,message:"The new cost-benefit report has been generated!"}';
	}
	
	if ($task == 'edit') {
		
		$ID = $_POST['ID'];		
		$nom = $_POST['name'];
		$desc = $_POST['description'];
		$remarques = $_POST['remarks'];
		
		// query to update the record in the table
		$query = "UPDATE ".$workspace.".cost_benefit SET nom = '$nom', description = '$desc', remarques = '$remarques' WHERE id = $ID;";
		$arr = array();
				
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The report information has been successfully updated!"}';		
		}
			
	}
	
	if ($task == 'delete'){ // happens when a cost-benefit report is deleted

		$temp = $_POST['IDs'];	
		$records = json_decode($temp, true);
		$length = count($records);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected records
			$ID = $records[$i]['id'];			
			$query .= "DELETE FROM ".$workspace.".cost_benefit WHERE id = $ID;";
		}
		
		if (!$rs = pg_query($dbconn,$query)){			
			Echo '{success:"false",message:'.json_encode(pg_last_error($dbconn)).'}';						
		}
		else {
			Echo '{success:"true",message: "The selected cost-benefit reports have been deleted!"}';	
		}		
	}
?>	