<?php
	require_once 'dbConnect.php'; // Connect to the database	
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'loadCostValues') {
		$altID = $_POST['altID'];
		$query = "SELECT cost_values.id, cost_values.alt_id, cost_values.mesure_id, cost_values.mode, cost_values.cout_investissement, cost_values.valeur_residuelle, 
		cost_values.cout_exploitation, cost_values.cout_entretien, cost_values.cout_reparation, cost_values.periode_ans, cost_values.taux_interet, cost_values.cout_annuel,
		measures.ouvrage FROM ".$workspace.".cost_values, ".$workspace.".measures WHERE alt_id = $altID AND mesure_id = measures.id;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	// to save the dirty matrix values to the 'cost_values' table 
	if ($_GET['task'] == 'save'){
		$workspace = $_GET['ws'];
		$postdata = file_get_contents("php://input"); 
		$phpArray = json_decode($postdata, true);
		$length = count($phpArray);	
		
		for ($i = 0; $i < $length; $i++) { // loop for each of the dirty records
			$temp = $phpArray[$i]; 			
			$id = $temp['id'];		
			$mode = $temp['mode'];
			$investment_cost = $temp['cout_investissement'];
			$maintenance_cost = $temp['cout_entretien'];
			$repair_cost = $temp['cout_reparation'];
			$operating_cost = $temp['cout_exploitation'];
			$no_of_years = $temp['periode_ans'];
			$residual_value = $temp['valeur_residuelle'];
			$interest_rate = $temp['taux_interet'];
			$annual_cost = floatval($maintenance_cost)+ floatval($operating_cost)+ floatval($repair_cost) + ((floatval($investment_cost) - floatval($residual_value))/floatval($no_of_years))+((floatval($investment_cost) + floatval($residual_value))*floatval($interest_rate)*0.005);
			
		//	$annual_cost = $temp['cout_annuel'];
			
			$query .= "UPDATE ".$workspace.".cost_values SET mode= '$mode', cout_investissement = $investment_cost, cout_exploitation = $operating_cost, 
				cout_entretien = $maintenance_cost, cout_reparation = $repair_cost, periode_ans = $no_of_years, valeur_residuelle = $residual_value, taux_interet = $interest_rate, cout_annuel = $annual_cost
				WHERE id = $id;";						
		}
		
		If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
		else {			
			Echo '{success:true,message:"The cost values have been updated!"}';
		}	
	}
	
?>	
		