/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */

/**
 * @requires plugins/Tool.js
 * @requires Ext.ux.util.js
 */

/** api: (define)
 *  module = riskgis.plugins
 *  class = RiskScenarios
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("riskgis.plugins");

/** api: constructor
 *  .. class:: RiskScenarios(config)
 *
 *    Provides action for viewing the calculated risk scenarios and its respective parameters of the calculation.
 */
riskgis.plugins.RiskScenarios = Ext.extend(gxp.plugins.Tool, {
    
    /** api: ptype = riskgis_RiskScenarios */
    ptype: "riskgis_RiskScenarios", 
	
	/** api: config[riskSceMenuText]
     *  ``String``
     *  Text for the menu item (i18n).
     */
    riskSceMenuText: "View Scenarios",

    /** api: config[riskSceActionTip]
     *  ``String``
     *  Text for the action tooltip (i18n).
     */
    riskSceActionTip: "View the list of calculated risk scenarios and their respective parameters used in the calculation",
	
	addActions: function() {
		var actions = riskgis.plugins.RiskScenarios.superclass.addActions.apply(this, [{
            menuText: this.riskSceMenuText,
            iconCls: "riskgis-icon-sceList",
            tooltip: this.riskSceActionTip,
            handler: function() {
				
				this.riskSceStore = new Ext.data.GroupingStore({
					url: 'riskScenarioInfo.php',
					baseParams: {
						task: 'load',
						ws: 'riskgis_two',
						userID: userid,
						userRole: role
					},
					sortInfo:{field: 'id', direction: "ASC"},
					groupField:(role == 'admin')? 'nom_utilisateur':'option',
					autoLoad: true,
					reader: new Ext.data.JsonReader({
						totalProperty : 'totalCount',
						root          : 'rows',
						successProperty: 'success',
						idProperty    : 'idd',
						fields: [
							{name : 'id', type : 'int'},
							{name : 'nom', type : 'String'},
							{name : 'description', type : 'String'},
							{name : 'alt_nom', type : 'String'},
							{name : 'option', type : 'String'},
							{name : 'processus_type', type : 'String'},
							{name : 'processus_nom', type : 'String'},
							{name : 'objet_nom', type : 'String'}, 
							{name : 'objet_type', type : 'String'},
							{name : 'prix_surface', type : 'String'},
							{name : 'nombre_personnes', type : 'String'},
							{name : 'exp_personnes', type : 'String'},
							{name : 'prix_personne', type : 'String'},
							{name : 'vul_parametres', type : 'String'},
							{name : 'indice', type : 'String'},
							{name : 'id_utilisateur', type : 'int'},
							{name : 'nom_utilisateur', type : 'String'},
							{name : 'poly_texte', type : 'String'}
						] 
					})
				});
				
				var expander = new Ext.grid.RowExpander({
					tpl : new Ext.Template(
						'<p><b>Description:</b> {description}</p>'
					)
				});
				
				this.riskSceGrid = new Ext.grid.GridPanel({
					store: this.riskSceStore,
					colModel: new Ext.grid.ColumnModel({
						defaults: {	
							sortable: false,
							menuDisabled: (role == 'admin')? false:true
						},
						columns: [
						//	new Ext.grid.RowNumberer(),
							expander,
							{header: "ID", dataIndex: 'id', hidden: true},
							{header: "Name", dataIndex: 'nom'},
							{header: "Description", dataIndex: 'description', hidden: true},	
							{header: "Alternative", dataIndex: 'alt_nom'},	
							{header: "Option", dataIndex: 'option', renderer: function(value, metaData, record, rowIndex, colIndex, store) {							 
							  if (value == 'L') return 'Layer-based';							
							  else return 'Zone-based';
							}},
							{header: "Hazard Type", dataIndex: 'processus_type'},	
							{header: "Object Type", dataIndex: 'objet_type'},
							{header: "Mapping Index", dataIndex: 'indice', hidden: true},
							{header: "Polygon Area", dataIndex: 'poly_texte', hidden: true},
							{header: "User ID", dataIndex: 'id_utilisateur', hidden: true},
							{header: "User Name", dataIndex: 'nom_utilisateur', hidden: true},
							{
								xtype: 'actioncolumn',
								items: [
									{
										icon: 'theme/app/img/parameter.png',  
										tooltip: 'View parameters',								
										handler: function(grid, rowIndex, colIndex) {
											var rec = grid.getStore().getAt(rowIndex);
											new Ext.Window({
												title: "Parameter configuration",
												layout: 'fit',										
												plain:false,
												autoHeight: true,
												modal: true,
												maximizable: false,
												collapsible: false,
												closeable: true,
												closeAction:'hide',
												items: [
													new Ext.grid.PropertyGrid({
														title: rec.get('nom'),
														autoHeight: true,
														width: 300,
														source: {
															"Alternative Name": rec.get('alt_nom'),
															"Option": rec.get('option'),
															"Hazard Type": rec.get('processus_type'),
															"Hazard Name":  rec.get('processus_nom'),
															"Object Type": rec.get('objet_type'),
															"Object Name": rec.get('objet_nom'),
															'Price (per m2)': rec.get('prix_surface'),
															'People (no.)': rec.get('nombre_personnes'),
															'People Exposition (hours)': rec.get('exp_personnes'),
															'People (CHF)': rec.get('prix_personne'),
															'Vulnerability Configuration': rec.get('vul_parametres'),
															'Zone Area': rec.get('poly_texte')
														},
														viewConfig : {
															forceFit: true,
															scrollOffset: 2 // the grid will never have scrollbars
														}
													})
												]											
											}).show();							
											
										}
									},{
										icon: 'theme/app/img/chart_bar.png',  
										tooltip: 'View the summary information',	
										handler: function(grid, rowIndex, colIndex) {
											// view summary information of each selected risk scenarios 
											var rec = grid.getStore().getAt(rowIndex);
											var layerName = rec.get('indice');
											app.tools.riskanalysis.viewSummary(layerName);									
										}
									},{
										icon: 'theme/app/img/shape_handles.png',  
										tooltip: 'Visualize the defined zone in the map',	
										handler: function(grid, rowIndex, colIndex) {
											// extract coordinates and show the feature polygon 
											var rec = grid.getStore().getAt(rowIndex);
											if (rec.get('option') == 'L') {
												Ext.Msg.alert('Information', 'This scenario is a layer-based scenario and therefore, the defined zone cannot be visualized!');
											}
											else {
												var polyStr = rec.get('poly_texte');
												var feature = new OpenLayers.Format.WKT().read(polyStr);
												var layer = new OpenLayers.Layer.Vector("Area_layer_" + rec.get('nom'));										
												app.mapPanel.map.addLayer(layer);										
												layer.addFeatures([feature]);										
											}									
										}
									},{
										icon: 'src/gxp/theme/img/silk/map.png',  
										tooltip: 'Visualize the selected risk scenario in the map',								
										handler: function(grid, rowIndex, colIndex) {
											// to load the layer to the map 
											var rec = grid.getStore().getAt(rowIndex);
											var lyr_name = rec.get('indice');
											var names = {};
											names[lyr_name] = true; 
												
											var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
											app.tools.addlyrs.setSelectedSource(source);																		
											app.tools.addlyrs.selectedSource.store.load({
												callback: function(records, options, success) {
													var gridPanel, sel;
														if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
															gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
															sel = gridPanel.getSelectionModel();
															sel.clearSelections();
														}
														// select newly added layers
														var newRecords = [];
														var last = 0;
														app.tools.addlyrs.selectedSource.store.each(function(record, index) {
															if (record.get("name") in names) {
																last = index;
																newRecords.push(record);
															}
														});
														if (gridPanel) {
															// this needs to be deferred because the 
															// grid view has not refreshed yet
															window.setTimeout(function() {
																sel.selectRecords(newRecords);
																gridPanel.getView().focusRow(last);
															}, 100);
														} else {
															app.tools.addlyrs.addLayers(newRecords, true);
														}                                                                                    
												},
												scope: this                                            
											});
										}
									},{
										icon: 'theme/app/img/table_edit.png',  
										tooltip: 'Edit Information',	
										getClass: function(v, meta, rec) {          
											if ( role != 'admin') {
												if (userid != rec.get('id_utilisateur')) {
													return 'x-hide-display';
												}
											}
										},
										handler: function(grid, rowIndex, colIndex) {									
											var rec = grid.getStore().getAt(rowIndex);
											var riskSceEditWin = new Ext.Window({
													title: 'Edit the risk scenario layer information..',
													layout: 'form',
													autoHeight: true,
													plain:false,
													modal: true,
													resizable: false,
													closeable: true,
													closeAction:'hide',
													buttonAlign:'right',
													items: new Ext.form.FormPanel({
														labelWidth: 100, // label settings here cascade unless overridden
														itemId: 'riskSceEditForm',
														frame:true,			
														bodyStyle:'padding:5px 5px 0',
														width: 350,	
														autoHeight:true,		
														defaults: {
															anchor: '100%'
														},
														items: [{
															fieldLabel: 'Name/Title',
															xtype: 'textfield',
															name: 'name',
															allowBlank: false,
															value: rec.get('nom')
														},{
															fieldLabel: 'Description',
															xtype: 'textarea',
															name: 'description',
															value: rec.get('description')
														}],
														buttons: [{
															text: 'Update',
															icon:  'theme/app/img/table_save.png',
															handler: function(){
																var form = riskSceEditWin.getComponent('riskSceEditForm').getForm();				
																if (form.isValid()) {
																	form.submit({
																		url: 'riskScenarioInfo.php',
																		params: {
																			task: 'edit',
																			ws: 'riskgis_two',
																			mpIndex: rec.get('indice'),
																			ID: rec.get('id')
																		},
																		waitMsg : 'Please wait...',
																		success: function(form, action) {
																			Ext.Msg.alert('Success', action.result.message);
																			riskSceEditWin.close();
																			grid.getStore().reload({ // refresh the grid view
																				callback: function(){
																					grid.getView().refresh();
																				}
																			});
																		},
																		failure: function(form, action) {
																			Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
																		}
																	});
																}
																else {
																	Ext.Msg.alert('Validation Error', 'The form is not valid!' );
																}
															}
														},{
															text: 'Cancel',
															icon:  'src/gxp/theme/img/decline.png',
															handler: function(){
																riskSceEditWin.close();
															}
														}]
													})											
												});
												
												riskSceEditWin.show();
										}
									}
								]
							}]
						}),
						view: new Ext.grid.GroupingView({				
							forceFit: true,
							groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
						}),						
						frame:true,
						stripeRows: true,
						loadMask: true,
						width: 700,
						height: 450,						
						plugins:[expander,
							new Ext.ux.grid.Search({
								iconCls:'gxp-icon-zoom-to',
								disableIndexes:['id','indice','poly_texte','id_utilisateur'],
								minChars:2,
								autoFocus:true,
								width: 150,
								mode: 'local'
						})],
						bbar: [{
							xtype: 'tbtext',
							id: 'listSceTotalRec',
							text: 'Loading ...'
						},'->',{
							text: 'Delete',
							iconCls: 'delete',
							handler: function(){
								// delete the record from store, db, geoserver (if not admin, user can only delete the ones he/she created)
								Ext.Msg.show({
									title:'Delete the selected risk scenarios?',
									msg: 'Are you sure to delete the selected risk scenarios? This action is undoable, and it will also delete associated records if these risk scenarios are being used in cost-benefit calculation!',
									buttons: Ext.Msg.YESNOCANCEL,
									fn: function(buttonId, text, opt){
										if (buttonId == 'yes'){
											var records = this.riskSceGrid.getSelectionModel().getSelections();
											var ids = [];
											for (var i=0;i<records.length;i++) {
											if (records[i].get('id_utilisateur') == userid || role == 'admin'){ // if not admin, user can only delete the ones he/she created
													ids.push(records[i].data);
												}
												else {										
													Ext.Msg.alert('Information', 'You can only delete the layers you created, please exclude others from the list of selected records and try again!');
													return;
												}
											}
											
											Ext.Ajax.request({   
												url: 'riskScenarioInfo.php',
												params: {
													task: 'delete',
													ws: 'riskgis_two',
													IDs: Ext.encode(ids)
												},
												success: function(response, opts){
													var obj = Ext.decode(response.responseText);										
													Ext.Msg.alert('Information', obj.message);	
													// reload the store and refresh
													this.riskSceGrid.getStore().reload({ 
															callback: function(){
																this.riskSceGrid.getView().refresh();
															}
														});
												},
												failure: function(response, opts){
													var obj = Ext.decode(response.responseText);										
													Ext.Msg.alert('Failed', obj.message);
												}
											});
										}
									}
								});
							}
						}]						 
					});
				
				this.riskSceGrid.getStore().on('load', 					
					function (store, records, options) {
						Ext.getCmp('listSceTotalRec').setText('Total no. of risk scenarios: ' + records.length.toString());
					//	this.getBottomToolbar().get(0).setText('Total count: ' + records.length.toString());
					}
				);				
									
				this.riskSceInfoWin = new Ext.Window({
					title: 'Calculated risk scenarios information',
					layout: 'fit',
					plain:false,
					maximizable: true,
					collapsible: true,
					closeable: true,
					closeAction:'hide',
					buttonAlign:'right',
					items: [this.riskSceGrid],
					tools:[{
							id:'help',
							qtip: 'Get Help',
							handler: function(event, toolEl, panel){
								Ext.Msg.show({
									title:'Help Information',
									msg: '<b>Risk</b> is defined by <a href="./Einstein_1988.pdf" target="_blank">Einstein</a> (1988, p. 1076) as  “the multiplication of hazard and potential worth of loss since the same hazard can lead to entirely different consequences depending on the use of the affected terrain risk". For management purposes, a comprehensive and accurate risk assessment is necessary considering its essential role in risk management. Risk assessment should be able to support the decision-making and risk treatment process for the effectiveness of risk management. There are several assessment methods (qualitative, quantitative and semiquantitative) depending on the study scale, data availability and aims of the analysis.' + '<br><br>' + 'To learn more about the risk concepts and approaches applied in Switzerland, read the documents <a href="./Brundl_2013.pdf" target="_blank">here</a> and <a href="./Brundl_2009.pdf" target="_blank">here</a>.' + '<br><br>' + 'In this exercise, for the risk calculation, default object parameter and vulnerability values are taken from a tool called <a href="https://econome.ch/eco_work/index.php?PHPSESSID=94jep0nm74akn4qru80inier17" target="_blank">EconoMe</a>, assuming that buildings fall into the category of individual houses (exclusively for the residential use, such as villas and chalets).' + '<br><br>' + 'In this platform, different types of risk can be calculated (risk for objects, risk for persons, individual risk and risk total) based on the same principles establised in <a href="https://econome.ch/eco_work/index.php?PHPSESSID=94jep0nm74akn4qru80inier17&linkid=1" target="_blank">EconoMe</a> and <a href="https://www.vd.ch/index.php?id=61124" target="_blank">Valdorisk</a>.',
									buttons: Ext.Msg.OK,															   
									animEl: 'elId',
									icon: Ext.MessageBox.INFO
								});						
							}								
						},{
							id:'search',
							qtip: 'Play Video',
							handler: function(event, toolEl, panel){
								vid = new Ext.Window({
									title: 'Video Tutorial: risk estimation',
									resizable: false,
									html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xr8P1IA2j84?rel=0&amp;start=185" frameborder="0" allowfullscreen></iframe>'
								});
								
								vid.show();
							}
						}]
				});
				
				this.riskSceInfoWin.show();
			}
		}]);
	}

});	

Ext.preg(riskgis.plugins.RiskScenarios.prototype.ptype, riskgis.plugins.RiskScenarios);

	/* new Ext.ux.form.SearchField({
		store: this.riskSceStore,
		width: 150,
		onTrigger2Click: function(evt) {
			var text = this.getRawValue();
			if (text.length < 1) {
				this.onTrigger1Click();
				return;
			}
										 
			this.store.filter([
				{
					fn: function(item) {								
						return item.get('nom').match(text);  
					}
				}
			]);
		}
	}); */
	