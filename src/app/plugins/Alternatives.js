/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */
 
/**
 * @require plugins/Tool.js
 */
 
 /** api: (define)
 *  module = riskgis.plugins
 *  class = Alternatives
 */
 
 /** api: (extends)
 *  plugins/Tool.js
 */

Ext.ns("riskgis.plugins");

/** api: constructor
 *  .. class:: Alternatives(config)
 *
 *    Plugin for creating and viewing of alternative scenarios.
 *    
 */

riskgis.plugins.Alternatives = Ext.extend(gxp.plugins.Tool, {

	/** api: ptype = riskgis_Alternatives */
	ptype: "riskgis_Alternatives",
	
	/** api: config[altMenuText]
     *  ``String``
     *  Text for the menu item (i18n).
     */
    altMenuText: "Alternative",

    /** api: config[altActionTip]
     *  ``String``
     *  Text for the tooltip (i18n).
     */
    altActionTip: "Tool to create new alternative scenarios and view the list of existing scenarios",
	
	addActions: function() {
		var actions = riskgis.plugins.Alternatives.superclass.addActions.apply(this, [			 						
		{
			menuText: this.altMenuText,
            iconCls: "riskgis-icon-alternative",
            tooltip: this.altActionTip,
			scope: this,
            handler: function(args) {
				if (args == 'create') this.createLayer();
				else this.listLayers();
			}			
		}
		]);
	},
	
	/** api: method[createLayer]
     */
	createLayer: function() {
		var altAddForm = new Ext.form.FormPanel({
			labelWidth: 75, // label settings here cascade unless overridden
			url:'altInfo.php',
			frame:true,			
			bodyStyle:'padding:5px 5px 0',
			width: 450,	
			autoHeight:true,		
			items: [{
				xtype:'fieldset',
				title: 'Alternative Scenario Information',				
				collapsible: true,
				defaultType: 'textfield',
				defaults: {
					anchor: "90%",					
					msgTarget: "side"
				},
				collapsed: false,
				items :[{
						fieldLabel: 'Name',
						name: 'name',
						emptyText: 'Name of the alternative scenario ...',
						allowBlank: false
					},{
						fieldLabel: 'Description',
						xtype: 'textarea',
						emptyText: 'Description of the alternative scenario ...',
						name: 'description',
						allowBlank: true
					},{
						fieldLabel: 'Option',
						name: 'option',
						hiddenName: 'option',
						xtype: 'combo',
						itemId: 'alt_option',
						mode: "local",
						emptyText: 'Select the option to create ...',
						editable: false,
						allowBlank: false,
						store: new Ext.data.ArrayStore({
								id: 0,
								fields: [
									'myId',  // numeric value is the key
									'displayText'
								],
								data: [['Sketch', 'Sketch (mapping of measures)'], ['Descriptive', 'Non-sketch (non-mapping of measures)']]  // data is local
						}),
						valueField: 'myId',
						displayField: 'displayText',
						triggerAction: 'all',
						listeners: {
							select: function(combo, record, index) {
								if (combo.getValue() == 'Sketch') {
									Ext.getCmp('prelim_sketch').setVisible(true);
									Ext.getCmp('prelim_sketch').items.items[0].items.items[0].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).left);
									Ext.getCmp('prelim_sketch').items.items[0].items.items[1].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).bottom);
									Ext.getCmp('prelim_sketch').items.items[1].items.items[0].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).right);
									Ext.getCmp('prelim_sketch').items.items[1].items.items[1].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).top);
								}
								else {
									Ext.getCmp('prelim_sketch').setVisible(false);
								}
							},
							scope: this
						}
					},{
						fieldLabel: 'Measures',
						name: 'measures',
						hiddenName: 'measures',
						xtype: 'lovcombo',
						mode: "local",
						emptyText: 'Select the measures ...',
						editable: false,
						allowBlank: false,
						hideOnSelect:false,
						store: new Ext.data.ArrayStore({
								id: 0,
								fields: [
									'myId',  // numeric value is the key
									'displayText'
								],
								data: [[1, 'Galerie'], [2, 'Tunnel'],[3,'Galerie de protection contre les crues'],[4,'Ouvrage de stabilisation de la neige'],
								[5,'Installation de minage'],[6,'Filet de protection contre les chutes de pierres'],[7,'Digue en terre'],[8,'Ouvrage temporaire/ouvrage en bois'],
								[9,'Ouvrage de stabilisation des pentes'],[10,'Reboisements'],[11,'Barrage de correction torrentielle en bois'],[12,'Barrage de correction torrentielle en beton'],
								[13,'Filets de protection contre les laves torrentielles'],[14,'Rateliers en bois'],[15,'Barrage sur rivere de plaine et depotoir a alluvions en beton'],
								[16,'Maintient de la foret protectrice'],[17,"Mesures a l'objet"],[18,'Autre']]  // data is local
						}),
						valueField: 'myId',
						displayField: 'displayText',
						triggerAction: 'all'
					},{
						fieldLabel: 'Remarks',
						xtype: 'textarea',
						emptyText: 'If any other remarks (optional)',
						name: 'remarks',
						allowBlank: true
					}]
			},{
				xtype:'fieldset',
				id: 'prelim_sketch',
				title: 'Preliminary sketch option (WGS84)',
				labelWidth: 120,
				hidden: true,
				defaults: {
					anchor: "90%",
					msgTarget: "side"
				},
				collapsed: false,
				items:[{
					fieldLabel: 'BBox(minx,miny)',					
					xtype : 'compositefield',					
					items: [{
						xtype: 'numberfield',
						name: 'LatLon_minx',
						flex: 1,
						emptyText: 'LatLon value for minx'
					},{
						xtype: 'numberfield',
						name: 'LatLon_miny',
						flex: 1,
						emptyText: 'LatLon value for miny'
					}]
				},{
					fieldLabel: 'BBox(maxx,maxy)',
					xtype : 'compositefield',
					items: [{
						xtype: 'numberfield',
						name: 'LatLon_maxx',
						flex: 1,
						emptyText: 'LatLon value for maxx'
					},{
						xtype: 'numberfield',
						name: 'LatLon_maxy',
						flex: 1,
						emptyText: 'LatLon value for maxy'
					}]					
				}]
			}],
			buttons: [{
				text: 'Add',
				icon:  'src/gxp/theme/img/silk/map_add.png',
				handler: function(){
					//creating a new table and publishing it to the geoserver
					var form = altAddForm.getForm();		
					var fields = form.getFieldValues();
					if (form.isValid()) {			
						form.submit({
							url: 'altInfo.php',
							params: {
								task: 'add',
								ws: 'riskgis_two',
								userID: userid
							},
							waitMsg : 'Please wait...',
							success: function(form, action) {						
								Ext.Msg.alert('Success', action.result.message); // show the success message	
								form.reset(); // reset the form
								Ext.getCmp('prelim_sketch').setVisible(false);	
								
								// add the layer to the map	
								var lyr_name = action.result.mpIndex;
								var names = {};
								names[lyr_name] = true; 
								app.tools.addlyrs.selectedSource.store.load({
									callback: function(records, options, success) {
										var gridPanel, sel;
											if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
												gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
												sel = gridPanel.getSelectionModel();
												sel.clearSelections();
											}
											// select newly added layers
											var newRecords = [];
											var last = 0;
											app.tools.addlyrs.selectedSource.store.each(function(record, index) {
												if (record.get("name") in names) {
													last = index;
													newRecords.push(record);
												}
											});
											if (gridPanel) {
												// this needs to be deferred because the 
												// grid view has not refreshed yet
												window.setTimeout(function() {
													sel.selectRecords(newRecords);
													gridPanel.getView().focusRow(last);
												}, 100);
											} else {
												app.tools.addlyrs.addLayers(newRecords, true);
											}                                                                                    
									},
									scope: this                                            
								});								
							},
							// If you don't pass success:true, it will always go here
							failure: function(form, action) {
								Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
							}	
						});		
					}
					else {
						Ext.Msg.alert('Validation Error', 'The form is not valid!' );
					}
				}
			},{
				text: 'Reset',
				icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
				handler: function(){
					// reset the form and close the dialog box 
					altAddForm.getForm().reset();
					Ext.getCmp('prelim_sketch').setVisible(false);	
				}
			}]	
		});
		
		var altAddWin = new Ext.Window({
			title: 'Add a new alternative scenario..',
			layout: 'form',
			autoHeight: true,
			plain:false,
			modal: true,
			resizable: false,
			closeable: true,
			buttonAlign:'right',
			items: [altAddForm],
			tools:[
					{
						id:'help',
						qtip: 'Get Help',
						handler: function(event, toolEl, panel){
							Ext.Msg.show({
								title:'Help Information',
								msg: 'Potential risks can be reduced based on the contributing factors such as hazard, exposure of elements-at-risk and vulnerability through the implementation of effective risk management strategies. <b>Structural measures</b> are defined as “any physical construction to reduce or avoid possible impacts of hazards, or application of engineering techniques to achieve hazard-resistance and resilience in structures or systems” (<a href="http://www.unisdr.org/files/7817_UNISDRTerminologyEnglish.pdf" target="_blank">UNISDR</a>, p. 28). <b>Non-structural measures</b> are “any measures not involving physical construction that uses knowledge, practice or agreement to reduce risks and impacts, in particular through policies and laws, public awareness raising, training and education” (<a href="http://www.unisdr.org/files/7817_UNISDRTerminologyEnglish.pdf" target="_blank">UNISDR</a>, p. 28).' + '<br><br>' + 'Examples of structural mitigation measures are dikes, dams, slope stabilizations, hazard-resistant constructions, maintenance and planning of defense works. Non-structural measures concern non-physical actions such as insurance, relocation, land planning, early-warning systems, etc. <br><br>To learn more about possible mitigation measures, for example, in the case of debris flow, check this <a href="./Huebl&Fiebiger_2005.pdf" target="_blank">document</a> of Huebl & Fiebiger (2005).',
								buttons: Ext.Msg.OK,															   
								animEl: 'elId',
								icon: Ext.MessageBox.INFO
							});						
						}
				},	{
						id:'search',
						qtip: 'Play Video',
						handler: function(event, toolEl, panel){
							vid = new Ext.Window({
								title: 'Video Tutorial: formulation of alternative (risk reduction) scenario',
								resizable: false,
								html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xr8P1IA2j84?rel=0&amp;start=242" frameborder="0" allowfullscreen></iframe>'
							});
								
							vid.show();
						}
				}]
		});
		
		altAddWin.show();		
	},
	
	/** api: method[listLayers]
     */
	listLayers: function() {
		var xg = Ext.grid;		
		var altInfoStore = new Ext.data.GroupingStore({
				url: 'altInfo.php',
				baseParams: {
					task: 'loadGrid',
					ws: 'riskgis_two',
					userID: userid,
					userRole: role
				},
				sortInfo:{field: 'id', direction: "ASC"},
				groupField:(role == 'admin')? 'nom_utilisateur':'categorie',
				autoLoad: true,
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [					
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'}, 
						{name : 'categorie', type : 'String'}, 
						{name : 'mesures_nom', type : 'String'}, 
						{name : 'mesures_id', type : 'String'}, 
						{name : 'id_utilisateur', type : 'int'},
						{name : 'nom_utilisateur', type : 'String'},
						{name : 'indice', type : 'String'},
						{name : 'remarques', type : 'String'}
					] 
				})
		});	
		
		var expander = new Ext.grid.RowExpander({
			tpl : new Ext.Template(
				'<p><b>Description:</b> {description}</p>',
				'<p><b>Remarks:</b> {remarques}</p>'
			)
		});

		var altInfoGrid = new xg.GridPanel({
			view: new Ext.grid.GroupingView({
				markDirty: false
			}),
			store: altInfoStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					expander,
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description', hidden: true},
					{header: "Option", dataIndex: 'categorie', hidden: true},					
					{header: "Measures", dataIndex: 'mesures_nom'},	
					{header: "Measures ID", dataIndex: 'mesures_id', hidden: true},	
					{header: "User ID", dataIndex: 'id_utilisateur', hidden: true},
					{header: "User Name", dataIndex: 'nom_utilisateur', hidden: true},
					{header: "Remarks", dataIndex: 'remarques', hidden: true},
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [						
							{
								icon: 'theme/app/img/table_edit.png',  
								tooltip: 'Edit Information',	
								getClass: function(v, meta, rec) {          
									if ( role != 'admin') {
										if (userid != rec.get('id_utilisateur')) {
											return 'x-hide-display';
										}
									}
								},
								handler: function(grid, rowIndex, colIndex) {									
									var rec = grid.getStore().getAt(rowIndex);
									var altEditWin = new Ext.Window({
										title: 'Edit the alternative scenario information..',
										layout: 'form',
										autoHeight: true,
										plain:false,
										modal: true,
										resizable: false,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: new Ext.form.FormPanel({
											labelWidth: 100, // label settings here cascade unless overridden
											itemId: 'altEditForm',
											frame:true,			
											bodyStyle:'padding:5px 5px 0',
											width: 350,	
											autoHeight:true,		
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name/Title',
												xtype: 'textfield',
												name: 'name',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description',
												value: rec.get('description')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks',
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												icon:  'theme/app/img/table_save.png',
												handler: function(){
													var form = altEditWin.getComponent('altEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'altInfo.php',
															params: {
																task: 'edit',
																ws: 'riskgis_two',
																mpIndex: rec.get('indice'),
																ID: rec.get('id'),
																option: rec.get('categorie')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																altEditWin.close();
																grid.getStore().reload({ // refresh the grid view
																	callback: function(){
																		grid.getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											},{
												text: 'Cancel',
												icon:  'src/gxp/theme/img/decline.png',
												handler: function(){
													altEditWin.close();
												}
											}]
										})											
									});
												
									altEditWin.show();
								}
							},{
								icon: 'src/gxp/theme/img/silk/map.png',  
								tooltip: 'Visualize the selected alternative scenario',
								handler: function(grid, rowIndex, colIndex) {
									var rec = altInfoGrid.getStore().getAt(rowIndex);
									if (rec.get('categorie') == "Descriptive"){
										Ext.Msg.alert('Information', 'The selected alternative is a descriptive scenario and cannot be visualized on the map!');
									}
									else {
										// add the layer to the map	
										var lyr_name = rec.get('indice');
										var names = {};
										names[lyr_name] = true;
										
										var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
										app.tools.addlyrs.setSelectedSource(source);
										app.tools.addlyrs.selectedSource.store.load({
											callback: function(records, options, success) {
												var gridPanel, sel;
												if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
													gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
													sel = gridPanel.getSelectionModel();
													sel.clearSelections();
												}
												// select newly added layers
												var newRecords = [];
												var last = 0;
												app.tools.addlyrs.selectedSource.store.each(function(record, index) {
													if (record.get("name") in names) {
														last = index;
														newRecords.push(record);
													}
												});
												if (gridPanel) {
													// this needs to be deferred because the 
													// grid view has not refreshed yet
													window.setTimeout(function() {
														sel.selectRecords(newRecords);
														gridPanel.getView().focusRow(last);
													}, 100);
												} else {
													app.tools.addlyrs.addLayers(newRecords, true);
												}                                                                                    
											},
											scope: this                                            
										});	
									}
								},
								getClass: function(v, meta, rec) {                              
                                    return 'x-action-col-icon';
                                }
							},{
								icon: 'theme/app/img/money.png',  
								tooltip: 'Calculate the cost of the measures in the alternative scenarios',
								handler: function(grid, rowIndex, colIndex) {
									var rec = altInfoGrid.getStore().getAt(rowIndex);
									var id = rec.get('id');
									var sceName = rec.get('nom');
									app.tools.riskreduction.calculateCost(id, sceName);
								}
							}]
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: (role == 'admin')? false:true,
					width: 5
				}
			}),			
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			width: 700,
			height: 450,
			plugins:[expander,
				new Ext.ux.grid.Search({
					iconCls:'gxp-icon-zoom-to',
					disableIndexes:['id','indice','mesures_id', 'id_utilisateur'],
					minChars:2,
					autoFocus:true,
					width: 150,
					mode: 'local'
				})],
			bbar: [{
				xtype: 'tbtext',
				id: 'listAltTotalRec',
				text: 'Loading ...'
			},'->',{
				text: 'Delete',
				iconCls: 'delete',
				handler: function(){
					// delete the record from store, db, geoserver (if not admin, user can only delete the ones he/she created)
					Ext.Msg.show({
						title:'Delete the selected alternatives?',
						msg: 'Are you sure to delete the selected alternatives? This action is undoable, and it will also delete associated records if these alternatives are being used in risk, cost and cost-benefit calculation, including referenced hazard/object layers!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = altInfoGrid.getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
									if (records[i].get('id_utilisateur') == userid || role == 'admin'){ // if not admin, user can only delete the ones he/she created
										ids.push(records[i].data);
									}
									else {										
										Ext.Msg.alert('Information', 'You can only delete the layers you created, please exclude others from the list of selected records and try again!');
										return;
									}
								}
								
								Ext.Ajax.request({   
									url: 'altInfo.php',
									params: {
										task: 'delete',
										ws: 'riskgis_two',
										IDs: Ext.encode(ids)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										altInfoGrid.getStore().reload({ 
												callback: function(){
													altInfoGrid.getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]
		});
		
		altInfoGrid.getStore().on('load', 					
			function (store, records, options) {
				Ext.getCmp('listAltTotalRec').setText('Total no. of alternative scenarios: ' + records.length.toString());
			}
		);
		
		var altInfoWin = new Ext.Window({
			title: 'Alternative scenarios information',
			layout: 'fit',		
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: altInfoGrid,
			tools:[
					{
						id:'help',
						qtip: 'Get Help',
						handler: function(event, toolEl, panel){
							Ext.Msg.show({
								title:'Help Information',
								msg: 'Potential risks can be reduced based on the contributing factors such as hazard, exposure of elements-at-risk and vulnerability through the implementation of effective risk management strategies. <b>Structural measures</b> are defined as “any physical construction to reduce or avoid possible impacts of hazards, or application of engineering techniques to achieve hazard-resistance and resilience in structures or systems” (<a href="http://www.unisdr.org/files/7817_UNISDRTerminologyEnglish.pdf" target="_blank">UNISDR</a>, p. 28). <b>Non-structural measures</b> are “any measures not involving physical construction that uses knowledge, practice or agreement to reduce risks and impacts, in particular through policies and laws, public awareness raising, training and education” (<a href="http://www.unisdr.org/files/7817_UNISDRTerminologyEnglish.pdf" target="_blank">UNISDR</a>, p. 28).' + '<br><br>' + 'Examples of structural mitigation measures are dikes, dams, slope stabilizations, hazard-resistant constructions, maintenance and planning of defense works. Non-structural measures concern non-physical actions such as insurance, relocation, land planning, early-warning systems, etc. <br><br>To learn more about possible mitigation measures, for example, in the case of debris flow, check this <a href="./Huebl&Fiebiger_2005.pdf" target="_blank">document</a> of Huebl & Fiebiger (2005).',
								buttons: Ext.Msg.OK,															   
								animEl: 'elId',
								icon: Ext.MessageBox.INFO
							});		
					}
				},	{
						id:'search',
						qtip: 'Play Video',
						handler: function(event, toolEl, panel){
							vid = new Ext.Window({
								title: 'Video Tutorial: formulation of alternative (risk reduction) scenario',
								resizable: false,
								html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xr8P1IA2j84?rel=0&amp;start=242" frameborder="0" allowfullscreen></iframe>'
							});
								
							vid.show();
						}
				}]
		});
		
		altInfoWin.show();
	}
	
});	

Ext.preg(riskgis.plugins.Alternatives.prototype.ptype, riskgis.plugins.Alternatives);
	