/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */

/**
 * @requires plugins/Tool.js
 * @requires Ext.ux.util.js
 */

/** api: (define)
 *  module = riskgis.plugins
 *  class = Hazards
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("riskgis.plugins");

/** api: constructor
 *  .. class:: Hazards(config)
 *
 *    Provides action for creating and viewing the list of available hazard layers.
 */
riskgis.plugins.Hazards = Ext.extend(gxp.plugins.Tool, {
    
    /** api: ptype = riskgis_Hazards */
    ptype: "riskgis_Hazards", 
	
	/** api: config[hazardMenuText]
     *  ``String``
     *  Text for the menu item (i18n).
     */
    hazardMenuText: "Hazard",

    /** api: config[hazardActionTip]
     *  ``String``
     *  Text for the action tooltip (i18n).
     */
    hazardActionTip: "Tool to create new hazard layers and view the list of hazard layers",
	
	addActions: function() {
		var actions = riskgis.plugins.Hazards.superclass.addActions.apply(this, [			 						
		{
			menuText: this.hazardMenuText,
            iconCls: "riskgis-icon-hazard",
            tooltip: this.hazardActionTip,
			scope: this,
            handler: function(args) {
				if (args == 'create') this.createLayer();
				else this.listLayers();
			}			
		}
		]);
	},
	
	/** api: method[createLayer]
     */
	createLayer: function() {
		
		var hazAddForm = new Ext.form.FormPanel({
			labelWidth: 95, // label settings here cascade unless overridden
			url:'hazInfo.php',
			frame:true,	
			bodyStyle:'padding:5px 5px 0',
			width: 400,	
			autoHeight:true,		
			items: [{
				xtype:'fieldset',
				title: 'Hazard Layer Information',
				defaultType: 'textfield',
				defaults: {
					anchor: "90%",
					allowBlank: false,
					msgTarget: "side"
				},
				collapsible: true,
				autoHeight: true,
				items :[{
					fieldLabel: 'Name',
					name: 'haz_name',
					emptyText: 'Name of the hazard layer ...'
				},{
					fieldLabel: 'Description',
					xtype: 'textarea',
					name: 'haz_description',
					allowBlank: true,
					emptyText: 'Description of the hazard layer ...'
				},{
					fieldLabel: 'Remarks',
					xtype: 'textarea',
					name: 'haz_remarks',
					allowBlank: true,
					emptyText: 'Any remarks ...'
				},{
					fieldLabel: 'Return Period',
					name: 'haz_years',
					xtype: 'numberfield',
					emptyText: 'Return period of the hazard layer ...'
				},{
					fieldLabel: 'Hazard Type',
					name: 'haz_type',
					hiddenName: 'haz_type',
					xtype: "combo",
					mode: "local",
					editable: false,
					store: new Ext.data.ArrayStore({
						id: 0,
						fields: ['myValue','displayText'],
						data: [['LTO','Laves torrentielles']/* ,['INOD','Inondation dynamique'],['INOS','Inondation statique'] */]  // data is local
					}),
					valueField: 'myValue',
					displayField: 'displayText',
					emptyText: 'Select the hazard type ...',
					triggerAction: 'all'
				},{
					fieldLabel: 'Alternative',
					name: 'hazAltName',	
					hiddenName: 'hazAltID',
					editable: false,
					xtype: 'combo',					
					mode:'local',
					triggerAction: 'all',
					displayField: 'nom',
					valueField: 'id',
					store: new Ext.data.JsonStore({ 
						url: 'altInfo.php',										
						root: 'rows',
						fields : ['nom', 'id'],
						autoLoad: true,
						baseParams: {
							ws: 'riskgis_two',
							task: 'load',
							userID: userid,
							userRole: role
						}
					}),
					emptyText: 'Select the alternative scenario ...'
				},{
					fieldLabel: 'Option',
					name: 'option',
					hiddenName: 'option',
					xtype: 'combo',
					mode: "local",
					emptyText: 'Select the option to create ...',
					editable: false,
					store: new Ext.data.ArrayStore({
						id: 0,
						fields: [
							'myId',  // numeric value is the key
							'displayText'
						],
						data: [/* ['Sketch', 'Sketch a new layer'], */ ['Import', 'Import from an existing layer']]  // data is local
					}),
					valueField: 'myId',
					displayField: 'displayText',
					triggerAction: 'all',
					listeners: {
						select: function(combo, record, index) {
							if (combo.getValue() == 'Sketch') {
								Ext.getCmp('haz_import_name').setVisible(false);
								Ext.getCmp('haz_import_name').disable();
							 	Ext.getCmp('haz_sketch').setVisible(true);
								Ext.getCmp('haz_sketch').enable();
								Ext.getCmp('haz_sketch').items.items[0].items.items[0].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).left);
								Ext.getCmp('haz_sketch').items.items[0].items.items[1].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).bottom);
								Ext.getCmp('haz_sketch').items.items[1].items.items[0].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).right);
								Ext.getCmp('haz_sketch').items.items[1].items.items[1].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).top);
							}
							else {								
								Ext.getCmp('haz_sketch').setVisible(false);
								Ext.getCmp('haz_sketch').disable();
								Ext.getCmp('haz_import_name').setVisible(true);	
								Ext.getCmp('haz_import_name').enable();	
							}
						},
							scope: this
						}
				},{
					fieldLabel: 'Import Layer',
					name: 'haz_import_name',
					id: 'haz_import_name',
					hidden: true,
					hiddenName: 'haz_import_name',
					xtype: 'combo',			
					emptyText: 'Select the hazard layer ...',
					triggerAction: 'all',
					editable: false,
					mode:'local',
					triggerAction: 'all',
					displayField: 'nom',
					valueField: 'indice',
					store: new Ext.data.JsonStore({ //list all the available layers
						url: 'hazInfo.php',										
						root: 'rows',
						fields : ['nom', 'indice'],
						autoLoad: true,
						baseParams: {
							ws: 'riskgis_two',
							task: 'load',
							userID: userid,
							userRole: role
						}
					})									
				}]
			},{
				xtype:'fieldset',
				id: 'haz_sketch',
				title: 'Sketch option (WGS84)',
				labelWidth: 120,
				hidden: true,
				defaults: {
					anchor: "90%",
					msgTarget: "side"
				},
				collapsed: false,
				items:[{
					fieldLabel: 'BBox(minx,miny)',					
					xtype : 'compositefield',					
					items: [{
						xtype: 'numberfield',
						name: 'LatLon_minx',
						flex: 1,
						emptyText: 'LatLon value for minx'
					},{
						xtype: 'numberfield',
						name: 'LatLon_miny',
						flex: 1,
						emptyText: 'LatLon value for miny'
					}]
				},{
					fieldLabel: 'BBox(maxx,maxy)',
					xtype : 'compositefield',
					items: [{
						xtype: 'numberfield',
						name: 'LatLon_maxx',
						flex: 1,
						emptyText: 'LatLon value for maxx'
					},{
						xtype: 'numberfield',
						name: 'LatLon_maxy',
						flex: 1,
						emptyText: 'LatLon value for maxy'
					}]					
				}]
			}],
			buttons: [{
				text: 'Create',
				icon:  'src/gxp/theme/img/silk/map_add.png',
				handler: function(){
					var form = hazAddForm.getForm();				
					if (form.isValid()) {
						var fields = form.getFieldValues();					
						form.submit({
							url: 'hazInfo.php',
							waitMsg: "Creating the layer ...",
							waitMsgTarget: true,
							params: {
								task: 'add',
								ws: 'riskgis_two',
								userID: userid
							},
							success: function(form, action) {
								if (action.result.success) {
									Ext.Msg.alert('Success', action.result.message); // show the success message	
									form.reset(); // reset the form
									Ext.getCmp('haz_import_name').setVisible(false);	
									Ext.getCmp('haz_sketch').setVisible(false);	
									
									// add the layer to the map	
									var lyr_name = action.result.mpIndex;
									var names = {};
									names[lyr_name] = true; 
									
									var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
									app.tools.addlyrs.setSelectedSource(source);
									app.tools.addlyrs.selectedSource.store.load({
										callback: function(records, options, success) {
											var gridPanel, sel;
												if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
													gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
													sel = gridPanel.getSelectionModel();
													sel.clearSelections();
												}
												// select newly added layers
												var newRecords = [];
												var last = 0;
												app.tools.addlyrs.selectedSource.store.each(function(record, index) {
													if (record.get("name") in names) {
														last = index;
														newRecords.push(record);
													}
												});
												if (gridPanel) {
													// this needs to be deferred because the 
													// grid view has not refreshed yet
													window.setTimeout(function() {
														sel.selectRecords(newRecords);
														ridPanel.getView().focusRow(last);
													}, 100);
												} else {
													app.tools.addlyrs.addLayers(newRecords, true);
												}                                                                                    
										},
										scope: this                                            
									});
								}
								else Ext.Msg.alert('Failed', action.result.message);																			
							},
							// If you don't pass success:true, it will always go here
							failure: function(form, action) {
								Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
							},
							scope: this
						});			
					}
				}
			},{
				text: 'Reset',
				icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
				handler: function(){
					hazAddForm.getForm().reset();
					Ext.getCmp('haz_import_name').setVisible(false);	
					Ext.getCmp('haz_sketch').setVisible(false);					
				}
			}]	
		});				
							
		var hazAddWin = new Ext.Window({			
			title: 'Create a new hazard layer..',
			layout: 'fit',
			autoHeight: true,
			plain:false,
			modal: true,
			resizable: true,
			closeable: true,			
			buttonAlign:'right',
			items: [hazAddForm],
			tools:[
				{
					id:'help',
					qtip: 'Get Help',
					handler: function(event, toolEl, panel){
						Ext.Msg.show({
							title:'Help Information',
							msg: '<b>' + 'Intensity map' + '</b>' + ' indicates intensities of expected dangerous processes for different standard frequencies, such as flood depth and flow speed in case of flood. Intensities are estimated by the class of probability of occurrence (high, medium, low and very low) and do not consider the vulnerability of the objects present on the territory.' + '<br><br>' + 'The intensity maps are used for risk analysis, since they assess the frequency and intensity of an event. Intensity maps of different return periods are combined to form Swiss hazard maps, which are used by responsible authorities for managing risks. To learn more about Swiss hazard maps and their degrees of dangers for spatial planning, read the ' + '<a href="./BAFU_2015.pdf" target="_blank">document</a>.' + '<br><br>' + 'For more information about the basic notions linked to natural hazards and risk, read the ' + '<a href="./Vade-mecum.pdf" target="_blank">handbook</a> of Natural Hazards Unit, Directorate-General for the Environment, Canton of Vaud.',
							buttons: Ext.Msg.OK,															   
							animEl: 'elId',
							icon: Ext.MessageBox.INFO
						}); 
					}
				}]
		});
		
		hazAddWin.show();
	},

	/** api: method[listLayers]
     */
	listLayers: function() {
		var hazStore = new Ext.data.GroupingStore({
				url: 'hazInfo.php',
				baseParams: {
					task: 'load',
					ws: 'riskgis_two',
					userID: userid,
					userRole: role
				},				
				sortInfo:{field: 'id', direction: "ASC"},
				groupField:(role == 'admin')? 'nom_utilisateur':'groupe',
				autoLoad: true,
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'type', type : 'String'},
						{name : 'alt_nom', type : 'String'},
						{name : 'temp_de_retour', type : 'int'},
						{name : 'remarques', type : 'String'},
						{name : 'id_utilisateur', type : 'int'},
						{name : 'nom_utilisateur', type : 'String'},
						{name : 'nom_origine', type : 'String'},
						{name : 'indice', type : 'String'},
						{name : 'groupe', convert : function (v, rec){
							return rec.type + ', ' + rec.alt_nom;
						}}						
					] 
				})
		});	
		
		var expander = new Ext.grid.RowExpander({
			tpl : new Ext.Template(
				'<p><b>Description:</b> {description}</p>',
				'<p><b>Remarks:</b> {remarques}</p>'
			)
		});
		
		var hazGrid = new Ext.grid.GridPanel({
			store: hazStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [			
					expander,
					{header: "Group", dataIndex: 'groupe', hidden: true},
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Type", dataIndex: 'type'},					
					{header: "Layer Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description', hidden: true},
					{header: "Return Period", dataIndex: 'temp_de_retour'},
					{header: "For Alternative", dataIndex: 'alt_nom'},
					{header: "Remarks", dataIndex: 'remarques', hidden: true},		
					{header: "User ID", dataIndex: 'id_utilisateur', hidden: true},
					{header: "User Name", dataIndex: 'nom_utilisateur', hidden: true},
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Original Name", dataIndex: 'nom_origine', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
						{
							icon: 'src/gxp/theme/img/silk/map.png',  
							tooltip: 'Visualize the hazard map',								
							handler: function(grid, rowIndex, colIndex) {
								// to load the layer to the map 
								var rec = grid.getStore().getAt(rowIndex);
								var lyr_name = rec.get('indice');
								var names = {};
								names[lyr_name] = true;
										
								var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
								app.tools.addlyrs.setSelectedSource(source);
								app.tools.addlyrs.selectedSource.store.load({
									callback: function(records, options, success) {
										var gridPanel, sel;
										if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
											gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
											sel = gridPanel.getSelectionModel();
											sel.clearSelections();
										}
										// select newly added layers
										var newRecords = [];
										var last = 0;
										app.tools.addlyrs.selectedSource.store.each(function(record, index) {
											if (record.get("name") in names) {
												last = index;
												newRecords.push(record);
											}
										});
										if (gridPanel) {
											// this needs to be deferred because the 
											// grid view has not refreshed yet
											window.setTimeout(function() {
												sel.selectRecords(newRecords);
												gridPanel.getView().focusRow(last);
											}, 100);
										} else {
											app.tools.addlyrs.addLayers(newRecords, true);
										}                                                                                    
									},
									scope: this                                            
								});								
							}								
						},{
							icon: 'theme/app/img/table_edit.png',  
							tooltip: 'Edit Information',
							getClass: function(v, meta, rec) {          
								if ( role != 'admin') {
									if (userid != rec.get('id_utilisateur')) {
										return 'x-hide-display';
									}
								}
                            },
							handler: function(grid, rowIndex, colIndex) {									
								var rec = grid.getStore().getAt(rowIndex);
								var hazEditWin = new Ext.Window({
										title: 'Edit the hazard map information..',
										layout: 'form',
										autoHeight: true,
										plain:false,
										modal: true,
										resizable: false,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: new Ext.form.FormPanel({
											labelWidth: 100, // label settings here cascade unless overridden
											itemId: 'hazEditForm',
											frame:true,			
											bodyStyle:'padding:5px 5px 0',
											width: 350,	
											autoHeight:true,		
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name/Title',
												xtype: 'textfield',
												name: 'name',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Hazard Type',
												name: 'type',
												hiddenName: 'haz_type',
												xtype: "combo",
												mode: "local",
												editable: false,
												store: new Ext.data.ArrayStore({
													id: 0,
													fields: ['myValue','displayText'],
													data: [['LTO', 'Laves torrentielles']]  // data is local
												}),
												valueField: 'myValue',
												displayField: 'displayText',
												value: rec.get('type'),
												triggerAction: 'all',
												allowBlank: false
											},{
												fieldLabel: 'Return Period',
												xtype: 'numberfield',
												name: 'year',
												allowBlank: false,
												value: rec.get('temp_de_retour')
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description',
												value: rec.get('description')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks',
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												icon:  'theme/app/img/table_save.png',
												handler: function(){
													var form = hazEditWin.getComponent('hazEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'hazInfo.php',
															params: {
																task: 'edit',
																ws: 'riskgis_two',
																mpIndex: rec.get('indice'),
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																hazEditWin.close();
																grid.getStore().reload({ // refresh the grid view
																	callback: function(){
																		grid.getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											},{
												text: 'Cancel',
												icon:  'src/gxp/theme/img/decline.png',
												handler: function(){
													hazEditWin.close();
												}
											}]
										})											
									});
									
									hazEditWin.show();
							}
						}]
					}	
				],
				defaults: {
					sortable: true,
					menuDisabled: (role == 'admin')? false:true,
					width: 5
				}
			}),	
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			width: 700,
			height: 450,
			plugins:[expander,
				new Ext.ux.grid.Search({
					iconCls:'gxp-icon-zoom-to',
					disableIndexes:['id','groupe','indice','nom_origine','id_utilisateur'],
					minChars:2,
					autoFocus:true,
					width: 150,
					mode: 'local'
				})],
			bbar: [{
				xtype: 'tbtext',
				id: 'listHazTotalRec',
				text: 'Loading ...'
			},'->',{
				text: 'Delete',
				iconCls: 'delete',
				hidden: (role == 'admin')? false:true,
				handler: function(){
					// delete the record from store, db, geoserver (if not admin, user can only delete the ones he/she created)
					Ext.Msg.show({
						title:'Delete the selected hazard maps?',
						msg: 'Are you sure to delete the selected hazard maps? This action is undoable, and it will also delete associated records if these maps are being used in risk calculation!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = hazGrid.getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
									if (records[i].get('id_utilisateur') == userid || role == 'admin'){ // if not admin, user can only delete the ones he/she created
										ids.push(records[i].data);
									}
									else {										
										Ext.Msg.alert('Information', 'You can only delete the layers you created, please exclude others from the list of selected records and try again!');
										return;
									}
								}
								
								Ext.Ajax.request({   
									url: 'hazInfo.php',
									params: {
										task: 'delete',
										ws: 'riskgis_two',
										IDs: Ext.encode(ids)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										hazGrid.getStore().reload({ 
												callback: function(){
													hazGrid.getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]
		});
		
		hazGrid.getStore().on('load', 					
			function (store, records, options) {
				Ext.getCmp('listHazTotalRec').setText('Total no. of hazard layers: ' + records.length.toString());
			}
		);
				
		var hazInfoWin = new Ext.Window({
			title: 'Hazard layers information',
			layout: 'fit',
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: hazGrid,
			tools:[
					{
						id:'help',
						qtip: 'Get Help',
						handler: function(event, toolEl, panel){
							Ext.Msg.show({
								title:'Help Information',
								msg: '<b>' + 'Intensity map' + '</b>' + ' indicates intensities of expected dangerous processes for different standard frequencies, such as flood depth and flow speed in case of flood. Intensities are estimated by the class of probability of occurrence (high, medium, low and very low) and do not consider the vulnerability of the objects present on the territory.' + '<br><br>' + 'The intensity maps are used for risk analysis, since they assess the frequency and intensity of an event. Intensity maps of different return periods are combined to form Swiss hazard maps, which are used by responsible authorities for managing risks. To learn more about Swiss hazard maps and their degrees of dangers for spatial planning, read the ' + '<a href="./BAFU_2015.pdf" target="_blank">document</a>.' + '<br><br>' + 'For more information about the basic notions linked to natural hazards and risk, read the ' + '<a href="./Vade-mecum.pdf" target="_blank">handbook</a> of Natural Hazards Unit, Directorate-General for the Environment, Canton of Vaud.',
								buttons: Ext.Msg.OK,															   
								animEl: 'elId',
								icon: Ext.MessageBox.INFO
							});		
					}
				},{
						id:'search',
						qtip: 'Play Video',
						handler: function(event, toolEl, panel){
							vid = new Ext.Window({
								title: 'Video Tutorial: visualization of hazard and object layers',
								resizable: false,
								html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xr8P1IA2j84?rel=0&amp;start=26" frameborder="0" allowfullscreen></iframe>'
							});
							
							vid.show();
					}
				}]
		});
		
		hazInfoWin.show();
	}	

});	

Ext.preg(riskgis.plugins.Hazards.prototype.ptype, riskgis.plugins.Hazards);