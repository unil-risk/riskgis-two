/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */

/**
 * @requires plugins/Tool.js
 * @requires Ext.ux.util.js
 */

/** api: (define)
 *  module = riskgis.plugins
 *  class = Objects
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("riskgis.plugins");

/** api: constructor
 *  .. class:: Objects(config)
 *
 *    Provides action for creating and viewing the list of available objects (e.g. buildings) layers.
 */
riskgis.plugins.Objects = Ext.extend(gxp.plugins.Tool, {
    
    /** api: ptype = riskgis_Objects */
    ptype: "riskgis_Objects", 
	
	/** api: config[objectMenuText]
     *  ``String``
     *  Text for the menu item (i18n).
     */
    objectMenuText: "Object",

    /** api: config[objectActionTip]
     *  ``String``
     *  Text for the action tooltip (i18n).
     */
    objectActionTip: "Tool to create new object layers and view the list of existing object layers",
	
	addActions: function() {
		var actions = riskgis.plugins.Objects.superclass.addActions.apply(this, [			 						
		{
			menuText: this.objectMenuText,
            iconCls: "riskgis-icon-object",
            tooltip: this.objectActionTip,
			scope: this,
            handler: function(args) {
				if (args == 'create') this.createLayer();
				else this.listLayers();
			}			
		}
		]);
	},
	
	/** api: method[createLayer]
     */
	createLayer: function() {
		var objAddForm = new Ext.form.FormPanel({
			labelWidth: 95, // label settings here cascade unless overridden
			url:'objInfo.php',
			frame:true,	
			bodyStyle:'padding:5px 5px 0',
			width: 400,	
			autoHeight:true,		
			items: [{
				xtype:'fieldset',
				title: 'Object Layer Information',
				defaultType: 'textfield',
				defaults: {
					anchor: "90%",
					allowBlank: false,
					msgTarget: "side"
				},
				collapsible: true,
				autoHeight: true,
				items :[{
					fieldLabel: 'Name',
					name: 'obj_name',
					emptyText: 'Name of the object layer ...'
				},{
					fieldLabel: 'Description',
					xtype: 'textarea',
					name: 'obj_description',
					allowBlank: true,
					emptyText: 'Description of the object layer ...'
				},{
					fieldLabel: 'Remarks',
					xtype: 'textarea',
					name: 'obj_remarks',
					allowBlank: true,
					emptyText: 'Any remarks ...'
				},{
					fieldLabel: 'Object Type',
					name: 'obj_type',
					hiddenName: 'obj_type',
					xtype: "combo",
					mode: "local",
					editable: false,
					store: new Ext.data.ArrayStore({
						id: 0,
						fields: ['myValue','displayText'],
						data: [['Buildings', 'Buildings']]  // data is local
					}),
					valueField: 'myValue',
					displayField: 'displayText',
					emptyText: 'Select the object type ...',
					triggerAction: 'all'
				},{
					fieldLabel: 'Alternative',
					name: 'objAltName',	
					hiddenName: 'objAltID',
					editable: false,
					xtype: 'combo',					
					mode:'local',
					triggerAction: 'all',
					displayField: 'nom',
					valueField: 'id',
					store: new Ext.data.JsonStore({ 
						url: 'altInfo.php',										
						root: 'rows',
						fields : ['nom', 'id'],
						autoLoad: true,
						baseParams: {
							ws: 'riskgis_two',
							task: 'load',
							userID: userid,
							userRole: role
						}
					}),
					emptyText: 'Select the alternative scenario ...'
				},{
					fieldLabel: 'Option',
					name: 'option',
					hiddenName: 'option',
					xtype: 'combo',
					mode: "local",
					emptyText: 'Select the option to create ...',
					editable: false,
					store: new Ext.data.ArrayStore({
						id: 0,
						fields: [
							'myId',  // numeric value is the key
							'displayText'
						],
						data: [['Import', 'Import from an existing layer']]  // data is local
					}),
					valueField: 'myId',
					displayField: 'displayText',
					triggerAction: 'all',
					listeners: {
						select: function(combo, record, index) {
							if (combo.getValue() == 'Import') {
								Ext.getCmp('obj_import_name').setVisible(true);	
								Ext.getCmp('obj_import_name').enable();	
							}
							else {								
								
								
							}
						},
							scope: this
						}
				},{
					fieldLabel: 'Import Layer',
					name: 'obj_import_name',
					id: 'obj_import_name',
					hidden: true,
					hiddenName: 'obj_import_name',
					xtype: 'combo',			
					emptyText: 'Select the object layer ...',
					triggerAction: 'all',
					editable: false,
					mode:'local',
					triggerAction: 'all',
					displayField: 'nom',
					valueField: 'indice',
					store: new Ext.data.JsonStore({ //list all the available layers
						url: 'objInfo.php',										
						root: 'rows',
						fields : ['nom', 'indice'],
						autoLoad: true,
						baseParams: {
							ws: 'riskgis_two',
							task: 'load',
							userID: userid,
							userRole: role
						}
					})									
				}]
			}],
			buttons: [{
				text: 'Create',
				icon:  'src/gxp/theme/img/silk/map_add.png',
				handler: function(){
					var form = objAddForm.getForm();				
					if (form.isValid()) {
						var fields = form.getFieldValues();					
						form.submit({
							url: 'objInfo.php',
							waitMsg: "Creating the layer ...",
							waitMsgTarget: true,
							params: {
								task: 'add',
								ws: 'riskgis_two',
								userID: userid
							},
							success: function(form, action) {
								if (action.result.success) {
									Ext.Msg.alert('Success', action.result.message); // show the success message	
									form.reset(); // reset the form
									Ext.getCmp('obj_import_name').setVisible(false);	
									
									// add the layer to the map	
									var lyr_name = action.result.mpIndex;
									var names = {};
									names[lyr_name] = true; 
									
									var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
									app.tools.addlyrs.setSelectedSource(source);
									app.tools.addlyrs.selectedSource.store.load({
										callback: function(records, options, success) {
											var gridPanel, sel;
												if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
													gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
													sel = gridPanel.getSelectionModel();
													sel.clearSelections();
												}
												// select newly added layers
												var newRecords = [];
												var last = 0;
												app.tools.addlyrs.selectedSource.store.each(function(record, index) {
													if (record.get("name") in names) {
														last = index;
														newRecords.push(record);
													}
												});
												if (gridPanel) {
													// this needs to be deferred because the 
													// grid view has not refreshed yet
													window.setTimeout(function() {
														sel.selectRecords(newRecords);
														gridPanel.getView().focusRow(last);
													}, 100);
												} else {
													app.tools.addlyrs.addLayers(newRecords, true);
												}                                                                                    
										},
										scope: this                                            
									});
								}
								else Ext.Msg.alert('Failed', action.result.message);																			
							},
							// If you don't pass success:true, it will always go here
							failure: function(form, action) {
								Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
							},
							scope: this
						});			
					}
				}
			},{
				text: 'Reset',
				icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
				handler: function(){
					objAddForm.getForm().reset();
					Ext.getCmp('obj_import_name').setVisible(false);	
				}
			}]	
		});				
							
		var objAddWin = new Ext.Window({			
			title: 'Create a new object layer..',
			layout: 'fit',
			autoHeight: true,
			plain:false,
			modal: true,
			resizable: true,
			closeable: true,			
			buttonAlign:'right',
			items: [objAddForm],
			tools:[
				{
					id:'help',
					qtip: 'Get Help',
					handler: function(event, toolEl, panel){
						Ext.Msg.show({
							title:'Help Information',
							msg: 'In Switzerland, there exists a building database called ' + '<a href="https://www.bfs.admin.ch/bfs/en/home/registers/federal-register-buildings-dwellings.html" target="_blank">"Federal Register of Buildings and Dwellings (RBD)"</a>. It contains the basic data of each building (such as plot number, building address, location, year of construction/renovation, number of floors, rooms, surface housing and heating system, etc.) as well as some housing data (such as number of rooms, area, etc.). For the purpose of risk analysis, this data can be used, given information on building prices, occupancy of people, construction materials and so on. To learn more about this building data/information, check the building ' + '<a href="https://www.vd.ch/fileadmin/user_upload/organisation/dinf/sit/fichiers_pdf/Catalogue_caract%C3%A8res_RegBL_version_3.7.pdf" target="_blank">catalog</a> of the Canton of Vaud (pages:12-14) and the ' + '<a href="http://map.housing-stat.ch/" target="_blank">Buildings and Dwellings Portal</a> as an example.' + '<br><br>' + 'However, in this exercise, buildings data from ' + '<a href="https://www.openstreetmap.org" target="_blank">OpenStreetMap</a> are used for the simplicity of the exercise.',
							buttons: Ext.Msg.OK,															   
							animEl: 'elId',
							icon: Ext.MessageBox.INFO
						}); 
					}
				}]
		});
		
		objAddWin.show();		
	},

	/** api: method[listLayers]
     */
	listLayers: function() {
		var objStore = new Ext.data.GroupingStore({
				url: 'objInfo.php',
				baseParams: {
					task: 'load',
					ws: 'riskgis_two',
					userID: userid,
					userRole: role
				},
				sortInfo:{field: 'id', direction: "ASC"},
				groupField:(role == 'admin')? 'nom_utilisateur':'groupe',
				autoLoad: true,
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'type', type : 'String'},
						{name : 'alt_nom', type : 'String'},
						{name : 'remarques', type : 'String'},
						{name : 'id_utilisateur', type : 'int'},
						{name : 'nom_utilisateur', type : 'String'},
						{name : 'nom_origine', type : 'String'},
						{name : 'indice', type : 'String'},
						{name : 'groupe', convert : function (v, rec){
							return rec.type + ', ' + rec.alt_nom;
						}}						
					] 
				})
		});	
		
		var expander = new Ext.grid.RowExpander({
			tpl : new Ext.Template(
				'<p><b>Description:</b> {description}</p>',
				'<p><b>Remarks:</b> {remarques}</p>'
			)
		});
		
		var objGrid = new Ext.grid.GridPanel({
			store: objStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [			
					expander,
					{header: "Group", dataIndex: 'groupe', hidden: true},
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Type", dataIndex: 'type'},					
					{header: "Layer Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description', hidden: true},
					{header: "For Alternative", dataIndex: 'alt_nom'},
					{header: "Remarks", dataIndex: 'remarques', hidden: true},	
					{header: "User ID", dataIndex: 'id_utilisateur', hidden: true},
					{header: "User Name", dataIndex: 'nom_utilisateur', hidden: true},
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Original Name", dataIndex: 'nom_origine', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
						{
							icon: 'src/gxp/theme/img/silk/map.png',  
							tooltip: 'Visualize the object map',								
							handler: function(grid, rowIndex, colIndex) {
								// to load the layer to the map 
								var rec = grid.getStore().getAt(rowIndex);
								var lyr_name = rec.get('indice');
								var names = {};
								names[lyr_name] = true;
										
								var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
								app.tools.addlyrs.setSelectedSource(source);
								app.tools.addlyrs.selectedSource.store.load({
									callback: function(records, options, success) {
										var gridPanel, sel;
										if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
											gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
											sel = gridPanel.getSelectionModel();
											sel.clearSelections();
										}
										// select newly added layers
										var newRecords = [];
										var last = 0;
										app.tools.addlyrs.selectedSource.store.each(function(record, index) {
											if (record.get("name") in names) {
												last = index;
												newRecords.push(record);
											}
										});
										if (gridPanel) {
											// this needs to be deferred because the 
											// grid view has not refreshed yet
											window.setTimeout(function() {
												sel.selectRecords(newRecords);
												gridPanel.getView().focusRow(last);
											}, 100);
										} else {
											app.tools.addlyrs.addLayers(newRecords, true);
										}                                                                                    
									},
									scope: this                                            
								});								
							}								
						},{
							icon: 'theme/app/img/table_edit.png',  
							tooltip: 'Edit Information',	
							getClass: function(v, meta, rec) {          
								if ( role != 'admin') {
									if (userid != rec.get('id_utilisateur')) {
										return 'x-hide-display';
									}
								}
                            },
							handler: function(grid, rowIndex, colIndex) {									
								var rec = grid.getStore().getAt(rowIndex);
								var objEditWin = new Ext.Window({
										title: 'Edit the object map information..',
										layout: 'form',
										autoHeight: true,
										plain:false,
										modal: true,
										resizable: false,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: new Ext.form.FormPanel({
											labelWidth: 100, // label settings here cascade unless overridden
											itemId: 'objEditForm',
											frame:true,			
											bodyStyle:'padding:5px 5px 0',
											width: 350,	
											autoHeight:true,		
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name/Title',
												xtype: 'textfield',
												name: 'name',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Object Type',
												name: 'type',
												hiddenName: 'obj_type',
												xtype: "combo",
												mode: "local",
												editable: false,
												store: new Ext.data.ArrayStore({
													id: 0,
													fields: ['myValue','displayText'],
													data: [['Buildings', 'Buildings']]  // data is local
												}),
												valueField: 'myValue',
												displayField: 'displayText',
												value: rec.get('type'),
												triggerAction: 'all',
												allowBlank: false
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description',
												value: rec.get('description')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks',
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												icon:  'theme/app/img/table_save.png',
												handler: function(){
													var form = objEditWin.getComponent('objEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'objInfo.php',
															params: {
																task: 'edit',
																ws: 'riskgis_two',
																mpIndex: rec.get('indice'),
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																objEditWin.close();
																grid.getStore().reload({ // refresh the grid view
																	callback: function(){
																		grid.getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											},{
												text: 'Cancel',
												icon:  'src/gxp/theme/img/decline.png',
												handler: function(){
													objEditWin.close();
												}
											}]
										})											
									});
									
									objEditWin.show();
							}
						}]
					}	
				],
				defaults: {
					sortable: true,
					menuDisabled: (role == 'admin')? false:true,
					width: 5
				}
			}),	
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			width: 700,
			height: 450,
			plugins:[expander,
				new Ext.ux.grid.Search({
					iconCls:'gxp-icon-zoom-to',
					disableIndexes:['id','groupe','indice','nom_origine','id_utilisateur'],
					minChars:2,
					autoFocus:true,
					width: 150,
					mode: 'local'
				})],
			bbar: [{
				xtype: 'tbtext',
				id: 'listObjTotalRec',
				text: 'Loading ...'
			},'->',{
				text: 'Delete',
				iconCls: 'delete',
				hidden: (role == 'admin')? false:true,
				handler: function(){
					// delete the record from store, db, geoserver (if not admin, user can only delete the ones he/she created)
					Ext.Msg.show({
						title:'Delete the selected object maps?',
						msg: 'Are you sure to delete the selected object maps? This action is undoable, and it will also delete associated records if these maps are being used in risk calculation!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = objGrid.getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
								if (records[i].get('id_utilisateur') == userid || role == 'admin'){ // if not admin, user can only delete the ones he/she created
										ids.push(records[i].data);
									}
									else {										
										Ext.Msg.alert('Information', 'You can only delete the layers you created, please exclude others from the list of selected records and try again!');
										return;
									}
								}
								
								Ext.Ajax.request({   
									url: 'objInfo.php',
									params: {
										task: 'delete',
										ws: 'riskgis_two',
										IDs: Ext.encode(ids)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										objGrid.getStore().reload({ 
												callback: function(){
													objGrid.getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]
		});
		
		objGrid.getStore().on('load', 					
			function (store, records, options) {
				Ext.getCmp('listObjTotalRec').setText('Total no. of object layers: ' + records.length.toString());
			}
		);
				
		var objInfoWin = new Ext.Window({
			title: 'Object layers information',
			layout: 'fit',
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: objGrid,
			tools:[
					{
						id:'help',
						qtip: 'Get Help',
						handler: function(event, toolEl, panel){
							Ext.Msg.show({
							title:'Help Information',
							msg: 'In Switzerland, there exists a building database called ' + '<a href="https://www.bfs.admin.ch/bfs/en/home/registers/federal-register-buildings-dwellings.html" target="_blank">"Federal Register of Buildings and Dwellings (RBD)"</a>. It contains the basic data of each building (such as plot number, building address, location, year of construction/renovation, number of floors, rooms, surface housing and heating system, etc.) as well as some housing data (such as number of rooms, area, etc.). For the purpose of risk analysis, this data can be used, given information on building prices, occupancy of people, construction materials and so on. To learn more about this building data/information, check the building ' + '<a href="https://www.vd.ch/fileadmin/user_upload/organisation/dinf/sit/fichiers_pdf/Catalogue_caract%C3%A8res_RegBL_version_3.7.pdf" target="_blank">catalog</a> of the Canton of Vaud (pages:12-14) and the ' + '<a href="http://map.housing-stat.ch/" target="_blank">Buildings and Dwellings Portal</a> as an example.' + '<br><br>' + 'However, in this exercise, buildings data from ' + '<a href="https://www.openstreetmap.org" target="_blank">OpenStreetMap</a> are used for the simplicity of the exercise.',
							buttons: Ext.Msg.OK,															   
							animEl: 'elId',
							icon: Ext.MessageBox.INFO
						});		
					}
				},{
						id:'search',
						qtip: 'Play Video',
						handler: function(event, toolEl, panel){
							vid = new Ext.Window({
								title: 'Video Tutorial: visualization of hazard and object layers',
								resizable: false,
								html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xr8P1IA2j84?rel=0&amp;start=63" frameborder="0" allowfullscreen></iframe>'
							});
							
							vid.show();
					}
				}]
		});
		
		objInfoWin.show();
	}
});	

Ext.preg(riskgis.plugins.Objects.prototype.ptype, riskgis.plugins.Objects);	
	
	