/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */
 
/**
 * @require plugins/Tool.js 
 * @require GeoExt/widgets/Action.js
 * @require OpenLayers/Control/DrawFeature.js
 * @require OpenLayers/Handler/Polygon.js
 */
 
 /** api: (define)
 *  module = riskgis.plugins
 *  class = RiskCalculator
 */
 
 /** api: (extends)
 *  plugins/Tool.js
 */

Ext.ns("riskgis.plugins");

/** api: constructor
 *  .. class:: RiskCalculator(config)
 *
 *    Plugin for calculating risk based on vector-based spatial analysis.
 *    
 */

riskgis.plugins.RiskCalculator = Ext.extend(gxp.plugins.Tool, {

	/** api: ptype = riskgis_RiskCalculator */
	ptype: "riskgis_RiskCalculator",
	
	/** api: config[riskMenuText]
     *  ``String``
     *  Text for the menu item (i18n).
     */
    riskMenuText: "Calculate risk",

    /** api: config[riskActionTip]
     *  ``String``
     *  Text for the action tooltip (i18n).
     */
    riskActionTip: "Calculate risk",
	
	/** api: method[addActions]
     */
	addActions: function() {
		var actions = riskgis.plugins.RiskCalculator.superclass.addActions.apply(this, [{
            menuText: this.riskMenuText,
            iconCls: "riskgis-icon-riskcalculator",
			text: 'Risk Calculator',
            tooltip: this.riskActionTip,
            handler: function() {
			
				this.layer = new OpenLayers.Layer.Vector("zone_layer");
				
				// Some action defaults for a draw polygon button
				var actionDefaults = {
					map: app.mapPanel.map,
					enableToggle: true,
					toogleGroup: "controls",
					allowDepress: true
				};
				
				// Control of the polygon drawing
				var poly = new OpenLayers.Control.DrawFeature(
					this.layer, OpenLayers.Handler.Polygon,{
						eventListeners: {
							featureadded: function (event) {
								Ext.getCmp('areaPoly').setValue(event.feature.geometry); // set value of the polygon coordinates in 900913  
								poly.deactivate();
							}
						},
						callbacks: {
							create: function(data) { // remove previously drawn feature if existed
								if (this.layer.features.length > 0)
								{
									this.layer.removeAllFeatures();
								}
							}
						} 
				});	

				// Control of the polygon editing/dragging
				var modify = new OpenLayers.Control.ModifyFeature(this.layer, {
					mode: OpenLayers.Control.ModifyFeature.RESHAPE | OpenLayers.Control.ModifyFeature.DRAG, 
					standalone: false
				});		
				
				this.layer.events.on({
					afterfeaturemodified: function (event) {
						Ext.getCmp('areaPoly').setValue(event.feature.geometry); // set value of the polygon coordinates in 900913 
						modify.deactivate();
					}
				});
				
				this.riskCalForm = new Ext.form.FormPanel({
					labelWidth: 75, // label settings here cascade unless overridden
					url:'riskScenarioInfo.php',
					frame:true,	
					bodyStyle:'padding:5px 5px 0',
					width: 450,	
					autoHeight:true,		
					items: [{
						xtype:'fieldset',
						title: 'Risk Scenario Information',
						defaultType: 'textfield',
						defaults: {
							anchor: "95%",
							allowBlank: false,
							msgTarget: "side"
						},
						collapsible: true,
						autoHeight: true,
						items :[{
								fieldLabel: 'Name',
								name: 'scenario_name',
								emptyText: 'Name of the scenario ...'
							},{
								fieldLabel: 'Description',
								xtype: 'textarea',
								name: 'scenario_description',
								allowBlank: true,
								emptyText: 'Description of the scenario ...'
							},{
								fieldLabel: 'Alternative',
								name: 'alt_scenario_name',
								hiddenName: 'alt_scenario_id',
								editable: false,
								xtype: 'combo',					
								mode:'local',
								triggerAction: 'all',
								displayField: 'nom',
								valueField: 'id',
								store: new Ext.data.JsonStore({ 
									url: 'altInfo.php',										
									root: 'rows',
									fields : ['nom', 'id'],
									autoLoad: true,
									baseParams: {
										ws: 'riskgis_two',
										task: 'load',
										userID: userid,
										userRole: role
									}
								}),
								emptyText: 'Select the alternative scenario ...'
							},{
								fieldLabel: 'Option',
								name: 'option',
								hiddenName: 'option',
								xtype:'combo',
								emptyText: 'Select the extent of risk calculation ...',
								mode: "local",
								editable: false,
								store: new Ext.data.ArrayStore({
									id: 0,
									fields: [
										'myId',  // numeric value is the key
										'displayText'
									],
									data: [['L', 'Layer-based (as a whole)'], ['Z', 'Zone-based (as a defined polygon area)']]  // data is local
								}),
								valueField: 'myId',
								displayField: 'displayText',
								triggerAction: 'all',
								listeners: {
									select: function(combo, record, index) {
										if (combo.getValue() == 'Z') {
											app.mapPanel.map.addLayer(this.layer);
											this.riskCalForm.items.items[0].items.items[4].setVisible(true);
											Ext.getCmp('areaPoly').enable();
										}
										else {
											app.mapPanel.map.removeLayer(this.layer);
											this.riskCalForm.items.items[0].items.items[4].setVisible(false);
											Ext.getCmp('areaPoly').disable();
										}
									},
									scope: this
								}	
							},{
								fieldLabel: 'Area',
								xtype : 'compositefield',	
								hidden: true,
								items: [{
									xtype: 'textfield',
									id: 'areaPoly',
									readOnly: true,
									disabled: true,
									name: 'poly',
									flex: 1.8,
									emptyText: 'Define the zone using DRAW tool ...'
								}, 	new Ext.Button(
										new GeoExt.Action(Ext.apply({
										text: 'Draw',
										control: poly
									}, actionDefaults))),
									new Ext.Button(
										new GeoExt.Action(Ext.apply({
										text: 'Modify',
										control: modify
									}, actionDefaults)))
								]
							}]
						},{
							xtype:'tabpanel',
							plain:true,			
							activeTab: 0,
							height: 350,
							deferredRender: false,
							defaults:{bodyStyle:'padding:10px'},
							items:[{
								title: 'Hazard',
								defaultType: 'textfield',
								layout:'form',
								defaults: {
									anchor: "95%",
									allowBlank: false,
									msgTarget: "side"
								},
								items :[{
									fieldLabel: 'Type',
									name: 'haz_type',
									hiddenName: 'haz_type',
									itemId: 'haz_type',
									xtype:'combo',
									mode: "local",
									editable: false,
									emptyText: 'Select the hazard type ...',
									store: new Ext.data.ArrayStore({
											id: 0,
											fields: [
												'myId',  // numeric value is the key
												'displayText'
											],
											data: [['LTO', 'Laves torrentielles']/* ,['INOD','Inondation dynamique'],['INOS','Inondation statique'] */]  // data is local
									}),
									valueField: 'myId',
									displayField: 'displayText',
									triggerAction: 'all',
									listeners: {
										select: function(combo, record, index) {										
											this.riskCalForm.items.items[1].items.items[0].getComponent('hazName').clearValue();
											this.riskCalForm.items.items[1].items.items[0].getComponent('hazName').getStore().reload({ params: 
												{ 	ws: 'riskgis_two',													
													task: 'loadSelected',
													type: combo.getValue(),
													userID: userid,
													userRole: role
												} // reload the store to retrieve uploaded maps for the selected hazard type 
											});
										},
										scope: this
									}
								},{
									fieldLabel: 'Layer',
									name: 'haz_name',
									itemId: 'hazName',
									hiddenName: 'haz_name',
									xtype: 'combo',			
									emptyText: 'Select the hazard layer ...',
									triggerAction: 'all',
									editable: false,
									mode:'local',
									triggerAction: 'all',
									displayField: 'nom',
									valueField: 'indice',
									store: new Ext.data.JsonStore({ //list all the available layers
										url: 'hazInfo.php',										
										root: 'rows',
										fields : ['nom', 'indice']
									})									
								}]
							},{
								title: 'Object',
								defaultType: 'textfield',
								layout: 'form',
								defaults: {
									anchor: "95%",
									allowBlank: false,
									msgTarget: "side"
								},
								items :[{
									fieldLabel: 'Type',
									name: 'object_type',
									itemId: 'object_type',
									xtype:'combo',
									mode: "local",
									editable: false,
									emptyText: 'Select the object type ...',
									store: new Ext.data.ArrayStore({
											id: 0,
											fields: [
												'myId',  // numeric value is the key
												'displayText'
											],
											data: [['Buildings', 'Buildings']]  // data is local
									}),
									valueField: 'myId',
									displayField: 'displayText',
									triggerAction: 'all',
									listeners: {
										select: function(combo, record, index) {
											this.riskCalForm.items.items[1].items.items[1].getComponent('objName').clearValue();
											this.riskCalForm.items.items[1].items.items[1].getComponent('objName').getStore().reload({ params: 
												{ 	ws: 'riskgis_two',													
													task: 'loadSelected',
													type: combo.getValue(),
													userID: userid,
													userRole: role
												} // reload the store to retrieve uploaded maps for the selected object type
											});
										},
										scope: this
									}
								},{
									fieldLabel: 'Layer',
									name: 'object_name',
									itemId: 'objName',
									hiddenName: 'object_name',
									xtype: 'combo',			
									emptyText: 'Select the object layer ...',
									triggerAction: 'all',
									editable: false,
									mode:'local',
									triggerAction: 'all',
									displayField: 'nom',
									valueField: 'indice',
									store: new Ext.data.JsonStore({ //list all the available layers
										url: 'objInfo.php',										
										root: 'rows',
										fields : ['nom', 'indice']
									})									
								},{
									title: 'Parameters (value/people) of the object',
									name: 'value_object',
									xtype: 'editorgrid',
									itemId: 'objPara',
									height: 180,						
								//	disabled: true,
									store: new Ext.data.ArrayStore({ //list the default values
										id: 0,
										fields: [
											{name:'para'},
											{name:'value', allowBlank: false}
										],
										data: [
											['Price (per m2 in CHF)', 6500],
											['No. of people', 2.24],
											['Exposition (hours)', 16],
											['Value. of people (CHF)', 5000000]
										]											
									}),
									colModel: new Ext.grid.ColumnModel({
										defaults: {
											width: 70,
											sortable: false,
											menuDisabled: true
										},
										columns: [ 
											{header: "Parameters", dataIndex: 'para'},
											{header: "Values", dataIndex: 'value', editor: new Ext.form.NumberField()}											
										]
									}),
									viewConfig: {
										forceFit: true
									},
									frame: true
								}]
							},{
								title: 'Vulnerability',
								defaultType: 'textfield',
								layout: 'form',
								defaults: {
									anchor: "95%",
									allowBlank: false,
									msgTarget: "side"
								},
								items :[{
									title: 'Vulnerability-Intensity parameters (object/people)',
									name: 'value_vul',
									xtype: 'editorgrid',
									itemId: 'vulPara',
									height: 180,						
								//	disabled: true,
									store: new Ext.data.ArrayStore({ //list the default values
										id: 0,
										fields: [
											{name:'id'},											
											{name:'intensity_level'},
											{name:'damage_object', allowBlank: false},
											{name:'damage_people', allowBlank: false}											
										],
										data: [
											[1,'1: Low',0,0],
											[2,'2: Medium',55,0.3],
											[3,'3: High',75,4]
										]												
									}),
									colModel: new Ext.grid.ColumnModel({
										defaults: {
											width: 70,
											sortable: false,
											menuDisabled: true
										},
										columns: [
											{header: "ID", dataIndex: 'id', hidden: true},
											{header: "Intensity Levels", dataIndex: 'intensity_level'},
											{header: "Damage Total (%)", dataIndex: 'damage_object', editor: new Ext.form.NumberField({minValue:0, maxValue:100})},
											{header: "Dead (%)", dataIndex: 'damage_people', editor: new Ext.form.NumberField({minValue:0, maxValue:100})}
										]	
									}),
									viewConfig: {
										forceFit: true
									},
									frame: true
								} 
								]
							}]	
						}],
						buttons: [{
							text: 'Run',
							icon:  'src/gxp/theme/img/silk/control_play_blue.png', 
							handler: function(){							
								
								var form = this.riskCalForm.getForm(); // get the form data		
								var item = this.riskCalForm.items.items[0].items.items[4]; // get item to hide 
								
								var objRecords = []; // get the object parameters data
								this.riskCalForm.items.items[1].items.items[1].getComponent('objPara').getStore().each(function(record, index) {
									objRecords.push(record.data);
								});
								
								var vulRecords = []; // get the vulnerability parameters data
								this.riskCalForm.items.items[1].items.items[2].getComponent('vulPara').getStore().each(function(record, index) {
									vulRecords.push(record.data);
								});
								
								if (form.isValid()) { // submit the form if valid		
									form.submit({
										url: 'riskScenarioInfo.php',
										params: {
											task: 'add',
											ws: 'riskgis_two',
											objRecords: Ext.encode(objRecords),
											vulRecords: Ext.encode(vulRecords),
											userID: userid
										},
										timeout: 300000, // <<-- Timeout value specified here in milliseconds
										waitMsg : 'Please wait...',
										success: function(form, action) {
											Ext.Msg.alert('Success', action.result.message);	
											form.reset();													
											item.setVisible(false); // make invisible of the zone text box and buttons
											Ext.getCmp('areaPoly').disable(); // make disabled of the zone text box
								
											// add the calculated layer to the map	
											var lyr_name = action.result.mpIndex;
											var names = {};
											names[lyr_name] = true; 
											
											var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
											app.tools.addlyrs.setSelectedSource(source);																		
											app.tools.addlyrs.selectedSource.store.load({
												callback: function(records, options, success) {
													var gridPanel, sel;
														if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
															gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
															sel = gridPanel.getSelectionModel();
															sel.clearSelections();
														}
														// select newly added layers
														var newRecords = [];
														var last = 0;
														app.tools.addlyrs.selectedSource.store.each(function(record, index) {
															if (record.get("name") in names) {
																last = index;
																newRecords.push(record);
															}
														});
														if (gridPanel) {
															// this needs to be deferred because the 
															// grid view has not refreshed yet
															window.setTimeout(function() {
																sel.selectRecords(newRecords);
																gridPanel.getView().focusRow(last);
															}, 100);
														} else {
															app.tools.addlyrs.addLayers(newRecords, true);
														}                                                                                    
												},
												scope: this                                            
											});
										},
										// If you don't pass success:true, it will always go here
										failure: function(form, action) {
											Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
										}
									});
								}	
							},
							scope: this
						},{
							text: 'Reset',
							icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
							handler: function(){
								this.riskCalForm.getForm().reset(); // reset the form data 		
								app.mapPanel.map.removeLayer(this.layer); // remove the layer (if needed)
								this.riskCalForm.items.items[0].items.items[4].setVisible(false); // make invisible of the zone text box and buttons
								Ext.getCmp('areaPoly').disable(); // make disabled of the zone text box
							},
							scope: this
						}]	
					});				
						
				this.riskCalWin = new Ext.Window({
					title: 'Calculate a new risk scenario..',
					layout: 'fit',	
					autoScroll: true,
					plain:false,
					modal: false,
					maximizable: false,
					collapsible: true,
					closeable: true,						
					buttonAlign:'right',
					items: [this.riskCalForm],					
					tools:[
						{
							id:'help',
							qtip: 'Get Help',
							handler: function(event, toolEl, panel){
								Ext.Msg.show({
									title:'Help Information',
									msg: '<b>Risk</b> is defined by <a href="./Einstein_1988.pdf" target="_blank">Einstein</a> (1988, p. 1076) as  “the multiplication of hazard and potential worth of loss since the same hazard can lead to entirely different consequences depending on the use of the affected terrain risk". For management purposes, a comprehensive and accurate risk assessment is necessary considering its essential role in risk management. Risk assessment should be able to support the decision-making and risk treatment process for the effectiveness of risk management. There are several assessment methods (qualitative, quantitative and semiquantitative) depending on the study scale, data availability and aims of the analysis.' + '<br><br>' + 'To learn more about the risk concepts and approaches applied in Switzerland, read the documents <a href="./Brundl_2013.pdf" target="_blank">here</a> and <a href="./Brundl_2009.pdf" target="_blank">here</a>.' + '<br><br>' + 'In this exercise, for the risk calculation, default object parameter and vulnerability values are taken from a tool called <a href="https://econome.ch/eco_work/index.php?PHPSESSID=94jep0nm74akn4qru80inier17" target="_blank">EconoMe</a>, assuming that buildings fall into the category of individual houses (exclusively for the residential use, such as villas and chalets).' + '<br><br>' + 'In this platform, different types of risk can be calculated (risk for objects, risk for persons, individual risk and risk total) based on the same principles establised in <a href="https://econome.ch/eco_work/index.php?PHPSESSID=94jep0nm74akn4qru80inier17&linkid=1" target="_blank">EconoMe</a> and <a href="https://www.vd.ch/index.php?id=61124" target="_blank">Valdorisk</a>.',
									buttons: Ext.Msg.OK,															   
									animEl: 'elId',
									icon: Ext.MessageBox.INFO
								});	
							}
						},{
							id:'search',
							qtip: 'Play Video',
							handler: function(event, toolEl, panel){
								vid = new Ext.Window({
									title: 'Video Tutorial: risk estimation',
									resizable: false,
									html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xr8P1IA2j84?rel=0&amp;start=78" frameborder="0" allowfullscreen></iframe>'
								});
								
								vid.show();
							}
						}]
					});
					
				var win = Ext.WindowMgr.getActive();
				if (win) {
					win.close();					
				}
				
				this.riskCalWin.show();
			},
            scope: this
        }]);
		
		return actions;
    
	}
});

Ext.preg(riskgis.plugins.RiskCalculator.prototype.ptype, riskgis.plugins.RiskCalculator);
