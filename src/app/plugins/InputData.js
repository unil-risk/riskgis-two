/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */

/**
 * @requires plugins/Tool.js
 * @requires plugins/Hazards.js
 * @requires plugins/Objects.js
 */
 
 /** api: (define)
 *  module = riskgis.plugins
 *  class = InputData
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("riskgis.plugins");

/** api: constructor
 *  .. class:: InputData(config)
 *
 *    Provides actions for creating hazard and building layers as well as to view the list of available layers.
 */
riskgis.plugins.InputData = Ext.extend(gxp.plugins.Tool, {
	/** api: ptype = riskgis_InputData */
    ptype: "riskgis_InputData", 
	
	/** api: config[buttonText]
     *  ``String``
     *  Text for the Menu button (i18n).
     */
    buttonText: "Input Data",
	
	/** api: config[InputDataTooltip]
     *  ``String``
     *  Text for InputData action tooltip (i18n).
     */
    InputDataTooltip: "Tools for the data input",

    /** private: method[constructor]
     */
    constructor: function(config) {
        riskgis.plugins.InputData.superclass.constructor.apply(this, arguments);
    },

    /** private: method[destroy]
     */
    destroy: function() {
        this.button = null;
        riskgis.plugins.InputData.superclass.destroy.apply(this, arguments);
    },
	
	/** api: method[addActions]
     */
    addActions: function() {
        this.button = new Ext.SplitButton({
            iconCls: "riskgis-icon-inputdata",
            tooltip: this.InputDataTooltip,
            text: this.buttonText,           
            allowDepress: true,           
            scope: this,
            menu: new Ext.menu.Menu({
                items: [
					{
						text: 'Hazard',
						iconCls: "riskgis-icon-hazard",
						menu: {
							xtype: 'menu',
							items:[
								new Ext.menu.CheckItem(
									new Ext.Action({
										text: 'Create Layer',
										iconCls: "riskgis-icon-add-layer",									
										tooltip: 'Tool to create a new hazard layer',
										listeners: {
											checkchange: function(item, checked) {
												if (checked) {
													this.button.setIconClass(item.iconCls);
												}
											},
											scope: this
										},
										handler: function(){											
											app.tools.hazards.actions[0].execute('create');								
										}
									})   
								),
								new Ext.menu.CheckItem(
									new Ext.Action({
										text: 'View Layers',
										iconCls: "riskgis-icon-sceList",										
										tooltip: 'Tool to view the list of hazard layers',
										listeners: {
											checkchange: function(item, checked) {
												if (checked) {
													this.button.setIconClass(item.iconCls);
												}
											},
											scope: this
										},
										handler: function(){											
											app.tools.hazards.actions[0].execute('view');								
										}
									})   
								)
							]
						}						
					},{
						text: 'Object',
						iconCls: "riskgis-icon-object",
						menu: {
							xtype: 'menu',
							items:[
								new Ext.menu.CheckItem(
									new Ext.Action({
										text: 'Create Layer',
										iconCls: "riskgis-icon-add-layer",										
										tooltip: 'Tool to create a new object layer',
										listeners: {
											checkchange: function(item, checked) {
												if (checked) {
													this.button.setIconClass(item.iconCls);
												}
											},
											scope: this
										},
										handler: function(){
											app.tools.objects.actions[0].execute('create');									
										}
									})   
								),
								new Ext.menu.CheckItem(
									new Ext.Action({
										text: 'View Layers',
										iconCls: "riskgis-icon-sceList",										
										tooltip: 'Tool to view the list of object layers',
										listeners: {
											checkchange: function(item, checked) {
												if (checked) {
													this.button.setIconClass(item.iconCls);
												}
											},
											scope: this
										},
										handler: function(){
											app.tools.objects.actions[0].execute('view');									
										}
									})   
								)
							]
						}	
					}	
                ]
            })
        });
		
		return riskgis.plugins.InputData.superclass.addActions.apply(this, [this.button]);
    }
	
	});

Ext.preg(riskgis.plugins.InputData.prototype.ptype, riskgis.plugins.InputData);
