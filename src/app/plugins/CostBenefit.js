/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */
 
/**
 * @require plugins/Tool.js
 */
 
 /** api: (define)
 *  module = riskgis.plugins
 *  class = CostBenefit
 */
 
 /** api: (extends)
 *  plugins/Tool.js
 */

Ext.ns("riskgis.plugins");

/** api: constructor
 *  .. class:: CostBenefit(config)
 *
 *    Plugin for creating and viewing of cost-benefit reports.
 *    
 */
 
 riskgis.plugins.CostBenefit = Ext.extend(gxp.plugins.Tool, {

	/** api: ptype = riskgis_CostBenefit */
	ptype: "riskgis_CostBenefit",
	
	/** api: config[costBenMenuText]
     *  ``String``
     *  Text for the menu item (i18n).
     */
    costBenMenuText: "Cost-Benefit Analysis",

    /** api: config[costBenActionTip]
     *  ``String``
     *  Text for the tooltip (i18n).
     */
    costBenActionTip: "Tool to create new cost-benefit reports and view the list of available reports",
	
	addActions: function() {
		var actions = riskgis.plugins.CostBenefit.superclass.addActions.apply(this, [			 						
		{
			menuText: this.costBenMenuText,
            iconCls: "riskgis-icon-cba",
            tooltip: this.costBenActionTip,
			scope: this,
            handler: function(args) {
				if (args == 'create') this.createReport();
				else this.listReports();
			}			
		}
		]);
	},
	
	/** api: method[createReport]
     */
	createReport: function() {
		var xg = Ext.grid;		
		var costBenAddStore = new Ext.data.GroupingStore({
				url: 'costBenInfo.php',
				baseParams: {
					task: 'loadGrid',
					ws: 'riskgis_two',
					userID: userid,
					userRole: role
				},
				sortInfo:{field: 'id', direction: "ASC"},
				groupField:'alt_nom',
				autoLoad: true,
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [					
						{name : 'id', type : 'int'},
						{name : 'alt_id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'alt_nom', type : 'String'},
						{name : 'description', type : 'String'}, 
						{name : 'processus_type', type : 'String'},
						{name : 'objet_type', type : 'String'},						
						{name : 'indice', type : 'String'},
						{name : 'risque_total', type : 'float'},
						{name : 'cout_total', type : 'float'}
						/* {name : 'groupe', convert : function (v, rec){
							return rec.processus_type + ', ' + rec.alt_nom;
						} }*/
					] 
				})
		});	
		
		var expander = new Ext.grid.RowExpander({
			tpl : new Ext.Template(
				'<p><b>Description:</b> {description}</p>'
			)
		});
		
		var customRender = Ext.util.Format.frMoney = function(v) 	// plug Swiss Franc currency renderer into formatter
			{
				v = (Math.round((v-0)*100))/100;
				v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
				return (v + ' CHF');
			};
			
		var costBenAddGrid = new Ext.grid.GridPanel({
			store: costBenAddStore,
			colModel: new Ext.grid.ColumnModel({
				defaults: {	
					sortable: true,
					menuDisabled: true
				},
				columns: [
					//	new Ext.grid.RowNumberer(),
					expander,
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Alt_ID", dataIndex: 'alt_id', hidden: true},
					{header: "Risk Name", dataIndex: 'nom'},
					{header: "Alternative Name", dataIndex: 'alt_nom'},
					{header: "Description", dataIndex: 'description', hidden: true},					
					{header: "Hazard Type", dataIndex: 'processus_type'},	
					{header: "Object Type", dataIndex: 'objet_type'},
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Risk Total (per year)", dataIndex: 'risque_total', align: 'right', renderer: customRender},
					{header: "Cost Total (per year)", dataIndex: 'cout_total', align: 'right', renderer: customRender}
				]
			}),	
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			stripeRows: true,
			loadMask: true,
			width: 700,
			height: 450,						
			plugins:[expander,
				new Ext.ux.grid.Search({
					iconCls:'gxp-icon-zoom-to',
					disableIndexes:['id','alt_id','indice'],
					minChars:2,
					autoFocus:true,
					width: 150,
					mode: 'local'
			})],
			bbar: [{
				xtype: 'tbtext',
				id: 'listRiskSceTotalRec',
				text: 'Loading ...'
			},'->',{
				text: 'Generate New',
				iconCls: 'riskgis-icon-add-report',
				handler: function(){
					// check whether the risk scnearios are selected
					var records = costBenAddGrid.getSelectionModel().getSelections();
					if ( records.length < 2) {
						Ext.MessageBox.show({
							title: 'Help Support!',
							msg: 'Please select at least two risk scenarios for comparison of risk (before/after) to generate a new cost-benefit report!',
							buttons: Ext.MessageBox.OK,
							animEl: 'mb9',
							icon: Ext.MessageBox.INFO
						});
					}
					else {
						// show the costBenAddForm to enter the data
						var costBenAddForm = new Ext.form.FormPanel({
							labelWidth: 75, // label settings here cascade unless overridden
							url:'costBenInfo.php',
							frame:true,			
							bodyStyle:'padding:5px 5px 0',
							width: 450,	
							autoHeight:true,		
							items: [{
								xtype:'fieldset',
								title: 'Report Information',
								collapsible: true,
								defaultType: 'textfield',
								defaults: {
									anchor: "90%",					
									msgTarget: "side"
								},
								collapsed: false,
								items :[{
									fieldLabel: 'Name',
									name: 'name_report',
									emptyText: 'Name of the report..',
									allowBlank: false
								},{
									fieldLabel: 'Description',
									xtype: 'textarea',
									name: 'description_report',
									emptyText: 'Description of the report..',
									allowBlank: true
								},{
									fieldLabel: 'Remarks',
									xtype: 'textarea',
									name: 'remarks_report',
									emptyText: 'If any other remarks (optional)..',
									allowBlank: true
								}]	
							}],
							buttons: [{
								text: 'Add',
								icon:  'theme/app/img/report_add.png',
								handler: function(){
									var form = costBenAddForm.getForm();
									var recs = [];
									for (var i=0;i<records.length;i++) {
										recs.push(records[i].data);								
									}
									
									if (form.isValid()) {						
										form.submit({
											url: 'costBenInfo.php',
											params: {
												task: 'add',
												ws: 'riskgis_two',
												records: Ext.encode(recs),
												userID: userid
											},
											waitMsg : 'Please wait...',
											success: function(form, action) {
												if (action.result.success) {
													Ext.Msg.alert('Success', action.result.message); // show the success message	
													form.reset(); 
												}
												else Ext.Msg.alert('Failed', action.result.message);
											},
											// If you don't pass success:true, it will always go here
											failure: function(form, action) {
												Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
											}
										});	
									}
									else {
										Ext.Msg.alert('Validation Error', 'The form is not valid!' );
									}
								}
							},{
								text: 'Reset',
								icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
								handler: function(){
									// reset the form 
									costBenAddForm.getForm().reset(); 
								}
							}]		
						});

						new Ext.Window({
							title: 'Generate a new cost-benefit report ...',
							layout: 'form',
							autoHeight: true,
							plain:false,
							modal: true,
							resizable: true,
							closeable: true,
							closeAction:'hide',
							buttonAlign:'right',
							items: [costBenAddForm],
							tools:[
							{
								id:'help',
								qtip: 'Get Help',
								handler: function(event, toolEl, panel){
									Ext.Msg.show({
										title:'Help Information',
										msg: '<b>Cost-benefit analysis</b> is one of the methods used to decide if protective measures should be implemented or not. It compares the cost of a protective measure to the risk reduction that this measure offers. Therefore, it is an appropriate tool for governmental institutions that need an objective method to allocate their resources.' + '<br><br>' + 'In Switzerland, such an analysis is required for the funding of protective measures by the confederation, and <a href="https://econome.ch/eco_work/index.php?PHPSESSID=94jep0nm74akn4qru80inier17&linkid=1" target="_blank">EconoMe</a> is used for this purpose.'+ '<br><br>' + 'In this platform, based on the same principles of EconoMe, the cost-benefit ratio is calculated using the <a href="./Brundl_2011.pdf" target="_blank">formula (Eq. 50)</a>, as mentioned in Brundl et al. (2011, p. 14), and it is profitable if this ratio is >= 1.',
										buttons: Ext.Msg.OK,															   
										animEl: 'elId',
										icon: Ext.MessageBox.INFO
									});	
								}
							},{
								id:'search',
								qtip: 'Play Video',
								handler: function(event, toolEl, panel){
									vid = new Ext.Window({
										title: 'Video Tutorial: cost estimation',
										resizable: false,
										html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xr8P1IA2j84?rel=0&amp;start=790" frameborder="0" allowfullscreen></iframe>'
									});
											
									vid.show();
								}
							}]
						}).show();
					}	
				}
			}]						 
		});
		
		costBenAddGrid.getStore().on('load', 					
			function (store, records, options) {
				Ext.getCmp('listRiskSceTotalRec').setText('Total no. of risk scenarios: ' + records.length.toString());
				//	this.getBottomToolbar().get(0).setText('Total count: ' + records.length.toString());
			}
		);
		
		var costBenAddWin = new Ext.Window({
			title: 'Select to compare risk and generate a new cost-benefit report ...',
			layout: 'fit',
			plain:false,
			closeable: true,
			maximizable: true,
			collapsible: true,
			buttonAlign:'right',
			items: [costBenAddGrid],
			tools:[
					{
						id:'help',
						qtip: 'Get Help',
						handler: function(event, toolEl, panel){
							Ext.Msg.show({
								title:'Help Information',
								msg: '<b>Cost-benefit analysis</b> is one of the methods used to decide if protective measures should be implemented or not. It compares the cost of a protective measure to the risk reduction that this measure offers. Therefore, it is an appropriate tool for governmental institutions that need an objective method to allocate their resources.' + '<br><br>' + 'In Switzerland, such an analysis is required for the funding of protective measures by the confederation, and <a href="https://econome.ch/eco_work/index.php?PHPSESSID=94jep0nm74akn4qru80inier17&linkid=1" target="_blank">EconoMe</a> is used for this purpose.'+ '<br><br>' + 'In this platform, based on the same principles of EconoMe, the cost-benefit ratio is calculated using the <a href="./Brundl_2011.pdf" target="_blank">formula (Eq. 50)</a>, as mentioned in Brundl et al. (2011, p. 14), and it is profitable if this ratio is >= 1.',
								buttons: Ext.Msg.OK,															   
								animEl: 'elId',
								icon: Ext.MessageBox.INFO
							});	
					}
				},{
					id:'search',
					qtip: 'Play Video',
					handler: function(event, toolEl, panel){
						vid = new Ext.Window({
							title: 'Video Tutorial: cost estimation',
							resizable: false,
							html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xr8P1IA2j84?rel=0&amp;start=790" frameborder="0" allowfullscreen></iframe>'
						});
											
						vid.show();
					}
				}]
		});
		
		costBenAddWin.show();
		
	},
	/** api: method[listReports]
     */
	listReports: function() {
		var xg = Ext.grid;		
		var costBenInfoStore = new Ext.data.GroupingStore({
			url: 'costBenInfo.php',
			baseParams: {
				task: 'load',
				ws: 'riskgis_two',
				userID: userid,
				userRole: role
			},
			autoLoad: true,
			sortInfo:{field: 'id', direction: "ASC"},
			groupField:(role == 'admin')? 'nom_utilisateur':'',
			reader: new Ext.data.JsonReader({
				totalProperty : 'totalCount',
				root          : 'rows',
				successProperty: 'success',
				idProperty    : 'id',
				fields: [					
					{name : 'id', type : 'int'},
					{name : 'nom', type : 'String'},
					{name : 'description', type : 'String'}, 
					{name : 'remarques', type : 'String'},
					{name : 'id_utilisateur', type : 'int'},
					{name : 'nom_utilisateur', type : 'String'}
				] 
			})
		});
		
		var expander = new Ext.grid.RowExpander({
			tpl : new Ext.Template(
				'<p><b>Remarks:</b> {remarques}</p>'
			)
		});
		
		var costBenInfoGrid = new xg.GridPanel({
			store: costBenInfoStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					expander,
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description'},
					{header: "Remarks", dataIndex: 'remarques', hidden: true},
					{header: "User ID", dataIndex: 'id_utilisateur', hidden: true},
					{header: "User Name", dataIndex: 'nom_utilisateur', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
							{
								icon: 'theme/app/img/table_edit.png',  
								tooltip: 'Edit Information',	
								getClass: function(v, meta, rec) {          
									if ( role != 'admin') {
										if (userid != rec.get('id_utilisateur')) {
											return 'x-hide-display';
										}
									}
								},
								handler: function(grid, rowIndex, colIndex) {									
									var rec = grid.getStore().getAt(rowIndex);
									var costBenEditWin = new Ext.Window({
										title: 'Edit the cost-benefit report information..',
										layout: 'form',
										autoHeight: true,
										plain:false,
										modal: true,
										resizable: false,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: new Ext.form.FormPanel({
											labelWidth: 100, // label settings here cascade unless overridden
											itemId: 'costBenEditForm',
											frame:true,			
											bodyStyle:'padding:5px 5px 0',
											width: 350,	
											autoHeight:true,		
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name/Title',
												xtype: 'textfield',
												name: 'name',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description',
												value: rec.get('description')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks',
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												icon:  'theme/app/img/table_save.png',
												handler: function(){
													var form = costBenEditWin.getComponent('costBenEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'costBenInfo.php',
															params: {
																task: 'edit',
																ws: 'riskgis_two',															
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																costBenEditWin.close();
																grid.getStore().reload({ // refresh the grid view
																	callback: function(){
																		grid.getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											},{
												text: 'Cancel',
												icon:  'src/gxp/theme/img/decline.png',
												handler: function(){
													costBenEditWin.close();
												}
											}]
										})											
									});
												
									costBenEditWin.show();
								}
							},{
								icon: 'theme/app/img/table.png',  
								tooltip: 'View the included risk scenarios in the report',
								handler: function(grid, rowIndex, colIndex) {
									var rec = grid.getStore().getAt(rowIndex);
									var customRender = Ext.util.Format.frMoney = function(v) 	// plug Swiss Franc currency renderer into formatter
									{
										v = (Math.round((v-0)*100))/100;
										v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
										return (v + ' CHF');
									};
									var customExpRender = function(v) 	// plug exponential renderer into formatter
									{									
										return v.toExponential(2);
									};
									
									var costBenComDataGrid = new Ext.grid.GridPanel({
										store: new Ext.data.GroupingStore({
											url: 'costBenInfo.php',
											baseParams: {
												task: 'loadCostBenComValues',
												ws: 'riskgis_two',
												cbID: rec.get('id')												
											},
											sortInfo:{field: 'alt_id', direction: "ASC"},
											groupField:'alt_nom',
											remoteSort:true,
											reader: new Ext.data.JsonReader({
												totalProperty : 'totalCount',
												root          : 'rows',
												successProperty: 'success',
												idProperty    : 'id',
												fields: [ 				
													{name : 'alt_id', type : 'int'},
													{name : 'alt_nom', type : 'String'},
													{name : 'risque_sce_id', type : 'int'},
													{name : 'risque_sce_nom', type : 'String'},
													{name : 'processus_type', type : 'String'},
													{name : 'objet_type', type : 'String'},
													{name : 'objet_total', type : 'float'},
													{name : 'humain_total', type : 'float'},
													{name : 'risque_total', type : 'float'},
													{name : 'risque_individuel', type : 'float'},
													{name : 'indice', type : 'String'}
												]
											}),
											autoLoad: true
										}),								
										colModel: new Ext.grid.ColumnModel({
											defaults: {
												width: 120,
												sortable: false,
												menuDisabled: true
										},
										columns: [
											{header: "Alternative ID", dataIndex: 'alt_id', hidden: true},
											{header: "Risk Scenario ID", dataIndex: 'risque_sce_id', hidden: true},
											{header: "Alternative", dataIndex: 'alt_nom'},
											{header: "Risk Scenario", dataIndex: 'risque_sce_nom'},
											{header: "Hazard Type", dataIndex: 'processus_type'},
											{header: "Object Type", dataIndex: 'objet_type'},
											{header: "Object (CHF/year)", dataIndex: 'objet_total', renderer: customRender, align: 'right', summaryType: 'sum', summaryRenderer: customRender},
											{header: "Human (persons/year)", dataIndex: 'humain_total', renderer: customExpRender, align: 'right', summaryType: 'sum', summaryRenderer: customExpRender},
											{header: "Risk Total (CHF/year)", dataIndex: 'risque_total', renderer: customRender, align: 'right', summaryType: 'sum', summaryRenderer: customRender},
											{header: "Individual (person/year)", dataIndex: 'risque_individuel', renderer: customExpRender, align: 'right', summaryType: 'sum', summaryRenderer: customExpRender},
											{header: "Mapping Index", dataIndex: 'indice', hidden: true}
										]
										}),
										view: new Ext.grid.GroupingView({				
											forceFit: true,
											groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
										}),	
										plugins: new Ext.ux.grid.GroupSummary(),
										sm: new Ext.grid.RowSelectionModel({singleSelect:true}),	
										frame: true
									});
									
									// a window to show the panel 
									var costBenComDataWin = new Ext.Window({
										title: 'Included risk scenarios in this report: ' + rec.get('nom'),
										width: 500,
										height: 400,
										layout: 'fit',
										plain: true,
										maximizable: true,
										collapsible: true,
										closeable: true,
										buttonAlign:'right',
										items: [costBenComDataGrid]
									});
									
									costBenComDataWin.show();
								}
							},{								
								icon: 'theme/app/img/chart_bar.png',  
								tooltip: 'Visualize the cost-benefit values of risk before and after (Table/Chart)',								
								handler: function(grid, rowIndex, colIndex) {
									var rec = grid.getStore().getAt(rowIndex);								
									var costBenChartStore = new Ext.data.JsonStore({
										url: 'costBenInfo.php',
										sortInfo:{field: 'alt_id', direction: "ASC"},
										root: 'rows',
										fields: ['alt_id','alt_nom','cout_annuel','risque_total','benefice','rapport'],												
										autoLoad: true,
										baseParams: {
											task: 'loadChartStoreGrid',
											ws: 'riskgis_two',
											id: rec.get('id')
										}
									});
									
									var customRender = Ext.util.Format.frMoney = function(v) 	// plug Swiss Franc currency renderer into formatter
									{
										v = (Math.round((v-0)*100))/100;
										v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
										return (v + ' CHF');
									};
									
									// grid to show
									var costBenDataGrid = new Ext.grid.GridPanel({
										store: costBenChartStore,								
										colModel: new Ext.grid.ColumnModel({
											defaults: {
											width: 120,
											sortable: false,
											menuDisabled: true
										},
										columns: [
											{header: "Alternative ID", dataIndex: 'alt_id', hidden: true},
											{header: "Alternative", dataIndex: 'alt_nom'},
											{header: "Annual Cost (CHF/year)", dataIndex: 'cout_annuel', align: 'right', renderer: customRender},
											{header: "Risk Total (CHF/year)", dataIndex: 'risque_total', align: 'right', renderer: customRender},
											{header: "Benefit (CHF/year)", dataIndex: 'benefice', align: 'right', renderer: customRender},
											{header: "Cost-Benefit Ratio", dataIndex: 'rapport', align: 'right', renderer: function(v, params, record){												
												return Ext.util.Format.number(record.get('benefice')/record.get('cout_annuel'), '0.00'); 
											}}
										]
										}),
										viewConfig: {
											forceFit: true
										},
										sm: new Ext.grid.RowSelectionModel({singleSelect:true}),	
										frame: true
									});
									
									var costBenChartPanel = new Ext.Panel({										
										frame:true,												
										layout: 'vbox',	
										layoutConfig: {
											align: 'stretch',
											pack: 'start'
										},
										items: [{
											flex: 1,
											layout: 'fit',
											border: true,
											frame: true,
											title: 'Stacked Column Chart: Comparison of annual risk and cost',
											items: [{
												xtype: 'stackedcolumnchart',
												store: costBenChartStore,
												url: 'src/ext/resources/charts.swf',
												xField: 'alt_nom',												
												yAxis: new Ext.chart.NumericAxis({
													stackingEnabled: true,
													labelRenderer : Ext.util.Format.numberRenderer('0,0')
												}),
												series: [{
													displayName: 'Risk Total (CHF/year)',
													yField: 'risque_total',
													style: {
														mode: 'stretch'
													}
												},{
													displayName: 'Annual Cost (CHF/year)',
													yField: 'cout_annuel',
													style: {
														mode: 'stretch',
														color: 0xA52A2A
													}
												}],
												extraStyle: {															
													legend: {																		
														display: 'bottom',
														padding: 5,
														font: {
															family: 'Tahoma',
															size: 10
														}
													}
												}
											}]			
										},{	
											flex: 1,
											layout: 'fit',
											border: true,
											items: [costBenDataGrid]
										}]
									});	
									
									// a window to show the panel 
									var costBenChartWin = new Ext.Window({
										title: 'Visualization of the report: ' + rec.get('nom'),
										width: 450,
										height: 500,
										layout: 'fit',
										plain: true,
										maximizable: true,
										collapsible: true,
										closeable: true,
										buttonAlign:'right',
										items: [costBenChartPanel]
									});
													
									costBenChartWin.show();
								}
							}]
					}	
				],
				defaults: {
					sortable: true,
					menuDisabled: (role == 'admin')? false:true,
					width: 5
				}
			}),
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),	
			frame:true,
			stripeRows: true,
			width: 700,
			height: 450,
			plugins:[expander,
				new Ext.ux.grid.Search({
					iconCls:'gxp-icon-zoom-to',
					disableIndexes:['id', 'id_utilisateur'],
					minChars:2,
					autoFocus:true,
					width: 150,
					mode: 'local'
				})
			],
			bbar: [{
				xtype: 'tbtext',
				id: 'listCBATotalRec',
				text: 'Loading ...'
			},'->',{
				text: 'Delete',
				iconCls: 'delete',
				handler: function(){
					// delete the record from store, db (if not admin, user can only delete the ones he/she created)
					Ext.Msg.show({
						title:'Delete the selected reports?',
						msg: 'Are you sure to delete the selected reports? This action is undoable!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = costBenInfoGrid.getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
									if (records[i].get('id_utilisateur') == userid || role == 'admin'){ // if not admin, user can only delete the ones he/she created
										ids.push(records[i].data);
									}
									else {										
										Ext.Msg.alert('Information', 'You can only delete the reports you created, please exclude others from the list of selected records and try again!');
										return;
									}
								}
								
								Ext.Ajax.request({   
									url: 'costBenInfo.php',
									params: {
										task: 'delete',
										ws: 'riskgis_two',
										IDs: Ext.encode(ids)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										costBenInfoGrid.getStore().reload({ 
												callback: function(){
													costBenInfoGrid.getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]						 
		});
				
		costBenInfoGrid.getStore().on('load', 					
			function (store, records, options) {
				Ext.getCmp('listCBATotalRec').setText('Total no. of reports: ' + records.length.toString());
			}
		);

		var costBenInfoWin = new Ext.Window({
			title: 'Cost benefit reports information',
			layout: 'fit',		
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: costBenInfoGrid,
			tools:[
			{
				id:'help',
				qtip: 'Get Help',
				handler: function(event, toolEl, panel){
					Ext.Msg.show({
						title:'Help Information',
						msg: '<b>Cost-benefit analysis</b> is one of the methods used to decide if protective measures should be implemented or not. It compares the cost of a protective measure to the risk reduction that this measure offers. Therefore, it is an appropriate tool for governmental institutions that need an objective method to allocate their resources.' + '<br><br>' + 'In Switzerland, such an analysis is required for the funding of protective measures by the confederation, and <a href="https://econome.ch/eco_work/index.php?PHPSESSID=94jep0nm74akn4qru80inier17&linkid=1" target="_blank">EconoMe</a> is used for this purpose.'+ '<br><br>' + 'In this platform, based on the same principles of EconoMe, the cost-benefit ratio is calculated using the <a href="./Brundl_2011.pdf" target="_blank">formula (Eq. 50)</a>, as mentioned in Brundl et al. (2011, p. 14), and it is profitable if this ratio is >= 1.',
						buttons: Ext.Msg.OK,															   
						animEl: 'elId',
						icon: Ext.MessageBox.INFO
					});							
				}
			},{
				id:'search',
				qtip: 'Play Video',
				handler: function(event, toolEl, panel){
					vid = new Ext.Window({
						title: 'Video Tutorial: cost estimation',
						resizable: false,
						html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xr8P1IA2j84?rel=0&amp;start=850" frameborder="0" allowfullscreen></iframe>'
					});
											
					vid.show();
				}
			}]
		});
		
		costBenInfoWin.show();		
	}
});	

Ext.preg(riskgis.plugins.CostBenefit.prototype.ptype, riskgis.plugins.CostBenefit);
	