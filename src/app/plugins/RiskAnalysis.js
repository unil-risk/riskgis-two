/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */

/**
 * @requires plugins/Tool.js
 * @requires plugins/RiskCalculator.js
 * @requires plugins/RiskScenarios.js
 */

/** api: (define)
 *  module = riskgis.plugins
 *  class = RiskAnalysis
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("riskgis.plugins");

/** api: constructor
 *  .. class:: RiskAnalysis(config)
 *
 *    Provides three actions for calculating risk, viewing the summary statistics and the list of calculated risk scenarios.
 */
riskgis.plugins.RiskAnalysis = Ext.extend(gxp.plugins.Tool, {
    
    /** api: ptype = riskgis_RiskAnalysis */
    ptype: "riskgis_RiskAnalysis", 

    /** api: config[buttonText]
     *  ``String``
     *  Text for the RiskAnalysis button (i18n).
     */
    buttonText: "Risk Analysis",

    /** api: config[calculatorMenuText]
     *  ``String``
     *  Text for RiskAnalysis's risk calculator menu item (i18n).
     */
    calculatorMenuText: "Risk Calculator",

    /** api: config[summaryMenuText]
     *  ``String``
     *  Text for RiskAnalysis's summary menu item (i18n).
     */
    summaryMenuText: "View Summary",
	
	/** api: config[sceListMenuText]
     *  ``String``
     *  Text for RiskAnalysis's list of calculated risk scenarios menu item (i18n).
     */
    sceListMenuText: "View Scenarios",

    /** api: config[calculatorTooltip]
     *  ``String``
     *  Text for RiskAnalysis's risk calculator action tooltip (i18n).
     */
    calculatorTooltip: "Calculate a new risk scenario",

    /** api: config[summaryTooltip]
     *  ``String``
     *  Text for RiskAnalysis's summary action tooltip (i18n).
     */
    summaryTooltip: "View the summary statistics of a calculated risk scenario",
	
	/** api: config[sceListTooltip]
     *  ``String``
     *  Text for RiskAnalysis's list of calculated risk scenarios action tooltip (i18n).
     */
    sceListTooltip: "View the list of calculated risk scenarios and their respective parameters used in the calculation",

    /** api: config[RiskAnalysisTooltip]
     *  ``String``
     *  Text for RiskAnalysis action tooltip (i18n).
     */
    RiskAnalysisTooltip: "Tools for risk analysis",

    /** private: method[constructor]
     */
    constructor: function(config) {
        riskgis.plugins.RiskAnalysis.superclass.constructor.apply(this, arguments);
    },

    /** private: method[destroy]
     */
    destroy: function() {
        this.button = null;
        riskgis.plugins.RiskAnalysis.superclass.destroy.apply(this, arguments);
    },
   
    /** api: method[addActions]
     */
    addActions: function() {
        this.activeIndex = 0;
        this.button = new Ext.SplitButton({
            iconCls: "riskgis-icon-riskcalculator",
            tooltip: this.RiskAnalysisTooltip,
            text: this.buttonText,        
            allowDepress: true,
            handler: function(button, event) {
                if(button.pressed) {
                    button.menu.items.itemAt(this.activeIndex).setChecked(true);
                }
            },
            scope: this,            
            menu: new Ext.menu.Menu({
                items: [
					new Ext.menu.CheckItem(
						new Ext.Action({
							text: this.calculatorMenuText,
							iconCls: "riskgis-icon-riskcalculator",							
							tooltip: this.calculatorTooltip,
							listeners: {
                                checkchange: function(item, checked) {
                                    this.activeIndex = 0;                                    
                                    if (checked) {
                                        this.button.setIconClass(item.iconCls);
                                    }
                                },
                                scope: this
                            },
							handler: function(){
								app.tools.calculator.actions[0].execute();								
							}
						})   
                    ),                    
                    new Ext.menu.CheckItem(
						new Ext.Action({
							text: this.sceListMenuText,
							iconCls: "riskgis-icon-sceList",							
							tooltip: this.sceListTooltip,
							listeners: {
                                checkchange: function(item, checked) {
                                    this.activeIndex = 1;                                   
                                    if (checked) {
                                        this.button.setIconClass(item.iconCls);
                                    }
                                },
                                scope: this
                            },
							handler: function(){
								app.tools.scenarios.actions[0].execute();	
							},
                            scope: this
						})                       
                    )
                ]
            })
        });

        return riskgis.plugins.RiskAnalysis.superclass.addActions.apply(this, [this.button]);
    },
	
	/** api: method[viewSummary]
     */
	viewSummary: function(config) {
        config = config || {};
		
		var chartStore = new Ext.data.GroupingStore({
			url: 'riskScenarioInfo.php',
			baseParams: {
				task: 'loadChartStoreGrid',
				ws: 'riskgis_two',
				mpIndex: config									
			},
			sortInfo:{field: 'return_period', direction: "ASC"},
			groupField:'return_period',
			reader: new Ext.data.JsonReader({
				totalProperty : 'totalCount',
				root          : 'rows',
				successProperty: 'success',
				idProperty    : 'id',
				fields: [
					{name : 'return_period', type : 'int'},
					{name : 'intensity_level', type : 'int'},
					{name : 'exposed_obj', type : 'float'},
					{name : 'risk_obj', type : 'float'},
					{name : 'exposed_people', type : 'float'},
					{name : 'risk_people_killed', type : 'float'},
					{name : 'risk_total', type : 'float'}
				]	
			}),
			autoLoad: true
			});
										
		/* var chartStore = new Ext.data.JsonStore({
			url: 'riskScenarioInfo.php',
			sortInfo:{field: 'return_period', direction: "ASC"},
			root: 'rows',
			fields: ['return_period', 'intensity_level', 'exposed_obj', 'risk_obj','exposed_people','risk_people_killed'],												
			autoLoad: true,
				baseParams: {
				task: 'loadChartStoreGrid',
				ws: 'riskgis',
				mpIndex: config
			}
		}); */
				
		var dataGrid = new Ext.grid.GridPanel({
			store: chartStore,								
			colModel: new Ext.grid.ColumnModel({
				defaults: {
					width: 120,
					sortable: false,
					menuDisabled: true
			},
			columns: [				
				{header: "Return Period", dataIndex: 'return_period', hidden: true},
				{header: "Intensity Level", dataIndex: 'intensity_level'},
				{header: "No. of exposed objects", dataIndex: 'exposed_obj', align: 'right', summaryType: 'sum', renderer: Ext.util.Format.numberRenderer('0,0')},
				{header: "Risk of objects (amount/year)", dataIndex: 'risk_obj', align: 'right', summaryType: 'sum', renderer: Ext.util.Format.numberRenderer('0,000.00')},
				{header: "No. of exposed people", dataIndex: 'exposed_people', align: 'right', summaryType: 'sum', renderer: Ext.util.Format.numberRenderer('0,0')},
				{header: "Risk of humans (persons/year)", dataIndex: 'risk_people_killed', align: 'right', summaryType: 'sum', renderer: Ext.util.Format.numberRenderer('0,0.000000')},
				{header: "Risk Total (amount/year)", dataIndex: 'risk_total', align: 'right', summaryType: 'sum', renderer: Ext.util.Format.numberRenderer('0,000.00')}
				]
			}),
			viewConfig: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),
			/* viewConfig: {
				forceFit: true
			}, */
			plugins: new Ext.ux.grid.GroupSummary(),
			stripeRows : true,
			sm: new Ext.grid.RowSelectionModel({singleSelect:true}),	
			frame: true,
			tbar : []																	
		});		
			
		var chartPanel = new Ext.Panel({										
			frame:true,												
			layout: 'vbox',	
			layoutConfig: {
				align: 'stretch',
				pack: 'start'
			},
			items: [{
				xtype: 'compositefield',
				itemId: 'xyAxis',
				combineErrors: false,
				items: [{
					xtype: 'button',
					icon: 'src/gxp/theme/img/silk/arrow_refresh.png',
					tooltip: 'Reload the chart with the selected value per each intensity level',
					handler: function(){	
						var yAxis = chartPanel.getComponent('xyAxis').items.items[0].getValue();	
						var yAxisTitle = chartPanel.getComponent('xyAxis').items.items[0].getRawValue();
						chartPanel.getComponent('riskChart').items.items[0].setYField(yAxis);	
						chartPanel.getComponent('riskChart').items.items[0].setYAxis(new Ext.chart.NumericAxis({
							title: yAxisTitle,
							labelRenderer: (yAxis == 'risk_people_killed')? Ext.util.Format.numberRenderer('0,0.0000'): Ext.util.Format.numberRenderer('0,0')
						}));
						}
					},{
						name: 'vul_type',
						xtype: "combo",
						mode: "local",
						editable: false,
						store: new Ext.data.ArrayStore({
							id: 0,
							fields: ['myValue','displayText'],
							data: [['exposed_obj', 'No. of exposed objects'],['exposed_people', 'No. of exposed people'],
								['risk_obj', 'Risk of objects (amount/year)'], ['risk_people_killed', 'Risk of humans (persons/year)'],
								['risk_total', 'Risk Total (amount/year)']]  // data is local
						}),
						valueField: 'myValue',
						value: 'risk_total',
						displayField: 'displayText',
						triggerAction: 'all'
				}]
			},{	
				flex: 1,
				layout: 'fit',
				border: true,
				frame: true,	
				itemId: 'riskChart',
				title: 'Column Chart: Calculated risk for the selected risk scenario',
				items: [{
					xtype: 'columnchart',
					store: chartStore,												
					yField: 'risk_total',
					url: 'src/ext/resources/charts.swf',
					xField: 'intensity_level',
					xAxis: new Ext.chart.CategoryAxis({
						title: 'Intensity Level'
					}),
					yAxis: new Ext.chart.NumericAxis({
						title: 'Risk Total (amount/year)',
						labelRenderer : Ext.util.Format.numberRenderer('0,0')
					}),
					chartStyle: {
						padding: 10,
						animationEnabled: true,
						font: {
							name: 'Tahoma',
							color: 0x444444,
							size: 11
						},
						dataTip: {
							padding: 5,
							border: {
								color: 0x99bbe8,
								size:1
							},
							background: {
								color: 0xDAE7F6,
								alpha: .9
							},
							font: {
								name: 'Tahoma',
								color: 0x15428B,
								size: 10,
								bold: true
							},
							xAxis: {
								color: 0x69aBc8,
								majorTicks: {color: 0x69aBc8, length: 4},
								minorTicks: {color: 0x69aBc8, length: 2},
								majorGridLines: {size: 1, color: 0xeeeeee}
							},
							yAxis: {
								color: 0x69aBc8,
								majorTicks: {color: 0x69aBc8, length: 4},
								minorTicks: {color: 0x69aBc8, length: 2},
								majorGridLines: {size: 1, color: 0xdfe8f6}
							}
						}
					},
					extraStyle: {
						yAxis: {
							titleRotation: -90
						}
					}  
				}]
			},{	
				flex: 1,
				layout: 'fit',
				border: true,
				items: [dataGrid]
			}]
		});	
									
		// a window to show the panel 
		var chartWin = new Ext.Window({
			title: 'Summary statistics of the selected risk scenario: ' + config,
			width: 450,
			height: 500,
			layout: 'fit',
			plain: true,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: [chartPanel]
		});
													
		chartWin.show();
	}	
        
});

Ext.preg(riskgis.plugins.RiskAnalysis.prototype.ptype, riskgis.plugins.RiskAnalysis);
