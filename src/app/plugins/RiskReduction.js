/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */

/**
 * @requires plugins/Tool.js
 * @requires plugins/Alternatives.js
 * @requires plugins/CostBenefit.js
 */
 
 /** api: (define)
 *  module = riskgis.plugins
 *  class = RiskReduction
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("riskgis.plugins");

/** api: constructor
 *  .. class:: RiskReduction(config)
 *
 *    Provides actions for creating/viewing alternative (measures) scenarios, cost estimations and generation of CBA reports. 
 */
riskgis.plugins.RiskReduction = Ext.extend(gxp.plugins.Tool, {
	/** api: ptype = riskgis_RiskReduction */
    ptype: "riskgis_RiskReduction", 
	
	/** api: config[buttonText]
     *  ``String``
     *  Text for the Menu button (i18n).
     */
    buttonText: "Risk Reduction",
	
	/** api: config[RiskReductionTooltip]
     *  ``String``
     *  Text for RiskReduction action tooltip (i18n).
     */
    RiskReductionTooltip: "Tools for the risk reduction",

    /** private: method[constructor]
     */
    constructor: function(config) {
        riskgis.plugins.RiskReduction.superclass.constructor.apply(this, arguments);
    },

    /** private: method[destroy]
     */
    destroy: function() {
        this.button = null;
        riskgis.plugins.RiskReduction.superclass.destroy.apply(this, arguments);
    },
	
	/** api: method[addActions]
     */
    addActions: function() {
        this.button = new Ext.SplitButton({
            iconCls: "riskgis-icon-riskreduction",
            tooltip: this.RiskReductionTooltip,
            text: this.buttonText,           
            allowDepress: true,           
            scope: this,
            menu: new Ext.menu.Menu({
                items: [
					{
						text: 'Alternative',
						iconCls: "riskgis-icon-alternative",
						menu: {
							xtype: 'menu',
							items:[
								new Ext.menu.CheckItem(
									new Ext.Action({
										text: 'Create New',
										iconCls: "riskgis-icon-add-layer",									
										tooltip: 'Tool to create a new alternative scenario',
										listeners: {
											checkchange: function(item, checked) {
												if (checked) {
													this.button.setIconClass(item.iconCls);
												}
											},
											scope: this
										},
										handler: function(){											
											app.tools.alternatives.actions[0].execute('create');								
										}
									})   
								),
								new Ext.menu.CheckItem(
									new Ext.Action({
										text: 'View Scenarios',
										iconCls: "riskgis-icon-sceList",										
										tooltip: 'Tool to view the list of alternative scenarios',
										listeners: {
											checkchange: function(item, checked) {
												if (checked) {
													this.button.setIconClass(item.iconCls);
												}
											},
											scope: this
										},
										handler: function(){											
											app.tools.alternatives.actions[0].execute('view');								
										}
									})   
								)
							]
						}						
					},{
						text: 'Cost-Benefit Analysis',
						iconCls: "riskgis-icon-cba",
						menu: {
							xtype: 'menu',
							items:[
								new Ext.menu.CheckItem(
									new Ext.Action({
										text: 'Generate Report',
										iconCls: "riskgis-icon-add-report",										
										tooltip: 'Tool to create a new cost-benefit report',
										listeners: {
											checkchange: function(item, checked) {
												if (checked) {
													this.button.setIconClass(item.iconCls);
												}
											},
											scope: this
										},
										handler: function(){
											app.tools.costbenefit.actions[0].execute('create');									
										}
									})   
								),
								new Ext.menu.CheckItem(
									new Ext.Action({
										text: 'View Reports',
										iconCls: "riskgis-icon-table",										
										tooltip: 'Tool to view the list of cost-benefit reports',
										listeners: {
											checkchange: function(item, checked) {
												if (checked) {
													this.button.setIconClass(item.iconCls);
												}
											},
											scope: this
										},
										handler: function(){
											app.tools.costbenefit.actions[0].execute('view');									
										}
									})   
								)
							]
						}	
					}	
                ]
            })
        });
		
		return riskgis.plugins.RiskReduction.superclass.addActions.apply(this, [this.button]);
    },
	
	/** api: method[calculateCost]
     */
	calculateCost: function(id,sceName) {	
		var summary = new Ext.ux.grid.GroupSummary();
		
		var customRender = Ext.util.Format.frMoney = function(v) 	// plug Swiss Franc currency renderer into formatter
			{
				v = (Math.round((v-0)*100))/100;
				v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
				return (v + ' CHF');
			};
									
		Ext.ux.grid.GroupSummary.Calculations['totalCost'] = function(v, record, field){
			if (record.data.mesure_id == 16) {
				return v + parseFloat(record.get('cout_entretien'));															
			}
			else {
				return v + parseFloat(record.get('cout_entretien'))+ parseFloat(record.get('cout_exploitation'))+ parseFloat(record.get('cout_reparation')) + ((parseFloat(record.get('cout_investissement')) - parseFloat(record.get('valeur_residuelle')))/parseFloat(record.get('periode_ans')))+((parseFloat(record.get('cout_investissement')) + parseFloat(record.get('valeur_residuelle')))*parseFloat(record.get('taux_interet'))*0.005);
			}
		};
		
		var KbRender = function(v, params, record){
			if (record.data.mode == 'Auto') {
				switch (record.data.mesure_id) {
												case 1:
												case 4:
												case 6:
												case 7:
												case 8:
												case 9:
												case 10:
												case 11:
												case 12:
												case 18:
													var temp = parseFloat(record.get('cout_investissement')) * 0;
													record.set('cout_exploitation',	temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 2:
												case 3:
													var temp = parseFloat(record.get('cout_investissement')) * 0.005;
													record.set('cout_exploitation',	temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 5:
													var temp = parseFloat(record.get('cout_investissement')) * 0.05;
													record.set('cout_exploitation',	temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 13:
												case 15:
													var temp = parseFloat(record.get('cout_investissement')) * 0.01;
													record.set('cout_exploitation',	temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 14:
													var temp = parseFloat(record.get('cout_investissement')) * 0.02;
													record.set('cout_exploitation',	temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;												
				}
			}
			else {
				if (record.data.mesure_id == 16) {
					params.css = "disable-cell" ; //this changes the cell background-color 
					//	return '<span style="color:#000000; font-weight =bold">' + customRender(v) + '</span>';
				}
				else return customRender(v);
			}								
		};

		var KuRender = function(v, params, record){
										if (record.data.mode == 'Auto') {
											switch (record.data.mesure_id) {
												case 1:
													var temp = parseFloat(record.get('cout_investissement')) * 0.015 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 2:
												case 6:
												case 8:
												case 10:
												case 11:
												case 12:
													var temp = parseFloat(record.get('cout_investissement')) * 0.02 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 3:
												case 7:
													var temp = parseFloat(record.get('cout_investissement')) * 0.005 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 4:
												case 9:
												case 14:
												case 15:
													var temp = parseFloat(record.get('cout_investissement')) * 0.01 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 5:
													var temp = parseFloat(record.get('cout_investissement')) * 0.04 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 13:
													var temp = parseFloat(record.get('cout_investissement')) * 0.03 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 18:
													var temp = parseFloat(record.get('cout_investissement')) * 0 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
											}
										}
										else {
											return customRender(v);
											// if (record.data.mesure_id == 16) {
												// record.set('cout_entretien',v);
												// return customRender(v);		
											// }
											// else return customRender(v);
										}																
		};	
									
		var KrRender = function(v, params, record){
			if (record.data.mode == 'Auto') {
											switch (record.data.mesure_id) {
												case 1:
													var temp = parseFloat(record.get('cout_investissement')) * 0.015 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 2:
												case 6:
												case 8:
												case 10:
												case 11:
												case 12:
													var temp = parseFloat(record.get('cout_investissement')) * 0.02 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 3:
												case 7:
													var temp = parseFloat(record.get('cout_investissement')) * 0.005 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 4:
												case 9:
												case 14:
												case 15:												
													var temp = parseFloat(record.get('cout_investissement')) * 0.01 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 5:
													var temp = parseFloat(record.get('cout_investissement')) * 0.04 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 13:
													var temp = parseFloat(record.get('cout_investissement')) * 0.03 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 18:
													var temp = parseFloat(record.get('cout_investissement')) * 0 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
											}
										}
										else {
											if (record.data.mesure_id == 16) {
												params.css = "disable-cell" ; //this changes the cell background-color 
											//	return '<span style="color:#000000; font-weight =bold">' + customRender(v) + '</span>';
											}
											else return customRender(v);											
										}	
		};	
									
		var costEditGrid = new Ext.grid.EditorGridPanel({
			store: new Ext.data.GroupingStore({
				url: 'costInfo.php',
				baseParams: {
					task: 'loadCostValues',
					ws: 'riskgis_two',
					altID: id											
				},
				sortInfo:{field: 'mesure_id', direction: "ASC"},
				groupField:'alt_id',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [ 					
						{name : 'id', type : 'int'},
						{name : 'alt_id', type : 'int'},
						{name : 'mesure_id', type : 'int'},
						{name : 'ouvrage', type : 'String'},
						{name : 'mode', type : 'String'},
						{name : 'cout_investissement', type : 'String'}, 
						{name : 'cout_exploitation', type : 'String'}, 
						{name : 'cout_entretien', type : 'String'},
						{name : 'cout_reparation', type : 'String'},
						{name : 'periode_ans', type : 'int'},
						{name : 'valeur_residuelle', type : 'String'},
						{name : 'taux_interet', type : 'int'},
						{name : 'cout_annuel', type : 'String'}
					]
				}),
				autoLoad: true
			}),
			colModel: new Ext.grid.ColumnModel({
				defaults: {
					width: 120,
					sortable: true,
					menuDisabled: true
				},
				columns: [
					{header: "ID", sortable: true, dataIndex: 'id', hidden: true},
					{header: "Alt ID", sortable: true, dataIndex: 'alt_id', hidden: true},
					{header: "Measure ID", sortable: true, dataIndex: 'mesure_id', hidden: true},
					{header: "Type of the measure", sortable: true, dataIndex: 'ouvrage'},
					{header: "Mode", sortable: true, dataIndex: 'mode', editable: true, 
						editor: new Ext.form.ComboBox({
							store: new Ext.data.ArrayStore({
								fields: ["value", "text"],
								data: [
									['Auto', "Auto"],
									['Manuel', "Manual"]
								]
							}),
						valueField: "value",
						displayField: "text",
						mode: "local",
						triggerAction: 'all',
						allowBlank: false
					}),
					renderer: function(v, params, record){
						if (record.data.mesure_id == 16 || record.data.mesure_id ==17) {
							params.css = "disable-cell" ; //this changes the cell background-color 
							return '<span style="color:#000000; font-weight =bold">' + v + '</span>';
						}
						else return v;
					}},												
					{header: "Investment Cost", sortable: true, align: 'right', dataIndex: 'cout_investissement', renderer: function(v, params, record){
						if (record.data.mesure_id == 16) {
							params.css = "disable-cell" ; //this changes the cell background-color 
							//	return '<span style="color:#000000; font-weight =bold">' + customRender(v) + '</span>';
						}
						else return customRender(v);
					}, editable: true, editor: new Ext.form.NumberField()},
					{header: "Operating Cost", sortable: true, align: 'right',   dataIndex: 'cout_exploitation', renderer: KbRender, editable: true, editor: new Ext.form.NumberField()},
					{header: "Maintenance Cost", sortable: true,  align: 'right',  dataIndex: 'cout_entretien', renderer: KuRender, editable: true, editor: new Ext.form.NumberField()},
					{header: "Repair Cost", sortable: true,  align: 'right',  dataIndex: 'cout_reparation', renderer: KrRender, editable: true, editor: new Ext.form.NumberField()},
					{header: "No. of effective years", sortable: true, align: 'right', dataIndex: 'periode_ans', editable: true, editor: new Ext.form.NumberField({allowNegative: false,allowBlank: false}), renderer: function(v,params, record){
						if (record.data.mode == 'Auto') {
														switch (record.data.mesure_id) {
															case 1:
															case 2:
															case 4:
															case 15:
																record.set('periode_ans',80);
																params.css = "disable-cell" ; //this changes the cell background-color 
																return '<span style="color:#000000; font-weight =bold">' + 80 + ' years' + '</span>';																
																break;
															case 3:
															case 7:
																record.set('periode_ans',100);
																params.css = "disable-cell" ; //this changes the cell background-color 
																return '<span style="color:#000000; font-weight =bold">' + 100 + ' years' + '</span>';
																break;
															case 5:
															case 18:
																record.set('periode_ans',20);
																params.css = "disable-cell" ; //this changes the cell background-color 
																return '<span style="color:#000000; font-weight =bold">' + 20 + ' years' + '</span>';
																break;
															case 6:
															case 9:
															case 12:
															case 14:
																record.set('periode_ans',50);
																params.css = "disable-cell" ; //this changes the cell background-color 
																return '<span style="color:#000000; font-weight =bold">' + 50 + ' years' + '</span>';
																break;
															case 8:
															case 10:
															case 11:
															case 13:
																record.set('periode_ans',30);
																params.css = "disable-cell" ; //this changes the cell background-color 
																return '<span style="color:#000000; font-weight =bold">' + 30 + ' years' + '</span>';
																break;															
														}														
						}
						else {														
							if (record.data.mesure_id == 16) {
								params.css = "disable-cell" ; //this changes the cell background-color 
								//	return '<span style="color:#000000; font-weight =bold">' + v + ' years' + '</span>';
							}
							else return v +' years';
						}
					}},
					{header: "Residual Value (after N years)", sortable: true, align: 'right', dataIndex: 'valeur_residuelle', editable: true, editor: new Ext.form.NumberField(), renderer: function(v, params, record){
						if (record.data.mode == 'Auto') {
							record.set('valeur_residuelle',0);
							params.css = "disable-cell" ; //this changes the cell background-color 
							return '<span style="color:#000000; font-weight =bold">' + customRender(0) + '</span>';
						}
						else {
							if (record.data.mesure_id == 16) {
								params.css = "disable-cell" ; //this changes the cell background-color 
								//	return '<span style="color:#000000; font-weight =bold">' + customRender(v) + '</span>';
							}
							else return customRender(v);
						}
					}},
					{header: "Interest Rate (%)", sortable: true, align: 'right', dataIndex: 'taux_interet', editable: true, editor: new Ext.form.NumberField({allowNegative: false,allowBlank: false}), renderer: function(v, params, record){
						if (record.data.mode == 'Auto') {
							record.set('taux_interet',2);
							params.css = "disable-cell" ; //this changes the cell background-color 
							return '<span style="color:#000000; font-weight =bold">' + '2 %' + '</span>';
						}
						else {
							if (record.data.mesure_id == 16) {
								params.css = "disable-cell" ; //this changes the cell background-color 
								//	return '<span style="color:#000000; font-weight =bold">' + v +' %' + '</span>';
							}
							else return v +' %';
						}
					}},
					{header: "Annual Cost Total (per year)", sortable: true, align: 'right', dataIndex: 'cout_annuel', summaryType: 'totalCost', summaryRenderer: customRender, renderer: function(v, params, record){
						if (record.data.mesure_id == 16) {
							var total = parseFloat(record.get('cout_entretien'));															
						}
						else {
							var total = parseFloat(record.get('cout_entretien'))+ parseFloat(record.get('cout_exploitation'))+ parseFloat(record.get('cout_reparation')) + ((parseFloat(record.get('cout_investissement')) - parseFloat(record.get('valeur_residuelle')))/parseFloat(record.get('periode_ans')))+((parseFloat(record.get('cout_investissement')) + parseFloat(record.get('valeur_residuelle')))*parseFloat(record.get('taux_interet'))*0.005);
						}	
						//	var total = parseFloat(record.data.cout_entretien)+ parseFloat(record.data.cout_exploitation)+ parseFloat(record.data.cout_reparation) + ((parseFloat(record.data.cout_investissement) - parseFloat(record.data.valeur_residuelle))/parseFloat(record.data.periode_ans))+((parseFloat(record.data.cout_investissement) + parseFloat(record.data.valeur_residuelle))*parseFloat(record.data.taux_interet)*0.005);
						//	record.set('cout_annuel',total); 
							params.css = "disable-cell" ; //this changes the cell background-color 
							return '<span style="color:#000000; font-weight =bold">' + customRender(total) + '</span>';
						}
					}
				]	
			}),
			view: new Ext.grid.GroupingView({
				forceFit: true,
				showGroupName: false,
				enableNoGroups: false,
				enableGroupingMenu: false,
				hideGroupedColumn: true
			}),
			plugins: summary,									
			sm: new Ext.grid.RowSelectionModel({singleSelect:true}),												
			width: 700,
			height: 450,																
			frame: true,
			stripeRows : true,
			listeners: {
				beforeedit: function(e){
					if (e.record.get('mesure_id') == 17) {
						// disable editing for 'mode' combo box, in case of measure no. 16 (all other columns are allowed) 
						if (e.column == 4) return false;
						else return true;
					}
					else if (e.record.get('mesure_id') == 16) {
						// disable editing for 'mode' combo box, in case of measure no. 17 (only maintenance cost is allowed)
						if (e.column == 7) return true;
						else return false;
					}
					else {
						// enable editing for 'mode' combo box, in case of all other measures
						if (e.column == 4) return true;
													
						// if the selected cell is not 'mode' column 
						if (e.record.get('mode') == 'Auto') {
							if (e.column == 5) { 
								return true; // enable editing for investment cost column only
							}
							else {
								return false; // disable editing for other columns
							}
						}
						else {
							return true;	
						}			
					}																																	
				}
			},
			buttons: [
				{
					text: 'Save Values',
					iconCls: 'save',
					handler: function(){
						// to POST only the dirty records to update the values
						var records = [];
						var costEditStore = costEditGrid.getStore();
						for (var i= 0;i < costEditStore.data.length;i++) {
							recs = costEditStore.data.items[i];
							if (recs.dirty == true){
								records.push((recs.data));
							}
						}
												
						if (records.length != 0) {
							Ext.Ajax.request({
								url:'costInfo.php',
								params: {
									task: 'save',
									ws: 'riskgis_two'
								},
								jsonData: Ext.util.JSON.encode(records),
								success: function(response, opts) {
									costEditStore.commitChanges(); // unmark the dirty records and commit all changes to the records
									var obj = Ext.decode(response.responseText);										
									Ext.Msg.alert('Success', obj.message);	
								}
							});
						}
						else { Ext.Msg.alert('Information', 'There are no new values to be saved!');
						}
					}	
				}
			]
		});	
									
		var costEditWin = new Ext.Window({
			title: 'Calculate the cost sheet for: ' + sceName,	
			layout: 'fit',
			plain:true,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: [costEditGrid],
			tools:[
				{
					id:'help',
					qtip: 'Get Help',
					handler: function(event, toolEl, panel){
						Ext.Msg.show({
							title:'Help Information',
							msg: 'In this platform, measures and default parameter values for cost estimation are obtained from <a href="https://econome.ch/eco_work/index.php?PHPSESSID=94jep0nm74akn4qru80inier17&linkid=1" target="_blank">EconoMe</a> and <a href="https://www.vd.ch/index.php?id=61124" target="_blank">Valdorisk</a>. The annual cost of the measure is calculated using the <a href="./Brundl_2011.pdf" target="_blank">formula (Eq. 49)</a> used in EconoMe, as mentioned in Brundl et al. (2011, p. 14).',
							buttons: Ext.Msg.OK,															   
							animEl: 'elId',
							icon: Ext.MessageBox.INFO
						});
					}
				},{
					id:'search',
					qtip: 'Play Video',
					handler: function(event, toolEl, panel){
						vid = new Ext.Window({
							title: 'Video Tutorial: cost estimation',
							resizable: false,
							html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Xr8P1IA2j84?rel=0&amp;start=500" frameborder="0" allowfullscreen></iframe>'
						});
								
						vid.show();
					}
				}]
		});	
									
		costEditWin.show();
	}
	
	});

Ext.preg(riskgis.plugins.RiskReduction.prototype.ptype, riskgis.plugins.RiskReduction);