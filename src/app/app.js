/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */
 
/**
 * Add all your dependencies here.
 *
 * @require widgets/Viewer.js
 * @require plugins/LayerTree.js
 * @require plugins/LayerProperties.js
 * @require widgets/WMSLayerPanel.js 
 * @require plugins/OLSource.js
 * @require plugins/WMSCSource.js
 * @require plugins/OSMSource.js
 * @require plugins/MapBoxSource.js
 * @require plugins/GoogleSource.js
 * @require plugins/WMSCSource.js
 * @require plugins/ZoomToExtent.js
 * @require plugins/ZoomToLayerExtent.js
 * @require plugins/NavigationHistory.js
 * @require plugins/Zoom.js
 * @require plugins/Measure.js
 * @require plugins/AddLayers.js
 * @require plugins/RemoveLayer.js
 * @require plugins/WMSGetFeatureInfo.js
 * @require plugins/Legend.js
 * @require plugins/Styler.js
 * @require plugins/GoogleGeocoder.js
 * @require plugins/FeatureManager.js
 * @require plugins/FeatureEditor.js
 * @require plugins/SnappingAgent.js
 * @require plugins/FeatureGrid.js
 * @require plugins/FeatureEditorForm.js
 * @require plugins/RiskCalculator.js
 * @require plugins/RiskScenarios.js
 * @require plugins/RiskAnalysis.js
 * @require plugins/InputData.js
 * @require plugins/RiskReduction.js
 * @require OpenLayers/Control/MousePosition.js
 * @require RowExpander.js
 * @require Search.js
 * @require ColorManager.js
 * @require GroupSummary.js
 * @require LovCombo.js 

 */
 
var app = new gxp.Viewer({
    portalConfig: {		
        layout: "border",
        region: "center",  
		defaults: {
			split: true
		},
        // by configuring items here, we don't need to configure portalItems
        // and save a wrapping container
        items: [{
            id: "centerpanel",
            xtype: "panel",
            layout: "fit",
            region: "center",
            border: false,
			margins: '53 0 0 0',
            items: ["mymap"]
        }, {
			id: "westcontainer",
			xtype: "container",
			layout: "vbox",
			region: "west",
			margins: '53 0 0 0',
			split: false,
			width: 250,
			defaults: {
				layout: "fit",
				width: "100%",			
				autoScroll: true,
				containerScroll: true
			},
			items: [{
				title: "Layers",				
				id: "westpanel",
				border: false,
				flex: 1
			}, {
				id: "legendpanel",				
				height: 250
			}]
		},{
			id: "south",
			xtype: "panel",
			layout: "fit",
			region: "south",
			border: true,
			height: 200,
			minSize: 50,
			maxSize: 250
		}],
        bbar: {id: "mybbar"}
    },
    
    // configuration of all tool plugins for this application
    tools: [{
        ptype: "gxp_layertree",
        outputConfig: {
            id: "tree",
            border: true,
            tbar: [] // we will add buttons to "tree.bbar" later
        },
        outputTarget: "westpanel"
    }, {
        ptype: "gxp_addlayers",
        actionTarget: "tree.tbar",
		id: 'addlyrs'
    }, {
        ptype: "gxp_removelayer",
        actionTarget: ["tree.tbar", "tree.contextMenu"]
    }, {
        ptype: "gxp_layerproperties",		
        actionTarget: ["tree.tbar", "tree.contextMenu"]
    }, {
        ptype: "gxp_zoomtoextent",
        actionTarget: "map.tbar"
    }, {
		ptype: "gxp_zoomtolayerextent",
		actionTarget: ["tree.tbar", "tree.contextMenu"]
	},{
        ptype: "gxp_zoom",
        actionTarget: "map.tbar"
    }, {
        ptype: "gxp_navigationhistory",
        actionTarget: "map.tbar"
    },{
        ptype: "gxp_measure",
        actionTarget: "map.tbar"
    },{
		ptype: "gxp_wmsgetfeatureinfo",
		format: "grid",
		outputConfig: {
			width: 300
		},
		actionTarget: "map.tbar"
	},{
		ptype: "gxp_legend",
		outputTarget: "legendpanel",
		outputConfig: {
			autoHeight: true,
			autoWidth: true			
		}
	}, {
		ptype: "gxp_featureeditor",
		id: 'ftEditor',
		featureManager: "manager",
		autoLoadFeature: true,
		snappingAgent: "snapping-agent",
		supportNoGeometry: false, 
		splitButton: false,
		showSelectedOnly: false
	}, {
		ptype: "gxp_googlegeocoder",
		outputTarget: "map.tbar",
		outputConfig: {
			emptyText: "Search for a location ..."
		}
	}, /* {
		ptype: "gxp_styler",	
		actionTarget: ["tree.tbar", "tree.contextMenu"],
		sameOriginStyling: false,		
		requireDescribeLayer: false
	}, */ {
		ptype: "gxp_featuremanager",
		id: "manager",
		paging: true,
		autoSetLayer: true,
		autoLoadFeatures: true
	},{
		ptype: "gxp_snappingagent",
		id: "snapping-agent",
		autoSetLayer: true/* ,
		targets: [{
			source: "local",
			name: "debris_layer"
		}] */
	},{
		ptype: "gxp_featuregrid",
		featureManager: "manager",
		alwaysDisplayOnMap: true,
		selectOnMap: true,
		autoCollapse: true,
		outputConfig: {
			loadMask: true
		},
		outputTarget: "south",
		controlOptions: {  
			multiple: false, 
			hover: false,
            toggleKey: "ctrlKey", // ctrl key removes from selection
            multipleKey: "shiftKey" // shift key adds to selection
		}
	},{
		ptype: "riskgis_RiskCalculator",
		actionTarget: "",
		id: "calculator"
	},{
		ptype: "riskgis_RiskScenarios",
		actionTarget: "",
		id: "scenarios"
	},{
		ptype: "riskgis_InputData",
		actionTarget: "map.tbar",
		id: "inputdata"
	},{
		ptype: "riskgis_RiskAnalysis",
		actionTarget: "map.tbar",
		id: "riskanalysis"
	},{
		ptype: "riskgis_Hazards",
		actionTarget: "",
		id: "hazards"
	},{
		ptype: "riskgis_Objects",
		actionTarget: "",
		id: "objects"
	},{
		ptype: "riskgis_RiskReduction",
		actionTarget: "map.tbar",
		id: "riskreduction"
	},{
		ptype: "riskgis_Alternatives",
		actionTarget: "",
		id: "alternatives"
	},{
		ptype: "riskgis_CostBenefit",
		actionTarget: "",
		id: "costbenefit"
	}],
    
    // layer sources
    sources: {
        /*local: {
            ptype: "gxp_wmscsource",
            url: "/geoserver/wms/opengeo",
            version: "1.1.1"
        },*/
		local: {
            ptype: "gxp_wmscsource",
			url: "/geoserver/wsname/wms", // replaces with your wsname of the respective exercise
            version: "1.1.1",
			hidden: true
        }, 
        osm: {
            ptype: "gxp_osmsource"
        },
		google: {
			ptype: "gxp_googlesource"
		},
		ol: {
			ptype: "gxp_olsource"
		}, 
		mapbox: {
			ptype: "gxp_mapboxsource"
		}
    },
    
    // map and layers
    map: {
        id: "mymap", // id needed to reference map in portalConfig above
        title: "Map",
        projection: "EPSG:900913",
        center: [796657.18624, 5836769.57608],
        zoom: 10,
/* 		controls: [		
			new OpenLayers.Control.Navigation({
                zoomWheelOptions: {interval: 250},
				dragPanOptions: {enableKinetic: true}
            }),
            new OpenLayers.Control.PanPanel(),
            new OpenLayers.Control.ZoomPanel(),
            new OpenLayers.Control.Attribution(),
			new OpenLayers.Control.MousePosition({
			prefix: '<a target="_blank" ' +
				'href="http://spatialreference.org/ref/sr-org/7483/">' +
				'EPSG:900913</a> coordinates: '
			})
		], */
        layers: [{
            source: "google",
            name: "TERRAIN",
            group: "background"
        },{
            source: "google",
            name: "SATELLITE",
            group: "background"
        },{
            source: "osm",
            name: "mapnik",
            group: "background"
        }],
        items: [{
            xtype: "gx_zoomslider",
            vertical: true,
            height: 100
        }]
    }

});		
	
	// make certain layers non-editable 
	app.on("layerselectionchange", function() {
		if (!!app.selectedLayer) {
			if ( app.selectedLayer.get("name") == 'debris_brienz' || app.selectedLayer.get("name") == 'buildings_brienz') {		
				app.tools.ftEditor.readOnly = true; 
			}
			else {			
				app.tools.ftEditor.readOnly = false;
			} 
		}	
    });
