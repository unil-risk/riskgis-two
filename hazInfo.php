<?php
	require_once 'dbConnect.php'; // Connect to the database
	require_once 'geoServerConfig.php'; // GeoServer configurations
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$userID = $_POST['userID'];	
	$userRole = $_POST['userRole'];
	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'loadSelected') {
		$hazType = $_POST['type'];
		// allow all layers to be visible if logged with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT nom, indice FROM ".$workspace.".hazards WHERE type = '$hazType';";	
		}
		else {
			// allow only own and admin layers to be visible if logged with other roles
			$query = "SELECT nom, indice FROM ".$workspace.".hazards WHERE type = '$hazType' 
			AND (hazards.id_utilisateur = $userID OR hazards.id_utilisateur = (SELECT id FROM $workspace.users WHERE id = hazards.id_utilisateur AND role = 'admin'));";
		}
					 
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
				
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'load') {
		// allow all layers to be visible if logged with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT hazards.*, alternatives.nom as alt_nom, user_name AS nom_utilisateur 
			FROM ".$workspace.".hazards, ".$workspace.".hazards_alternatives, ".$workspace.".alternatives, ".$workspace.".users 
			WHERE hazards_alternatives.processus_id = hazards.id AND alternatives.id = hazards_alternatives.alt_id AND hazards.id_utilisateur = users.id;"; 		
		}
		else { 
			// allow only own and admin layers to be visible if logged with other roles
			$query = "SELECT hazards.*, alternatives.nom as alt_nom, user_name AS nom_utilisateur 
			FROM ".$workspace.".hazards, ".$workspace.".hazards_alternatives, ".$workspace.".alternatives, ".$workspace.".users 
			WHERE hazards_alternatives.processus_id = hazards.id AND alternatives.id = hazards_alternatives.alt_id AND hazards.id_utilisateur = users.id
			AND (hazards.id_utilisateur = $userID OR hazards.id_utilisateur = (SELECT id FROM $workspace.users WHERE id = hazards.id_utilisateur AND role = 'admin'));"; 	
		}		
		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
				
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
	// retrieve POST data 
		$name = $_POST['haz_name'];
		$description = $_POST['haz_description'];
		$remarks = $_POST['haz_remarks'];
		$return_period = $_POST['haz_years'];
		$type = $_POST['haz_type'];
		$hazAltID = $_POST['hazAltID'];
		$copy_lyr_name = $_POST['haz_import_name'];
		$option = $_POST['option'];	
		
		$mapping_index = preg_replace('/\s+/','_',$name); // replace whitespaces and spaces with underscore 
		$mapping_index = strtolower($mapping_index); // change to lowercase // change later to a unique index name 
		
		$minx = $_POST['LatLon_minx'];
		$miny = $_POST['LatLon_miny'];
		$maxx = $_POST['LatLon_maxx'];
		$maxy = $_POST['LatLon_maxy'];
		
		if ($option == 'Sketch') {
		### if sketch option, create a new empty table 
			$query = "CREATE TABLE ".$workspace.".".$mapping_index." (
					  fid serial NOT NULL,
					  geom geometry(MultiPolygon,4326),			  
					  intensity_level integer NOT NULL,
					  remarques text,
					  CONSTRAINT ".$mapping_index."_pkey PRIMARY KEY (fid),
					  CONSTRAINT enforce_dims_the_geom CHECK (st_ndims(geom) = 2), 
					  CONSTRAINT enforce_geotype_the_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text OR geom IS NULL),
					  CONSTRAINT enforce_srid_the_geom CHECK (st_srid(geom) = 4326)
					)
					WITH (
					  OIDS=FALSE
					);";
			
			### add a record to the hazards table with meta info
			### save relationship records to the hazards_alternatives table
			
			$query .= "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, id_utilisateur)
					VALUES(default,'$name','$description','$type','$remarks','$name','$mapping_index',$return_period, $userID);";
			
			$query .= "INSERT INTO $workspace.hazards_alternatives VALUES ((SELECT id FROM ".$workspace.".hazards WHERE indice = '$mapping_index'), $hazAltID);";
			
			If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
			else {				
				// Initiate cURL session				
				$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes"; // to add a new featuretype

				$url = $service . $request;
				$ch = curl_init($url);

				// Optional settings for debugging
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
				curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

				//Required POST request settings
				curl_setopt($ch, CURLOPT_POST, True);				
				curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

				//POST data
				curl_setopt($ch, CURLOPT_HTTPHEADER,
					array("Content-type: application/xml"));
				$xmlStr = "<featureType><name>".$mapping_index."</name><title>".$name."</title><latLonBoundingBox><minx>".$minx."</minx><maxx>".$maxx."</maxx><miny>".$miny."</miny><maxy>".$maxy."</maxy><crs>EPSG:4326</crs></latLonBoundingBox></featureType>";
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

				//POST return code
				$successCode = 201;

				// Execute the curl request
				$buffer = curl_exec($ch); 

				// Check for errors and process results
				$info = curl_getinfo($ch);
				if ($info['http_code'] != $successCode) {
					$msgStr = "# Unsuccessful cURL request to ";
					$msgStr .= $url." [". $info['http_code']. "]\n";
					Echo '{success:false,message:'.json_encode($msgStr).'}';
				} else {					
					$msgStr = "# Successful cURL request to ".$url."\n";
					Echo '{success: true, mpIndex:'.json_encode($mapping_index).',message:"The new hazard layer has been created and added to the map. You can now start drawing using CREATE and EDIT feature tools of the map panel!"}';
				}

				// free resources if curl handle will not be reused
				curl_close($ch); 
			}
		}	
		else{
			### if import option, import the existing layer
			$str = $workspaceGeoServer.':'.$mapping_index;
	
			### Step 1: publish the selected layer with a default styled called "hazard_style"	
			
			$xml = '<?xml version="1.0" encoding="UTF-8"?><wps:Execute version="1.0.0" service="WPS" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opengis.net/wps/1.0.0" xmlns:wfs="http://www.opengis.net/wfs" xmlns:wps="http://www.opengis.net/wps/1.0.0" xmlns:ows="http://www.opengis.net/ows/1.1" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:wcs="http://www.opengis.net/wcs/1.1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd"><ows:Identifier>gs:Import</ows:Identifier><wps:DataInputs><wps:Input><ows:Identifier>features</ows:Identifier><wps:Reference mimeType="text/xml" xlink:href="http://geoserver/wfs" method="POST"><wps:Body><wfs:GetFeature service="WFS" version="1.0.0" outputFormat="GML2" xmlns:'.$workspaceGeoServer.'="http://'.$workspaceGeoServer.'"><wfs:Query typeName="'.$workspaceGeoServer.':'.$copy_lyr_name.'"/></wfs:GetFeature></wps:Body></wps:Reference></wps:Input><wps:Input><ows:Identifier>workspace</ows:Identifier><wps:Data><wps:LiteralData>'.$workspaceGeoServer.'</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>store</ows:Identifier><wps:Data><wps:LiteralData>postgis</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>name</ows:Identifier><wps:Data><wps:LiteralData>'.$mapping_index.'</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>styleName</ows:Identifier><wps:Data><wps:LiteralData>hazard_style</wps:LiteralData></wps:Data></wps:Input></wps:DataInputs><wps:ResponseForm><wps:RawDataOutput><ows:Identifier>layerName</ows:Identifier></wps:RawDataOutput></wps:ResponseForm></wps:Execute>';
				
				// Initiate cURL session
				$link = "http://localhost:8080/geoserver/wps"; // WPS request URL
				$ch = curl_init($link);

				//Required request settings
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
				curl_setopt($ch, CURLOPT_POST, true); // -X POST		
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: xml")); // -H
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);				 
				
				curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

				//PUT return code: status OK
				$successCode = 200;

				$data = curl_exec($ch); // Execute the curl request
				$info = curl_getinfo($ch);
				
				if ($info['http_code'] != $successCode) {
						$msgStr = "# Unsuccessful cURL request to ";
						$msgStr .= $link." [". $info['http_code']. "]\n";
						Echo '{success:false,message:'.json_encode($msgStr).'}';
					}
				else {
					### Step 2: if returned data from WPS process is the layer name, save the records in the database in the hazards table
					### Step 3: save relationship records to the hazards_alternatives table					
					
					if ($data == $str) { // if success
						$query = "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, id_utilisateur)
						VALUES(default,'$name','$description','$type','$remarks','$copy_lyr_name','$mapping_index',$return_period, $userID);";
			
						$query .= "INSERT INTO $workspace.hazards_alternatives VALUES ((SELECT id FROM ".$workspace.".hazards WHERE indice = '$mapping_index'), $hazAltID);";
						
						If (!$rs = pg_query($dbconn,$query)) {
							Echo '{success:false,mpIndex:'.json_encode($mapping_index).',message:'.json_encode(pg_last_error($dbconn)).'}';
						}
						else {														
							Echo '{success:true,mpIndex:'.json_encode($mapping_index).',message: "The new hazard layer has been created and added to the map. You can now start editing using CREATE and EDIT feature tools of the map panel!"}';
							
						}
					}
					else { // if not success, still return the WPS error message
						Echo '{success:false,message:'.json_encode($data).'}';
					}
					
				}
				
				curl_close($ch);	
		}
	}
	
	if ($task == 'edit') {
		
		// Get the layer name 
		$mpIndex = $_POST['mpIndex'];		
		$ID = $_POST['ID'];	
		$nom = $_POST['name'];
		$desc = $_POST['description'];
		$ans = (int)$_POST['year'];
		$type = $_POST['haz_type'];
		$remarques = $_POST['remarks'];
		
			// Initiate cURL session			
			$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes/".$mpIndex.".xml"; // to edit a feature store

			$url = $service . $request;
			$ch = curl_init($url);

			// Optional settings for debugging
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

			//Required PUT request settings
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');			
			curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
			
			//POST data
			curl_setopt($ch, CURLOPT_HTTPHEADER,
				array("Content-type: application/xml"));
			$xmlStr = "<featureType><title>".$nom."</title><keywords><string>features</string><string>".$nom."</string></keywords><enabled>true</enabled></featureType>";
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

			//POST return code
			$successCode = 200;

			// Execute the curl request
			$buffer = curl_exec($ch); 
			
			$info = curl_getinfo($ch);
			curl_close($ch); // free resources if curl handle will not be reused
			
			if ($info['http_code'] != $successCode) {
				Echo '{success:false, message:"There is an error in changing the layer title in GeoServer!"}';
			}
			else {
				// if success, update the record in the table
				$query = "UPDATE ".$workspace.".hazards SET nom = '$nom', description = '$desc', type = '$type', remarques = '$remarques', temp_de_retour = $ans WHERE id = $ID;";
				$arr = array();
				
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {
					Echo '{success:true,message:"The layer information has been successfully updated!"}';		
				}				
			}
	}

	if ($task == 'delete') {
		$temp = $_POST['IDs'];		
		$hazRecords = json_decode($temp, true);
		$length = count($hazRecords);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected hazard layers
			$ID = $hazRecords[$i]['id'];			
			$mpIndex = $hazRecords[$i]['indice'];
			
			### Step1: remove the records from the hazards and its referenced records in other tables
			$query = "DELETE FROM ".$workspace.".hazards WHERE id = $ID;";
			
			### Step2: drop the respective layer table using indice column from the db (including the dependent view)
			$query .= "DROP TABLE ".$workspace.".$mpIndex CASCADE;";
				
			if (!$rs = pg_query($dbconn,$query)){			
				$message .= 'Failed to delete the record: '.$hazRecords[$i]['nom'].' due to the error: '.json_encode(pg_last_error($dbconn));						
			}
			else {
				// if success
				### Step3: remove the published map from the geoserver
					// Initiate cURL session					
					$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes/".$mpIndex.".xml?recurse=true"; // to delete a feature type

					$url = $service . $request;
					$ch = curl_init($url);

					// Optional settings for debugging
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
					curl_setopt($ch, CURLOPT_VERBOSE, true);
					curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

					//Required DELETE request settings
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");					
					curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

					//POST return code
					$successCode = 200;

					// Execute the curl request
					$buffer = curl_exec($ch); 

					// Check for errors and process results
					$info = curl_getinfo($ch);			
					if ($info['http_code'] != $successCode) {
						$message .= "Unuccessful cURL request to ".$url."\n";						
					}
					else {
						$message .= 'The selected map layer: '.$hazRecords[$i]['nom'].' has been deleted!';
					}
					curl_close($ch); // free resources if curl handle will not be reused
					
			}
		}
		Echo '{success: true, message:'.json_encode($message).'}';
	}
?>