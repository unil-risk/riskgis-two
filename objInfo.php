<?php
	require_once 'dbConnect.php'; // Connect to the database
	require_once 'geoServerConfig.php'; // GeoServer configurations
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$userID = $_POST['userID'];	
	$userRole = $_POST['userRole'];
	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'loadSelected') {
		$objType = $_POST['type'];
		// allow all layers to be visible if logged with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT nom, indice FROM ".$workspace.".objects WHERE type = '$objType';"; 
		}
		else {
			// allow only own and admin layers to be visible if logged with other roles
			$query = "SELECT nom, indice FROM ".$workspace.".objects WHERE type = '$objType'
			AND (objects.id_utilisateur = $userID OR objects.id_utilisateur = (SELECT id FROM $workspace.users WHERE id = objects.id_utilisateur AND role = 'admin'));"; 
		}
		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
				
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'load') {
		// allow all layers to be visible if logged with 'admin' role
		if ($userRole == 'admin') {		
			$query = "SELECT objects.*, alternatives.nom as alt_nom, user_name AS nom_utilisateur 
			FROM ".$workspace.".objects, ".$workspace.".objects_alternatives, ".$workspace.".alternatives, ".$workspace.".users  
			WHERE objects_alternatives.objet_id = objects.id AND alternatives.id = objects_alternatives.alt_id AND objects.id_utilisateur = users.id;"; 		
		}
		else {
			// allow only own and admin layers to be visible if logged with other roles
			$query = "SELECT objects.*, alternatives.nom as alt_nom, user_name AS nom_utilisateur
			FROM ".$workspace.".objects, ".$workspace.".objects_alternatives, ".$workspace.".alternatives, ".$workspace.".users  
			WHERE objects_alternatives.objet_id = objects.id AND alternatives.id = objects_alternatives.alt_id AND objects.id_utilisateur = users.id
			AND (objects.id_utilisateur = $userID OR objects.id_utilisateur = (SELECT id FROM $workspace.users WHERE id = objects.id_utilisateur AND role = 'admin'));"; 
		}
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
				
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
	// retrieve POST data 
		$name = $_POST['obj_name'];
		$description = $_POST['obj_description'];
		$remarks = $_POST['obj_remarks'];
		$type = $_POST['obj_type'];
		$objAltID = $_POST['objAltID'];
		$copy_lyr_name = $_POST['obj_import_name'];
		$option = $_POST['option'];	
		
		$mapping_index = preg_replace('/\s+/','_',$name); // replace whitespaces and spaces with underscore 
		$mapping_index = strtolower($mapping_index); // change to lowercase // change later to a unique index name 
		
		if ($option == 'Import') {
			### if import option, import the existing layer
			$str = $workspaceGeoServer.':'.$mapping_index;
	
			### Step 1: publish the selected layer 	
			
			$xml = '<?xml version="1.0" encoding="UTF-8"?><wps:Execute version="1.0.0" service="WPS" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opengis.net/wps/1.0.0" xmlns:wfs="http://www.opengis.net/wfs" xmlns:wps="http://www.opengis.net/wps/1.0.0" xmlns:ows="http://www.opengis.net/ows/1.1" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:wcs="http://www.opengis.net/wcs/1.1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd"><ows:Identifier>gs:Import</ows:Identifier><wps:DataInputs><wps:Input><ows:Identifier>features</ows:Identifier><wps:Reference mimeType="text/xml" xlink:href="http://geoserver/wfs" method="POST"><wps:Body><wfs:GetFeature service="WFS" version="1.0.0" outputFormat="GML2" xmlns:'.$workspaceGeoServer.'="http://'.$workspaceGeoServer.'"><wfs:Query typeName="'.$workspaceGeoServer.':'.$copy_lyr_name.'"/></wfs:GetFeature></wps:Body></wps:Reference></wps:Input><wps:Input><ows:Identifier>workspace</ows:Identifier><wps:Data><wps:LiteralData>'.$workspaceGeoServer.'</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>store</ows:Identifier><wps:Data><wps:LiteralData>postgis</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>name</ows:Identifier><wps:Data><wps:LiteralData>'.$mapping_index.'</wps:LiteralData></wps:Data></wps:Input></wps:DataInputs><wps:ResponseForm><wps:RawDataOutput><ows:Identifier>layerName</ows:Identifier></wps:RawDataOutput></wps:ResponseForm></wps:Execute>';
				
				// Initiate cURL session
				$link = "http://localhost:8080/geoserver/wps"; // WPS request URL
				$ch = curl_init($link);

				//Required request settings
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
				curl_setopt($ch, CURLOPT_POST, true); // -X POST		
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: xml")); // -H
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);				
				
				curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

				//PUT return code: status OK
				$successCode = 200;

				$data = curl_exec($ch); // Execute the curl request
				$info = curl_getinfo($ch);
				
				if ($info['http_code'] != $successCode) {
						$msgStr = "# Unsuccessful cURL request to ";
						$msgStr .= $link." [". $info['http_code']. "]\n";
						Echo '{success:false,message:'.json_encode($msgStr).'}';
					}
				else {
					### Step 2: if returned data from WPS process is the layer name, save the records in the database in the objects table
					### Step 3: save relationship records to the objects_alternatives table
					
					if ($data == $str) { // if success
						$query = "INSERT INTO $workspace.objects(id, nom, description, type, remarques, nom_origine, indice, id_utilisateur)
						VALUES(default,'$name','$description','$type','$remarks','$copy_lyr_name','$mapping_index', $userID);";
			
						$query .= "INSERT INTO $workspace.objects_alternatives VALUES ((SELECT id FROM ".$workspace.".objects WHERE indice = '$mapping_index'), $objAltID);";
						
						If (!$rs = pg_query($dbconn,$query)) {
							Echo '{success:false,mpIndex:'.json_encode($mapping_index).',message:'.json_encode(pg_last_error($dbconn)).'}';
						}
						else {
							Echo '{success:true,mpIndex:'.json_encode($mapping_index).',message: "The new object layer has been created and added to the map. You can now start editing using CREATE and EDIT feature tools of the map panel!"}';
						}
					}
					else { // if not success, still return the WPS error message
						Echo '{success:false,message:'.json_encode($data).'}';
					}
					
				}
				
				curl_close($ch);	
		}
	}
	
	if ($task == 'edit') {
		
		// Get the layer name 
		$mpIndex = $_POST['mpIndex'];		
		$ID = $_POST['ID'];	
		$nom = $_POST['name'];
		$desc = $_POST['description'];
		$type = $_POST['obj_type'];
		$remarques = $_POST['remarks'];
		
			// Initiate cURL session			
			$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes/".$mpIndex.".xml"; // to edit a feature store

			$url = $service . $request;
			$ch = curl_init($url);

			// Optional settings for debugging
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

			//Required PUT request settings
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');			
			curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
			
			//POST data
			curl_setopt($ch, CURLOPT_HTTPHEADER,
				array("Content-type: application/xml"));
			$xmlStr = "<featureType><title>".$nom."</title><keywords><string>features</string><string>".$nom."</string></keywords><enabled>true</enabled></featureType>";
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

			//POST return code
			$successCode = 200;

			// Execute the curl request
			$buffer = curl_exec($ch); 
			
			$info = curl_getinfo($ch);
			curl_close($ch); // free resources if curl handle will not be reused
			
			if ($info['http_code'] != $successCode) {
				Echo '{success:false, message:"There is an error in changing the layer title in GeoServer!"}';
			}
			else {
				// if success, update the record in the table
				$query = "UPDATE ".$workspace.".objects SET nom = '$nom', description = '$desc', type = '$type', remarques = '$remarques' WHERE id = $ID;";
				$arr = array();
				
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {
					Echo '{success:true,message:"The layer information has been successfully updated!"}';		
				}				
			}
	}

	if ($task == 'delete') {
		$temp = $_POST['IDs'];		
		$objRecords = json_decode($temp, true);
		$length = count($objRecords);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected object layers
			$ID = $objRecords[$i]['id'];			
			$mpIndex = $objRecords[$i]['indice'];
			
			### Step1: remove the records from the objects and its referenced records in other tables
			$query = "DELETE FROM ".$workspace.".objects WHERE id = $ID;";
			
			### Step2: drop the respective layer table using indice column from the db (including the dependent view)
			$query .= "DROP TABLE ".$workspace.".$mpIndex CASCADE;";
				
			if (!$rs = pg_query($dbconn,$query)){			
				$message .= 'Failed to delete the record: '.$objRecords[$i]['nom'].' due to the error: '.json_encode(pg_last_error($dbconn));						
			}
			else {
				// if success
				### Step3: remove the published map from the geoserver
					// Initiate cURL session					
					$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes/".$mpIndex.".xml?recurse=true"; // to delete a feature type

					$url = $service . $request;
					$ch = curl_init($url);

					// Optional settings for debugging
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
					curl_setopt($ch, CURLOPT_VERBOSE, true);
					curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

					//Required DELETE request settings
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");				
					curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

					//POST return code
					$successCode = 200;

					// Execute the curl request
					$buffer = curl_exec($ch); 

					// Check for errors and process results
					$info = curl_getinfo($ch);			
					if ($info['http_code'] != $successCode) {
						$message .= "Unuccessful cURL request to ".$url."\n";						
					}
					else {
						$message .= 'The selected map layer: '.$objRecords[$i]['nom'].' has been deleted!';
					}
					curl_close($ch); // free resources if curl handle will not be reused
					
			}
		}
		Echo '{success: true, message:'.json_encode($message).'}';
	}	
	
?>