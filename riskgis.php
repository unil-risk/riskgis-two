<?php
	session_start();
	if(!isset($_SESSION['usrname'])){
		header("location:../riskgis/index.php");	
	}
?>

<html>
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
	
        <title>RiskGIS - Exercise II: A learning environment for risk management of natural hazards</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="./favicon.ico">
		
		<!-- Bootstrap Core CSS -->
		<link href="../riskgis/startbootstrap-grayscale-1.0.6/css/bootstrap.css" rel="stylesheet">
		
		<!-- Custom Fonts -->
		<link href="../riskgis/startbootstrap-grayscale-1.0.6/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        
        <!-- Ext resources -->
        <link rel="stylesheet" type="text/css" href="src/ext/resources/css/ext-all.css">
        <link rel="stylesheet" type="text/css" href="src/ext/resources/css/xtheme-gray.css">
        <script type="text/javascript" src="src/ext/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="src/ext/ext-all.js"></script>

        <!-- OpenLayers resources -->
        <link rel="stylesheet" type="text/css" href="src/openlayers/theme/default/style.css">

        <!-- GeoExt resources -->
        <link rel="stylesheet" type="text/css" href="src/geoext/resources/css/popup.css">
        <link rel="stylesheet" type="text/css" href="src/geoext/resources/css/layerlegend.css">
        <link rel="stylesheet" type="text/css" href="src/geoext/resources/css/gxtheme-gray.css">

        <!-- gxp resources -->
        <link rel="stylesheet" type="text/css" href="src/gxp/theme/all.css">        

        <!-- app resources -->
        <link rel="stylesheet" type="text/css" href="theme/app/style.css">
		<link rel="stylesheet" type="text/css" href="theme/app/GroupSummary.css">
		<link rel="stylesheet" type="text/css" href="theme/app/LovCombo.css">
		
		<script>var role = "<?php echo $_SESSION['role']; ?>";</script>
		<script>var usrname = "<?php echo $_SESSION['usrname']; ?>";</script>
		<script>var userid = "<?php echo $_SESSION['userid']; ?>";</script>
        <script type="text/javascript" src="lib/app.js"></script>
        <script>
            Ext.BLANK_IMAGE_URL = "theme/app/img/blank.gif";
            OpenLayers.ImgPath = "externals/openlayers/img/";
        </script>
    </head>
	
	<body>
	<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="../riskgis/index.php" target='_blank'>
                    <i class="fa fa-play-circle"></i>  <span class="light">RISKGIS</span> 
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">  					
					<li><a href="https://youtu.be/Xr8P1IA2j84" target='_blank'>VIDEO TUTORIAL</a></li>
					<li><a href="./Tutorial_fr.pdf" target='_blank'>DOCUMENTATION</a></li>
					<li><a href="../riskgis/logout.php">LOGOUT</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	
	</body>

	
</html>
