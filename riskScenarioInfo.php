<?php
	require_once 'dbConnect.php'; // Connect to the database
	require_once 'geoServerConfig.php'; // GeoServer configurations
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$userID = $_POST['userID'];	
	$userRole = $_POST['userRole'];	
	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'load') {
		// allow all layers to be visible if logged with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT risk_scenarios.*, alternatives.nom AS alt_nom, hazards.nom AS processus_nom, objects.nom AS objet_nom, user_name AS nom_utilisateur
			FROM ".$workspace.".risk_scenarios, ".$workspace.".hazards, ".$workspace.".objects, ".$workspace.".alternatives, ".$workspace.".users  
			WHERE risk_scenarios.processus_id = hazards.id
			AND risk_scenarios.objet_id = objects.id
			AND risk_scenarios.alt_id = alternatives.id
			AND risk_scenarios.id_utilisateur = users.id;";
		}
		else {
			// allow only own layers to be visible if logged with other roles
			$query = "SELECT risk_scenarios.*, alternatives.nom AS alt_nom, hazards.nom AS processus_nom, objects.nom AS objet_nom, user_name AS nom_utilisateur
			FROM ".$workspace.".risk_scenarios, ".$workspace.".hazards, ".$workspace.".objects, ".$workspace.".alternatives, ".$workspace.".users  
			WHERE risk_scenarios.processus_id = hazards.id
			AND risk_scenarios.objet_id = objects.id
			AND risk_scenarios.alt_id = alternatives.id
			AND risk_scenarios.id_utilisateur = $userID
			AND risk_scenarios.id_utilisateur = users.id;";
		}
			
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}	
	}
	
	if ($task == 'loadChartStoreGrid') {
		$mpIndex = $_POST['mpIndex'];
		
		$query = "SELECT (frequency)^(-1) as return_period, intensity as intensity_level, count(*) as exposed_obj, sum(risk_obj) as risk_obj, sum(no_person) as exposed_people, sum(risk_person) as risk_people_killed, sum(risk_obj) + sum(risk_amount_person) as risk_total
					FROM $workspace.$mpIndex
					GROUP BY frequency, intensity
					ORDER BY intensity asc;"; 
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}	
		
	if ($task == 'add') {
		
		// retrieve POST data submitted by form
		$scenario_name = $_POST['scenario_name'];
		$scenario_description = $_POST['scenario_description'];
		$altID = $_POST['alt_scenario_id'];
		$option = $_POST['option'];	
		$area = $_POST['poly'];
		
		// retrieve POST data submitted by form (hazard tab)
		$haz_type = $_POST['haz_type']; 
		$haz_name = $_POST['haz_name'];
		
		// retrieve POST data submitted by form (object tab)
		$object_type = $_POST['object_type'];	
		$object_name = $_POST['object_name']; 
		$objRecords = $_POST['objRecords'];		
		$phpArrayObj = json_decode($objRecords, true);
		$price_sq_m = $phpArrayObj[0]['value'];	
		$no_person = $phpArrayObj[1]['value'];
		$exp_person_hour = $phpArrayObj[2]['value'];
		$exp_person = $phpArrayObj[2]['value']/24;
		$value_person = $phpArrayObj[3]['value'];
		
		// retrieve POST data submitted by form (vulnerability tab)
		$vulRecords = $_POST['vulRecords'];		
		$phpArrayVul = json_decode($vulRecords, true);
		
		// a new risk scenario table name to store the affected object records
		$mapping_index = preg_replace('/\s+/','_',$scenario_name); // replace whitespaces and spaces with underscore 
		$mapping_index = strtolower($mapping_index); // change to lowercase // change later to a unique index name 	
		
		### query to create a basic table for affected objects 		
		if ($option == 'L') { // if option is Layer-based, take object layer as a whole for calculation
			$query .= "CREATE TABLE $workspace.$mapping_index AS (
				SELECT distinct $object_name.geom, $object_name.id as id, area as surface, max(intensity) OVER (PARTITION BY $object_name.id) as intensity
				FROM $workspace.$haz_name, $workspace.$object_name
				WHERE ST_Intersects($object_name.geom, $haz_name.geom)
				ORDER BY $object_name.id asc
			);";
		}
		else { // if option is Zone-based, take object layer within the defined zone for calculation
			$polyGeomStr = "ST_Transform(ST_GeomFromText('$area',900913),4326)";
			$query .= "CREATE TABLE $workspace.$mapping_index AS (
				WITH obj AS (SELECT *
				FROM $workspace.$object_name
				WHERE ST_Intersects(geom,$polyGeomStr))
				SELECT distinct obj.geom, obj.id as id, area as surface, max(intensity) OVER (PARTITION BY obj.id) as intensity								
				FROM $workspace.$haz_name, obj			
				WHERE ST_Intersects(obj.geom, $haz_name.geom)				
				ORDER BY obj.id asc
			);";			
		}
		
		### query to add new columns 
		$query .= "ALTER TABLE ".$workspace.".".$mapping_index." 
				ADD COLUMN frequency double precision, 
				ADD COLUMN price_sq_m double precision,				
				ADD COLUMN amount double precision, 
				ADD COLUMN vul_obj double precision, 
				ADD COLUMN no_person double precision,	
				ADD COLUMN exp_person double precision,	
				ADD COLUMN vul_person double precision,
				ADD COLUMN value_person double precision,	
				ADD COLUMN risk_obj double precision,
				ADD COLUMN risk_person double precision,
				ADD COLUMN risk_amount_person double precision,
				ADD COLUMN risk_individual double precision
			;"; 
		
		### query to populate the frequency value
		$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET frequency = (t1.temp_de_retour)^(-1)
			FROM (SELECT temp_de_retour FROM $workspace.hazards WHERE indice = '$haz_name') AS t1;";		
			
		### query to populate the price_sq_m, no_person, exp_person and value_person values
		$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET price_sq_m = $price_sq_m, no_person = $no_person, exp_person = $exp_person, value_person = $value_person;";
		
		### query to populate the total amount column 	
		$query .= "UPDATE ".$workspace.".".$mapping_index." SET amount = price_sq_m * surface;";
		
		### query to populate the vulnerability value of the object and people inside within the object
		for ($i = 0; $i < count($phpArrayVul); $i++) {
			$intensity_level = $phpArrayVul[$i]['id'];
			$vul_obj = $phpArrayVul[$i]['damage_object']/100;
			$vul_person = $phpArrayVul[$i]['damage_people']/100;
			$query .= "UPDATE ".$workspace.".".$mapping_index." SET vul_obj = $vul_obj, vul_person = $vul_person WHERE intensity = $intensity_level;";
		}
		
		### query to populate the risk total of the objects and people		 	
		$query .= "UPDATE ".$workspace.".".$mapping_index." SET risk_obj = (frequency * vul_obj * amount),
					risk_person = (frequency * vul_person * no_person * exp_person),
					risk_amount_person = (frequency * vul_person * no_person * exp_person * value_person),
					risk_individual = (frequency * vul_person * 1 * exp_person);";
					
		### query to add a new record to the risk_scenarios table
		$query .= "INSERT INTO ".$workspace.".risk_scenarios (id, id_utilisateur, nom, description, alt_id, option, processus_type, processus_id, objet_type, objet_id, prix_surface, nombre_personnes, exp_personnes, prix_personne, vul_parametres, indice, poly_texte, objet_total, humain_total, risque_total, risque_individuel)
			VALUES (DEFAULT, $userID, '$scenario_name', '$scenario_description', '$altID', '$option', '$haz_type',
			(SELECT id FROM ".$workspace.".hazards WHERE indice = '$haz_name'), '$object_type',
			(SELECT id FROM ".$workspace.".objects WHERE indice = '$object_name'), 
			$price_sq_m, $no_person, $exp_person_hour, $value_person, '$vulRecords', '$mapping_index', '$area',
			(SELECT COALESCE(sum(risk_obj),0) FROM ".$workspace.".".$mapping_index."),
			(SELECT COALESCE(sum(risk_person),0) FROM ".$workspace.".".$mapping_index."),
			(SELECT COALESCE(sum(risk_obj)+sum(risk_amount_person),0) FROM ".$workspace.".".$mapping_index."),
			(SELECT COALESCE(sum(risk_individual),0) FROM ".$workspace.".".$mapping_index."));";
		
		If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				exit;
			}
		else {
			// if success, arrives here			
			# publish the calculated risk scenario table to the geoserver
			
			// Initiate cURL session			
			$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes"; // to add a new featuretype

			$url = $service . $request;
			$ch = curl_init($url);
			
			// Optional settings for debugging
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

			//Required POST request settings
			curl_setopt($ch, CURLOPT_POST, True);			
			curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
			
			//POST data 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/xml"));
			$xmlStr = "<featureType><name>".$mapping_index."</name><title>".$scenario_name."</title><keywords><string>features</string><string>".$scenario_name."</string></keywords></featureType>";
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);
			
			//POST return code
			$successCode = 201;

			// Execute the curl request
			$buffer = curl_exec($ch); 

			// Check for errors and process results
			$info = curl_getinfo($ch);
			// free resources if curl handle will not be reused
			curl_close($ch); 
			
			if ($info['http_code'] != $successCode) {
				$msgStr = "# Unsuccessful cURL request to ";
				$msgStr .= $url." [". $info['http_code']. "]\n";
				Echo '{success:false,message:'.json_encode($msgStr).'}';
			} else {
				// if successful, update the layer style
					$request = "rest/layers/".$workspaceGeoServer.":".$mapping_index."";
					$url = $service . $request;
					$ch = curl_init($url);
					
					// Optional settings for debugging
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
					curl_setopt($ch, CURLOPT_VERBOSE, true);
					curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

					//Required PUT request settings
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');					
					curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

					//POST data
					curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-type: text/xml"));
					$xmlStr = "<layer><defaultStyle><name>risk_buildings_style</name></defaultStyle><enabled>true</enabled></layer>";
					curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

					//POST return code
					$successCode = 200;
					
					// Execute the curl request
					$buffer = curl_exec($ch); 		
					// Check for errors and process results
					$info = curl_getinfo($ch);
					// free resources if curl handle will not be reused
					curl_close($ch);
					
					if ($info['http_code'] != $successCode) {
						Echo '{success:false, message:"There is an error in updating the layer style in GeoServer!"}';
					}
					else {
						$msgStr .= "Calculation completed and added the resulted risk total layer to the map!";
						Echo '{success: true, mpIndex:'.json_encode($mapping_index).',message:'.json_encode($msgStr).'}';
					}		
			//	$msgStr .= "Calculation completed and added the resulted risk total layer to the map!";
			//	Echo '{success: true, mpIndex:'.json_encode($mapping_index).',message:'.json_encode($msgStr).'}';
			}
		}	
	}
	
	if ($task == 'edit') {
		
		// Get the layer name 
		$mpIndex = $_POST['mpIndex'];		
		$ID = $_POST['ID'];	
		$nom = $_POST['name'];
		$desc = $_POST['description'];		
		
			// Initiate cURL session			
			$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes/".$mpIndex.".xml"; // to edit a feature store

			$url = $service . $request;
			$ch = curl_init($url);

			// Optional settings for debugging
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

			//Required PUT request settings
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');		
			curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
			
			//POST data
			curl_setopt($ch, CURLOPT_HTTPHEADER,
				array("Content-type: application/xml"));
			$xmlStr = "<featureType><title>".$nom."</title><keywords><string>features</string><string>".$nom."</string></keywords><enabled>true</enabled></featureType>";
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

			//POST return code
			$successCode = 200;

			// Execute the curl request
			$buffer = curl_exec($ch); 
			
			$info = curl_getinfo($ch);
			curl_close($ch); // free resources if curl handle will not be reused
			
			if ($info['http_code'] != $successCode) {
				Echo '{success:false, message:"There is an error in changing the layer title in GeoServer!"}';
			}
			else {
				// if success, update the record in the table
				$query = "UPDATE ".$workspace.".risk_scenarios SET nom = '$nom', description = '$desc' WHERE id = $ID;";
				$arr = array();
				
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {
					Echo '{success:true,message:"The layer information has been successfully updated!"}';		
				}				
			}
	}
	
	if ($task == 'delete') {
		$temp = $_POST['IDs'];		
		$riskSceRecords = json_decode($temp, true);
		$length = count($riskSceRecords);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected risk scenarios
			$ID = $riskSceRecords[$i]['id'];			
			$mpIndex = $riskSceRecords[$i]['indice'];
			
			### Step1: remove the records from the risk scenarios table and its referenced records in other tables
			$query = "DELETE FROM ".$workspace.".risk_scenarios WHERE id = $ID;";
			
			### Step2: drop the respective layer table using indice column from the db 
			$query .= "DROP TABLE ".$workspace.".$mpIndex CASCADE;";
				
			if (!$rs = pg_query($dbconn,$query)){			
				$message .= 'Failed to delete the record: '.$riskSceRecords[$i]['nom'].' due to the error: '.json_encode(pg_last_error($dbconn));						
			}
			else {
				// if success
				### Step3: remove the published map from the geoserver
					// Initiate cURL session					
					$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes/".$mpIndex.".xml?recurse=true"; // to delete a feature type

					$url = $service . $request;
					$ch = curl_init($url);

					// Optional settings for debugging
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
					curl_setopt($ch, CURLOPT_VERBOSE, true);
					curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

					//Required DELETE request settings
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");					
					curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

					//POST return code
					$successCode = 200;

					// Execute the curl request
					$buffer = curl_exec($ch); 

					// Check for errors and process results
					$info = curl_getinfo($ch);			
					if ($info['http_code'] != $successCode) {
						$message .= "Unuccessful cURL request to ".$url."\n";						
					}
					else {
						$message .= 'The selected risk scenario layer: '.$riskSceRecords[$i]['nom'].' has been deleted!';
					}
					curl_close($ch); // free resources if curl handle will not be reused
					
			}
		}
		Echo '{success: true, message:'.json_encode($message).'}';
	}

?>	