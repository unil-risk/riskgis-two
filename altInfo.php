<?php
	require_once 'dbConnect.php'; // Connect to the database
	require_once 'geoServerConfig.php'; // GeoServer configurations
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$userID = $_POST['userID'];	
	$userRole = $_POST['userRole'];		
	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}

	if ($task == 'load') {
		// allow all layers to be visible if logged with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT * FROM ".$workspace.".alternatives;";
		}
		else{
			// allow only own layers or 'do nothing' to be visible if logged with other roles
			$query = "SELECT * FROM ".$workspace.".alternatives WHERE id_utilisateur = $userID OR id = 1;";
		}
		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'loadGrid') {
		// allow all layers to be visible if logged with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT alternatives.*, string_agg(measures.ouvrage,', ') AS mesures_nom, string_agg(cast(measures.id as text),',') AS mesures_id, user_name AS nom_utilisateur
			FROM ".$workspace.".alternatives, ".$workspace.".alternatives_measures, ".$workspace.".measures, ".$workspace.".users  
			WHERE alternatives_measures.mes_id = measures.id
			AND alternatives.id = alternatives_measures.alt_id
			AND alternatives.id_utilisateur = users.id
			GROUP BY alternatives.id, user_name;";
		}
		else{
			// allow only own layers to be visible if logged with other roles
			$query = "SELECT alternatives.*, string_agg(measures.ouvrage,', ') AS mesures_nom, string_agg(cast(measures.id as text),',') AS mesures_id, user_name AS nom_utilisateur
			FROM ".$workspace.".alternatives, ".$workspace.".alternatives_measures, ".$workspace.".measures, ".$workspace.".users  
			WHERE alternatives_measures.mes_id = measures.id
			AND alternatives.id = alternatives_measures.alt_id
			AND id_utilisateur = $userID
			AND alternatives.id_utilisateur = users.id
			GROUP BY alternatives.id, user_name;";
		}

		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
		// retrieve POST data 
		$alternative_name = $_POST['name'];
		$alternative_description = $_POST['description'];
		$option = $_POST['option'];	
		$remarks = $_POST['remarks'];
		$arr =  explode(",",$_POST['measures']);
		
		$mapping_index = preg_replace('/\s+/','_',$alternative_name); // replace whitespaces and spaces with underscore 
		$mapping_index = strtolower($mapping_index); // change to lowercase // change later to a unique index name 
		$tab_name = $mapping_index;	
		
		$minx = $_POST['LatLon_minx'];
		$miny = $_POST['LatLon_miny'];
		$maxx = $_POST['LatLon_maxx'];
		$maxy = $_POST['LatLon_maxy'];
		
		if ($option == 'Sketch') {
		### if sketch option, create a new empty table
			$query = "CREATE TABLE ".$workspace.".".$tab_name." (
					  fid serial NOT NULL,
					  geom geometry(MultiPolygon,4326),			  
					  mesure_nom text,
					  mesure_description text,
					  remarques text,
					  CONSTRAINT ".$tab_name."_pkey PRIMARY KEY (fid),
					  CONSTRAINT enforce_dims_the_geom CHECK (st_ndims(geom) = 2), 
					  CONSTRAINT enforce_geotype_the_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text OR geom IS NULL),
					  CONSTRAINT enforce_srid_the_geom CHECK (st_srid(geom) = 4326)
					)
					WITH (
					  OIDS=FALSE
					);";
		}	
		
		### add a record to the alternative table with meta info
		### save relationship records to the alternatives_measures table
		
		$query .= "INSERT INTO ".$workspace.".alternatives VALUES (DEFAULT, '$alternative_name', '$alternative_description', '$option', '$mapping_index','$remarks', $userID);"; 
		foreach ($arr as $measure){
			$query .= "INSERT INTO ".$workspace.".alternatives_measures VALUES ((SELECT id FROM ".$workspace.".alternatives WHERE indice = '$mapping_index'),(SELECT id FROM ".$workspace.".measures WHERE id = $measure));";			
		}
		
		### add new records to cost_values table
		$query .= "INSERT INTO ".$workspace.".cost_values
			(
				alt_id,
				mesure_id,
				mode,
				cout_investissement,
				valeur_residuelle,
				cout_exploitation,
				cout_entretien,
				cout_reparation,
				periode_ans,
				taux_interet,
				cout_annuel
			)
		SELECT t3.id, t1.mes_id, t2.mode, 0, 0, 0, 0, 0, t2.duree_effet, t2.taux_interet, 0
		FROM
			".$workspace.".alternatives_measures t1,
			".$workspace.".measures t2,
			".$workspace.".alternatives t3
		WHERE
			t1.alt_id = t3.id
			AND t1.mes_id = t2.id
			AND t3.indice = '$mapping_index';";		
				
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			### if sketch option, publish the empty table to the geoserver
			if ($option == 'Sketch') {
			// Initiate cURL session				
				$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes"; // to add a new featuretype

				$url = $service . $request;
				$ch = curl_init($url);

				// Optional settings for debugging
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
				curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

				//Required POST request settings
				curl_setopt($ch, CURLOPT_POST, True);				
				curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

				//POST data
				curl_setopt($ch, CURLOPT_HTTPHEADER,
					array("Content-type: application/xml"));
				$xmlStr = "<featureType><name>".$tab_name."</name><title>".$alternative_name."</title><latLonBoundingBox><minx>".$minx."</minx><maxx>".$maxx."</maxx><miny>".$miny."</miny><maxy>".$maxy."</maxy><crs>EPSG:4326</crs></latLonBoundingBox></featureType>";
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

				//POST return code
				$successCode = 201;

				// Execute the curl request
				$buffer = curl_exec($ch); 

				// Check for errors and process results
				$info = curl_getinfo($ch);
				// free resources if curl handle will not be reused
				curl_close($ch);
					
				if ($info['http_code'] != $successCode) {
					$msgStr = "# Unsuccessful cURL request to ";
					$msgStr .= $url." [". $info['http_code']. "]\n";
					Echo '{success:false,message:'.json_encode($msgStr).'}';
				} else {
					// if successful, update the layer style
					$request = "rest/layers/".$workspaceGeoServer.":".$tab_name."";
					$url = $service . $request;
					$ch = curl_init($url);
					
					// Optional settings for debugging
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
					curl_setopt($ch, CURLOPT_VERBOSE, true);
					curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

					//Required PUT request settings
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');					
					curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

					//POST data
					curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-type: text/xml"));
					$xmlStr = "<layer><defaultStyle><name>alternative_style</name></defaultStyle><enabled>true</enabled></layer>";
					curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

					//POST return code
					$successCode = 200;
					
					// Execute the curl request
					$buffer = curl_exec($ch); 		
					// Check for errors and process results
					$info = curl_getinfo($ch);
					// free resources if curl handle will not be reused
					curl_close($ch);
					
					if ($info['http_code'] != $successCode) {
						Echo '{success:false, message:"There is an error in updating the layer style in GeoServer!"}';
					}
					else {
						$msgStr = "# Successful cURL request to ".$url."\n";
						Echo '{success: true, mpIndex:'.json_encode($mapping_index).',message:"The alternative layer has been created and added to the map. You can now start drawing the measures using CREATE and EDIT feature tools of the map center panel!"}';
					}
				}				 
			}
			else Echo '{success:true,message:"The new alternative scenario has been created!"}';			
		}
	}
	
	if ($task == 'edit') {
		
		// Get the layer name 
		$mpIndex = $_POST['mpIndex'];		
		$ID = $_POST['ID'];	
		$categorie = $_POST['option'];
		$nom = $_POST['name'];
		$desc = $_POST['description'];
		$remarques = $_POST['remarks'];
		
		if ($categorie == 'Sketch'){ // if sketch, update the layer title in GeoServer
			// Initiate cURL session			
			$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes/".$mpIndex.".xml"; // to edit a feature store

			$url = $service . $request;
			$ch = curl_init($url);

			// Optional settings for debugging
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

			//Required PUT request settings
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');			
			curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
			
			//POST data
			curl_setopt($ch, CURLOPT_HTTPHEADER,
				array("Content-type: application/xml"));
			$xmlStr = "<featureType><title>".$nom."</title><keywords><string>features</string><string>".$nom."</string></keywords><enabled>true</enabled></featureType>";
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

			//POST return code
			$successCode = 200;

			// Execute the curl request
			$buffer = curl_exec($ch); 
			
			$info = curl_getinfo($ch);
			curl_close($ch); // free resources if curl handle will not be reused
			
			if ($info['http_code'] != $successCode) {
				Echo '{success:false, message:"There is an error in changing the layer title in GeoServer!"}';
			}			
		}
		
		// if success and in both cases, update the record in the table
		$query = "UPDATE ".$workspace.".alternatives SET nom = '$nom', description = '$desc', remarques = '$remarques' WHERE id = $ID;";
		$arr = array();
				
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The layer information has been successfully updated!"}';		
		}
			
	}
	
	if ($task == 'delete') {
		$temp = $_POST['IDs'];		
		$altRecords = json_decode($temp, true);
		$length = count($altRecords);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected alternatives
			$query = '';
			$altID = $altRecords[$i]['id'];
			$altMP = $altRecords[$i]['indice'];
			
			// if option 'Descriptive', 
			// remove the records from the alternative table
			
			if ($altRecords[$i]['categorie'] == 'Descriptive'){
				$query .= "DELETE FROM ".$workspace.".alternatives WHERE id = $altID;";
				
				// run db queries
				if (!$rs = pg_query($dbconn,$query)){
					$message .= pg_last_error($dbconn)."\n";
				}	
				else {
					$message .= 'The selected alternative layer: '.$altRecords[$i]['nom'].' has been deleted!\n';
				}
			}
			else {
			// if option 'Sketch'  			
			// Step1: remove the publish maps from the geoserver
				// Initiate cURL session				
				$request = "rest/workspaces/".$workspaceGeoServer."/datastores/postgis/featuretypes/".$altMP.".xml?recurse=true"; // to delete a featuretype

				$url = $service . $request;
				$ch = curl_init($url);

				// Optional settings for debugging
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
				curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

				//Required DELETE request settings
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");				
				curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

				//POST return code
				$successCode = 200;

				// Execute the curl request
				$buffer = curl_exec($ch); 

				// Check for errors and process results
				$info = curl_getinfo($ch);			
				if ($info['http_code'] != $successCode) {
					$message .= "# Unuccessful cURL request to ".$url."\n";					
				} 
				else {
					// Step2: drop the respective tables using mapping_index from db
					$query .= "DROP TABLE ".$workspace.".$altMP;";
					// Step3: remove the records from the alternative table
					$query .= "DELETE FROM ".$workspace.".alternatives WHERE id = $altID;";
					
					// run db queries
					if (!$rs = pg_query($dbconn,$query)){
						$message .= pg_last_error($dbconn)."\n";
					}	
					else {
						$message .= 'The selected alternative layer: '.$altRecords[$i]['nom'].' has been deleted!\n';
					}
				}
				curl_close($ch); // free resources if curl handle will not be reused
			}
		}
		
		Echo '{success:true,message:'.json_encode($message).'}';
	}
	
?>
